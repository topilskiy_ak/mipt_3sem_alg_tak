#include "C_Floyd_lib.h"

// AIM: Алгоритм Флойда-Уоршелла на полном графе
int main() {
    const DenseGraph graph = read_floyd();
    plain_matrix<double> shortest_paths(graph.vertices(), graph.vertices());

    get_shortest_paths(shortest_paths, graph);

    output_matrix<double>(shortest_paths);

    return 0;
}