cmake_minimum_required(VERSION 3.6)
project(C_Floyd)

#===========================================================================================================================
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})

set(HEADER_FILES graph.h C_Floyd_lib.h)
set(SOLUTION_FILES C_Floyd.cpp C_Floyd_lib.cpp)

add_executable(C_Floyd ${SOLUTION_FILES})
target_link_libraries(C_Floyd graph)
#===========================================================================================================================
set(HEADER_FILES_TEST C_Floyd_lib.h gtest.h)
set(SOLUTION_FILES_TEST C_Floyd_test.cpp C_Floyd_lib.cpp)

add_executable(C_Floyd_test ${SOLUTION_FILES_TEST})
target_link_libraries(C_Floyd_test graph gtest_main)

#===========================================================================================================================
