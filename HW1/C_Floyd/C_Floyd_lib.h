#ifndef MIPT_3SEM_ALG_C_FLOYD_LIB_H
#define MIPT_3SEM_ALG_C_FLOYD_LIB_H

#include <graph/graph.h>
#include <fstream>

// AIM: Считать данные из файла floyd.in
const DenseGraph read_floyd();

// AIM: Применив Алгоритм Флойда-Уоршелла получить матрицу минимальных путей
void get_shortest_paths(plain_matrix<double>& shortest_paths, const DenseGraph& graph);

// AIM: Вывод в floyd.out матрицы минимальных путей
template<typename T>
void output_matrix(const plain_matrix<T>& mtx) {
    size_t mtx_size = mtx.w();
    std::ofstream fout("floyd.out");

    for (size_t v = 0; v < mtx_size; ++v) {
        for (size_t e = 0; e < mtx_size; ++e) {
            fout << mtx(v, e) << " ";
        }
        fout << std::endl;
    }

    fout.close();
}

#endif //MIPT_3SEM_ALG_C_FLOYD_LIB_H
