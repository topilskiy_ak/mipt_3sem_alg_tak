#include <gtest/gtest.h>
#include "C_Floyd_lib.h"

TEST(C_FloydTests, Test_read_floyd) {
    const DenseGraph graph = read_floyd();
    EXPECT_EQ(graph.to_string(), "0 5 9 100  100 0 2 8  100 100 0 7  4 100 100 0  ");
}

TEST(C_FloydTests, Test_get_shortest_paths) {
    const DenseGraph graph = read_floyd();
    plain_matrix<double> shortest_paths(graph.vertices(), graph.vertices());
    get_shortest_paths(shortest_paths, graph);

    EXPECT_EQ(shortest_paths.to_string(), "0 5 7 13  12 0 2 8  11 16 0 7  4 9 11 0  ");
}