#include "C_Floyd_lib.h"

// AIM: Считать данные из файла floyd.in
const DenseGraph read_floyd() {
    size_t num_vertices = 0;
    double weight = 0;
    std::ifstream fin("floyd.in");

    fin >> num_vertices;

    DenseGraph graph(num_vertices, true);
    for (size_t v = 0; v < graph.vertices(); ++v) {
        for (size_t e = 0; e < graph.vertices(); ++e) {
            fin >> weight;
            graph.insert(Edge(v, e, weight));
        }
    }

    fin.close();
    return graph;
}

// AIM: Применив Алгоритм Флойда-Уоршелла получить матрицу минимальных путей
void get_shortest_paths(plain_matrix<double>& shortest_paths, const DenseGraph& graph) {
    size_t num_v = graph.vertices();

    for (size_t v = 0; v < num_v; ++v) {
        for (size_t e = 0; e < num_v; ++e) {
            shortest_paths(v, e) = graph.edge_weight(v, e);
        }
    }

    for (size_t iterator = 0; iterator < num_v; ++iterator) {
        for (size_t v = 0; v < num_v; ++v) {
            for (size_t e = 0; e < num_v; ++e) {
                if (v != e) {
                    double alt_path = shortest_paths(v, iterator) + shortest_paths(iterator, e);
                    if (shortest_paths(v, e) > alt_path) {
                        shortest_paths(v, e) = alt_path;
                    }
                }
            }
        }
    }
}