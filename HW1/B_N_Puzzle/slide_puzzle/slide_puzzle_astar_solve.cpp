#include "slide_puzzle.h"
#include "slide_puzzle_astar_solve.h"

#include <set>
#include <sstream>

#include <iostream>

//===========================================================================================================================
// CLASS: Функтор для уникального сравнения конфигураций для set
class SlidePuzzleNodeUniqueComparator {
public:
    bool operator()(const SlidePuzzleNode& node1, const SlidePuzzleNode& node2) {
        for (size_t row = 0; row < node1.dimension(); ++row) {
            for (size_t col = 0; col < node1.dimension(); ++col) {
                if (node1(row, col) < node2(row, col))
                    return true;
                if (node1(row, col) > node2(row, col))
                    return false;
            }
        }

        return false;
    }
};

//===========================================================================================================================
// AIM: Вернуть по начальной конфигурации строку
// "-1" если данная конфигурация не может быть решенной
// "N" а на следующей строке "W"
// где N - глубина минимального решения,
// а   W - слово, описывающее решение в терминах ходов 0-элемента по матрице
std::string slide_puzzle_astar_solve(const size_t& dimension,
                                     const std::vector<size_t>& start_configuration) {

    std::set<SlidePuzzleNode, SlidePuzzleNodeUniqueComparator> visited_nodes;
    SlidePuzzlePriorityQue current_nodes;

    SlidePuzzleNode curr_node(dimension, start_configuration);

    if (!curr_node.is_resolvable()) {
        return "-1\n";
    }


    while(!curr_node.is_finished()) {
        if (visited_nodes.find(curr_node) == visited_nodes.end()) {
            curr_node.get_next_slide_puzzle_nodes(current_nodes);
            visited_nodes.insert(curr_node);
        }

        curr_node = current_nodes.top();
        current_nodes.pop();
    }


    std::stringstream result_buffer;
    result_buffer << curr_node.depth() << std::endl << curr_node.path() << std::endl;

    return result_buffer.str();
}

//===========================================================================================================================
