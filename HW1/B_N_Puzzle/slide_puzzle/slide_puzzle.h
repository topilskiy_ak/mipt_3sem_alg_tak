#ifndef MIPT_3SEM_ALG_SLIDE_PUZZLE_H
#define MIPT_3SEM_ALG_SLIDE_PUZZLE_H

#include <cstdlib>
#include <vector>
#include <string>
#include <memory>
#include <queue>

//===========================================================================================================================
// Класс данной конфигурации
class SlidePuzzle {
protected:
    size_t _dimension = 0;                 // размер матрицы
    size_t _zero_pos = 0;                  // координата пустой клетки в последовательности конфигурации
    size_t _manhat_dist = 0;               // расстояние манхеттона для данной конфигурации
    std::vector<size_t> _configuration;    // конфигурация матрицы

    // AIM: Вычислить координату пустой клетки в последовательности конфигурации
    void _set_zero_pos();

    // AIM: Вычислить расстояние манхеттона для данной конфигурации
    void _set_manhattan_distance();

    // AIM : Вернуть четность последовательности конфигурации, не учитывая 0-символ
    size_t _num_inversions() const;

    // AIM : Вернуть количество Линейных Строковых Конфликтов
    size_t _num_lin_row_conflicts() const;

    // AIM : Вернуть количество Линейных Столбцовых Конфликтов
    size_t _num_lin_col_conflicts() const;

public:
    // CONSTRUCTOR: строит элемент dimension-нашек по вектору configuration
    SlidePuzzle(const size_t &dimension, const std::vector<size_t> &configuration) :
        _dimension(dimension), _configuration(configuration) {
        _set_zero_pos();
        _set_manhattan_distance();
    }

    // AIM: Вернуть размерность Матрицы Конфигурации
    const size_t dimension() const { return _dimension; }

    // AIM: Вернуть const-ссылку на элемент матрицы конфигурации
    const size_t& operator() (const size_t& i, const size_t& j) const
                            { return _configuration[_dimension * i + j]; }

    // AIM: Вернуть, возможно ли из данный конфигурации попасть в завершенную
    bool is_resolvable() const;

    // AIM: Проверка конфигурации на завершенность
    // (те на равенство вектору <1, 2... _dimension-1, 0>)
    bool is_finished() const { return _manhat_dist == 0; }

    // AIM: Вернуть евристику - расстояние манхеттона для данной конфигурации
    size_t manhattan_distance() const { return _manhat_dist; }

    // AIM: Вернуть евристику - расстояние манхеттона c строк-столб конфликтами для данной конфигурации
    size_t manhattan_distance_w_lin_rowcol_conflicts() const;

    // AIM: Напечатать в STDOUT сведения о данной конфигурации
    void puzzle_show() const;
};

//===========================================================================================================================
// CLASS: Функтор для сравнения конфигураций по взвешанному пути (глубине + эвристике Манх с ЛинКонфл)
class SlidePuzzleNodeDistanceComparator;
// Класс данных оборачивающий конфигурацию в вершину графа под A*
class SlidePuzzleNode;

// TYPE: Очередь приоритета SlidePuzzleNode по заданному функтору
using SlidePuzzlePriorityQue = std::priority_queue<SlidePuzzleNode, std::vector<SlidePuzzleNode>,
                                                   SlidePuzzleNodeDistanceComparator>;

//===========================================================================================================================
// Класс данных оборачивающий конфигурацию в вершину графа под A*
class SlidePuzzleNode : public SlidePuzzle {
private:
    std::string _prev_moves = ""; // последовательность шагов, чтобы достичь данную конфигурацию

public:
    // CONSTRUCTOR: строит элемент dimension-нашек по вектору configuration
    SlidePuzzleNode(const size_t& dimension,
                    const std::vector<size_t>& configuration,
                    const std::string& prev_moves = "") :
            SlidePuzzle(dimension, configuration), _prev_moves(prev_moves) {}

    // AIM: Вернуть вектор конфигураций, которые могут быть получены из данный 1 ходом
    void get_next_slide_puzzle_nodes(SlidePuzzlePriorityQue& slide_puzzle_que) const;

    // AIM: Вернуть глубину прохода для данной конфигурации
    size_t depth() const { return _prev_moves.size(); }

    // AIM: Вернуть A* расстояние для данной вершины
    size_t distance() const { return depth() + manhattan_distance_w_lin_rowcol_conflicts(); }

    // AIM: Вернуть строку прохода до данной конфигурации
    const std::string& path() const { return _prev_moves; }

    // AIM: Напечатать в STDOUT сведения о данной вершине
    void show() const;
};

//===========================================================================================================================
// CLASS: Функтор для сравнения конфигураций по взвешанному пути (глубине + эвристике Манх с ЛинКонфл)
class SlidePuzzleNodeDistanceComparator {
public:
    bool operator()(const SlidePuzzleNode& node1, const SlidePuzzleNode& node2) {
        return node1.distance() > node2.distance();
    }
};

//===========================================================================================================================



#endif //MIPT_3SEM_ALG_SLIDE_PUZZLE_H
