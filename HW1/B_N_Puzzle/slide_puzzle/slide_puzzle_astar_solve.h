#ifndef MIPT_3SEM_ALG_SLIDE_PUZZLE_ASTAR_SOLVE_H
#define MIPT_3SEM_ALG_SLIDE_PUZZLE_ASTAR_SOLVE_H

#include <vector>
#include <string>

// AIM: Вернуть по начальной конфигурации строку
// "-1" если данная конфигурация не может быть решенной
// "N" а на следующей строке "W"
// где N - глубина минимального решения,
// а   W - слово, описывающее решение в терминах ходов 0-элемента по матрице
std::string slide_puzzle_astar_solve(const size_t& dimension,
                                     const std::vector<size_t>& start_configuration);

#endif //MIPT_3SEM_ALG_SLIDE_PUZZLE_ASTAR_SOLVE_H
