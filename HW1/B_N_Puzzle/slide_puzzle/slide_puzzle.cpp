#include "slide_puzzle.h"
#include <iostream>

//===========================================================================================================================
// AIM: Вычислить координату пустой клетки
void SlidePuzzle::_set_zero_pos() {
    while(_zero_pos < _configuration.size() && _configuration[_zero_pos] != 0) {
        ++_zero_pos;
    }
}

// AIM: Вычислить расстояние манхеттона для данной конфигурации
void SlidePuzzle::_set_manhattan_distance() {
    _manhat_dist = 0;

    for (size_t row = 0; row < _dimension; ++row) {
        for (size_t col = 0; col < _dimension; ++col) {
            const size_t curr_elem = (*this)(row, col);

            if(curr_elem != 0) {
                size_t curr_elem_fin_row = (curr_elem - 1) / _dimension;
                size_t curr_elem_fin_col = (curr_elem - 1) % _dimension;

                _manhat_dist += abs(static_cast<int>(row) - static_cast<int>(curr_elem_fin_row));
                _manhat_dist += abs(static_cast<int>(col) - static_cast<int>(curr_elem_fin_col));
            }
        }
    }
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM : Вернуть четность последовательности конфигурации, не учитывая 0-символ
size_t SlidePuzzle::_num_inversions() const {
    size_t num_inversions = 0;

    for (size_t first_it = 0; first_it < _configuration.size(); ++first_it) {
        for (size_t second_it = first_it + 1; second_it < _configuration.size(); ++second_it) {
            if (_configuration[first_it] != 0 && _configuration[second_it] != 0
                    && _configuration[first_it] > _configuration[second_it])
                ++num_inversions;
        }
    }

    return num_inversions;
}

// AIM: Вернуть, возможно ли из данный конфигурации попасть в завершенную
bool SlidePuzzle::is_resolvable() const {
    size_t num_inversions = _num_inversions();

    if (_dimension % 2 != 0) {
        return num_inversions % 2 == 0;
    } else {
        size_t zero_row_from_bot = _dimension - (_zero_pos / _dimension) - 1;
        return (zero_row_from_bot + num_inversions) % 2 == 0;
    }
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM : Вернуть количество Линейных Строковых Конфликтов
size_t SlidePuzzle::_num_lin_row_conflicts() const {
    size_t num_lin_row_confl = 0;

    for (size_t row = 0; row < _dimension; ++row) {
        size_t curr_row_max = 0;

        for (size_t col = 0; col < _dimension; ++col) {
            size_t curr_elem = (*this)(row, col);
            size_t curr_elem_row = (curr_elem - 1) / _dimension;

            if (curr_elem != 0 && row == curr_elem_row) {
                if (curr_elem > curr_row_max){
                    curr_row_max = curr_elem;
                } else {
                    ++num_lin_row_confl;
                }
            }
        }
    }

    return num_lin_row_confl;
}

// AIM : Вернуть количество Линейных Столбцовых Конфликтов
size_t SlidePuzzle::_num_lin_col_conflicts() const {
    size_t num_lin_col_confl = 0;

    for (size_t col = 0; col < _dimension; ++col) {
        size_t curr_col_max = 0;

        for (size_t row = 0; row < _dimension; ++row) {
            size_t curr_elem = (*this)(row, col);
            size_t curr_elem_col = (curr_elem - 1) % _dimension;

            if (curr_elem != 0 && col == curr_elem_col) {
                if (curr_elem > curr_col_max) {
                    curr_col_max = curr_elem;
                } else {
                    ++num_lin_col_confl;
                }
            }
        }
    }

    return num_lin_col_confl;
}

// AIM: Вернуть евристику - расстояние манхеттона c строк-столб конфликтами для данной конфигурации
size_t SlidePuzzle::manhattan_distance_w_lin_rowcol_conflicts() const {
    size_t lrc_manhat_dist = _manhat_dist;

    lrc_manhat_dist += 2 * _num_lin_row_conflicts();
    lrc_manhat_dist += 2 * _num_lin_col_conflicts();

    return lrc_manhat_dist;
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM: Напечатать в STDOUT сведения о данной конфигурации
void SlidePuzzle::puzzle_show() const {
    for (size_t row = 0; row < _dimension; ++row) {
        for (size_t col = 0; col < _dimension; ++col) {
            std::cout << (*this)(row, col) << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

//===========================================================================================================================
// AIM: Вернуть вектор конфигураций, которые могут быть получены из данный 1 ходом
void SlidePuzzleNode::get_next_slide_puzzle_nodes(SlidePuzzlePriorityQue& slide_puzzle_que) const {
    size_t zero_pos_row = _zero_pos / _dimension;
    size_t zero_pos_col = _zero_pos % _dimension;

    std::vector<size_t> new_configuration(_configuration);

    // Move 0 Up
    if (zero_pos_row != 0) {
        size_t new_zero_pos = _zero_pos - _dimension;
        std::swap(new_configuration[_zero_pos], new_configuration[new_zero_pos]);

        slide_puzzle_que.emplace(_dimension, new_configuration, _prev_moves + "U");
        std::swap(new_configuration[_zero_pos], new_configuration[new_zero_pos]);
    }

    // Move 0 Down
    if (zero_pos_row + 1 != _dimension) {
        size_t new_zero_pos = _zero_pos + _dimension;
        std::swap(new_configuration[_zero_pos], new_configuration[new_zero_pos]);

        slide_puzzle_que.emplace(_dimension, new_configuration, _prev_moves + "D");

        std::swap(new_configuration[_zero_pos], new_configuration[new_zero_pos]);
    }

    // Move 0 Right
    if (zero_pos_col + 1 != _dimension) {
        size_t new_zero_pos = _zero_pos + 1;
        std::swap(new_configuration[_zero_pos], new_configuration[new_zero_pos]);

        slide_puzzle_que.emplace(_dimension, new_configuration, _prev_moves + "R");

        std::swap(new_configuration[_zero_pos], new_configuration[new_zero_pos]);
    }

    // Move 0 Left
    if (zero_pos_col != 0) {
        size_t new_zero_pos = _zero_pos - 1;
        std::swap(new_configuration[_zero_pos], new_configuration[new_zero_pos]);

        slide_puzzle_que.emplace(_dimension, new_configuration, _prev_moves + "L");
    }
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM: Напечатать в STDOUT сведения о данной вершине
void SlidePuzzleNode::show() const {
    std::cout << "Depth: " << depth() << std::endl;
    std::cout << "Path:  " << path() << std::endl;
    puzzle_show();
}

//===========================================================================================================================
