#include <gtest/gtest.h>
#include "slide_puzzle.h"
#include "slide_puzzle_astar_solve.h"

const static size_t DIMENSION1 = 3;
const static size_t DIMENSION2 = 4;
std::vector<size_t> config1{ 1, 2, 3, 4, 5, 6, 7, 8, 0 };
std::vector<size_t> config2{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0, 13, 14, 12, 15 };
std::vector<size_t> config3{ 1, 2, 3, 4, 5, 6, 7, 8, 0 };
std::vector<size_t> config4{ 7, 8, 3, 4, 2, 1, 6, 0, 5 };
std::vector<size_t> config5{ 6, 8, 3, 4, 2, 1, 7, 0, 5 };
SlidePuzzleNode spn1(DIMENSION1, config1);
SlidePuzzleNode spn2(DIMENSION2, config2);
SlidePuzzleNode spn3(DIMENSION1, config3);
SlidePuzzleNode spn4(DIMENSION1, config4);


TEST(SlidePuzzleNodeTests, TestConstructor) {
    SlidePuzzleNode spn1(DIMENSION1, config1);
}

TEST(SlidePuzzleNodeTests, TestManhattanDistance) {
    EXPECT_EQ(spn1.manhattan_distance(), 0);
    EXPECT_EQ(spn2.manhattan_distance(), 3);
    EXPECT_EQ(spn3.manhattan_distance(), 0);
    EXPECT_EQ(spn4.manhattan_distance(), 13);
}

TEST(SlidePuzzleNodeTests, TestUpgradedDistance) {
    EXPECT_EQ(spn1.distance(), 0);
    EXPECT_EQ(spn2.distance(), 3);
    EXPECT_EQ(spn3.distance(), 0);
    EXPECT_EQ(spn4.distance(), 17);
}

TEST(SlidePuzzleNodeTests, TestDimension) {
    EXPECT_EQ(spn1.dimension(), DIMENSION1);
    EXPECT_EQ(spn2.dimension(), DIMENSION2);
    EXPECT_EQ(spn3.dimension(), DIMENSION1);
    EXPECT_EQ(spn4.dimension(), DIMENSION1);
}

TEST(SlidePuzzleNodeTests, TestIsResolvable) {
    EXPECT_TRUE(spn1.is_resolvable());
    EXPECT_FALSE(spn2.is_resolvable());
    EXPECT_TRUE(spn3.is_resolvable());
    EXPECT_TRUE(spn4.is_resolvable());
}

TEST(SlidePuzzleNodeTests, TestIsFinished) {
    EXPECT_TRUE(spn1.is_finished());
    EXPECT_FALSE(spn2.is_finished());
    EXPECT_TRUE(spn3.is_finished());
    EXPECT_FALSE(spn4.is_finished());
}



TEST(SlidePuzzleAstarSolveTests, TestSolving) {
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION1, config1), "0\n\n");
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION2, config2), "-1\n");
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION1, config3), "0\n\n");
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION1, config4), "23\nLUURDRULDLURRDDLULDRURD\n");
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION1, config5), "-1\n");

    std::vector<size_t> config01{0, 1, 6, 4, 3, 2, 7, 5, 8};
    std::vector<size_t> config02{0, 1, 2, 3, 4, 5, 6, 7, 8};
    std::vector<size_t> config03{1, 2, 3, 8, 0, 4, 7, 6, 5};
    std::vector<size_t> config04{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0, 13, 14, 15, 12};

    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION1, config01), "8\nRDRULDDR\n");
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION1, config02), "22\nDRRULLDDRUURDLLURRDLDR\n");
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION1, config03), "-1\n");
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION2, config04), "1\nD\n");
}

TEST(SlidePuzzleAstarSolveTests, TestSolving8) {
    std::vector<size_t> config81{0, 1, 6, 4, 3, 2, 7, 5, 8};
    std::vector<size_t> config82{1, 2, 3, 6, 7, 8, 0, 4, 5};
    std::vector<size_t> config83{5, 1, 3, 4, 0, 8, 7, 6, 2};
    std::vector<size_t> config84{7, 5, 3, 4, 0, 8, 1, 6, 2};
    std::vector<size_t> config85{7, 3, 5, 4, 2, 8, 1, 6, 0};

    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION1, config81), "8\nRDRULDDR\n");
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION1, config82), "10\nRULDRRULDR\n");
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION1, config83), "14\nDRULDLUURDLDRR\n");
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION1, config84), "24\nDRULLDRULURDLDRULURDLDRR\n");
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION1, config85), "24\nLLURRULLDRDLURULDRDLURRD\n");
}

TEST(SlidePuzzleAstarSolveTests, TestSolving16) {
    std::vector<size_t> config61{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0, 13, 14, 15, 12};
    std::vector<size_t> config62{1, 2, 3, 4, 5, 0, 7, 8, 9, 10, 11, 6, 13, 14, 15, 12};
    std::vector<size_t> config63{14, 2, 3, 4, 5, 1, 7, 8, 9, 10, 11, 6, 13, 0, 15, 12};
    std::vector<size_t> config64{1, 4, 2, 3, 5, 7, 9, 8, 10, 0, 11, 6, 13, 14, 15, 12};
    std::vector<size_t> config65{1, 2, 3, 4, 5, 12, 8, 7, 9, 10, 11, 6, 13, 0, 14, 15};

    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION2, config61), "1\nD\n");
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION2, config62), "12\nDRRULDLURRDD\n");
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION2, config63), "30\nRURULLLURDLDRURDDLULURULDRRRDD\n");
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION2, config64), "29\nLDRRUULURDLDRRULDDLLURURURDDD\n");
    EXPECT_EQ(slide_puzzle_astar_solve(DIMENSION2, config65), "20\nRUULDRRULDLURDRULDDR\n");
}