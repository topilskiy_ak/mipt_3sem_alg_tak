#include <fstream>
#include <math.h>

#include <queue>
#include <set>
#include <sstream>

#include <iostream>

#include <cstdlib>
#include <vector>
#include <string>

#include <queue>
#include <set>

//===========================================================================================================================
// Класс данной конфигурации
class SlidePuzzle {
protected:
    size_t _dimension = 0;                 // размер матрицы
    size_t _zero_pos = -1;                  // координата пустой клетки в последовательности конфигурации
    size_t _manhat_dist = -1;               // расстояние манхеттона для данной конфигурации
    std::vector<size_t> _configuration;    // конфигурация матрицы

    // AIM: Вычислить координату пустой клетки в последовательности конфигурации
    void _set_zero_pos();

    // AIM: Вычислить расстояние манхеттона для данной конфигурации
    void _set_manhattan_distance();

    // AIM : Вернуть четность последовательности конфигурации, не учитывая 0-символ
    size_t _num_inversions() const;

    // AIM : Вернуть количество Линейных Строковых Конфликтов
    size_t _num_lin_row_conflicts() const;

    // AIM : Вернуть количество Линейных Столбцовых Конфликтов
    size_t _num_lin_col_conflicts() const;

public:
    // CONSTRUCTOR: строит элемент dimension-нашек по вектору configuration
    explicit SlidePuzzle(const size_t &dimension, const std::vector<size_t> &configuration,
                const size_t& zero_pos = -1, const size_t& manhat_dist = -1) :
            _dimension(dimension), _configuration(configuration),
            _zero_pos(zero_pos), _manhat_dist(manhat_dist) {

        if (_zero_pos == -1) {
            _set_zero_pos();
        }
        if (_manhat_dist == -1) {
            _set_manhattan_distance();
        }
    }

    // AIM: Вернуть размерность Матрицы Конфигурации
    const size_t dimension() const { return _dimension; }

    // AIM: Вернуть const-ссылку на элемент матрицы конфигурации
    const size_t& operator() (const size_t& i, const size_t& j) const
    { return _configuration[_dimension * i + j]; }

    // AIM: Вернуть, возможно ли из данный конфигурации попасть в завершенную
    bool is_resolvable() const;

    // AIM: Проверка конфигурации на завершенность
    // (те на равенство вектору <1, 2... _dimension-1, 0>)
    bool is_finished() const { return _manhat_dist == 0; }

    // AIM: Вернуть евристику - расстояние манхеттона для данной конфигурации
    size_t manhattan_distance() const { return _manhat_dist; }

    // AIM: Вернуть евристику - расстояние манхеттона c строк-столб конфликтами для данной конфигурации
    size_t manhattan_distance_w_lin_rowcol_conflicts() const;

    // AIM: Напечатать в STDOUT сведения о данной конфигурации
    void puzzle_show() const;
};


class SlidePuzzleNodeDistanceComparator;
//===========================================================================================================================
// Класс данных оборачивающий конфигурацию в вершину графа под A*
class SlidePuzzleNode : public SlidePuzzle {
private:
    std::string _prev_moves = ""; // последовательность шагов, чтобы достичь данную конфигурацию

public:
    // CONSTRUCTOR: строит элемент dimension-нашек по вектору configuration
    explicit SlidePuzzleNode(const size_t& dimension,
                    const std::vector<size_t>& configuration,
                    const std::string& prev_moves = "",
                    const size_t& zero_pos = -1, const size_t& manhat_dist = -1) :
            SlidePuzzle(dimension, configuration, zero_pos, manhat_dist), _prev_moves(prev_moves) {}

    // AIM: Вернуть строку с последовательностью шагов
    const std::string& prev_moves() const { return _prev_moves; }

    // AIM: Вернуть вектор конфигураций, которые могут быть получены из данный 1 ходом
    void get_next_slide_puzzle_nodes(std::priority_queue<SlidePuzzleNode, std::vector<SlidePuzzleNode>, SlidePuzzleNodeDistanceComparator>& current_nodes) const;

    // AIM: Вернуть глубину прохода для данной конфигурации
    size_t depth() const { return _prev_moves.size(); }

    // AIM: Вернуть A* расстояние для данной вершины
    size_t distance() const { return depth() + manhattan_distance_w_lin_rowcol_conflicts(); }

    // AIM: Вернуть строку прохода до данной конфигурации
    const std::string& path() const { return _prev_moves; }

    // AIM: Напечатать в STDOUT сведения о данной вершине
    void show() const;
};

//===========================================================================================================================
// CLASS: Функтор для сравнения конфигураций по взвешанному пути (глубине + эвристике Манх с ЛинКонфл)
class SlidePuzzleNodeDistanceComparator {
public:
    bool operator()(const SlidePuzzleNode& node1, const SlidePuzzleNode& node2) {
        return node1.distance() > node2.distance();
    }
};
//===========================================================================================================================
// AIM: Вычислить координату пустой клетки
void SlidePuzzle::_set_zero_pos() {
    _zero_pos = 0;

    while(_zero_pos < _configuration.size() && _configuration[_zero_pos] != 0) {
        ++_zero_pos;
    }
}

size_t manhattan_delta(const size_t& curr_elem, const size_t& curr_elem_pos, const size_t& dimension) {
    size_t curr_elem_row = (curr_elem_pos) / dimension;
    size_t curr_elem_col = (curr_elem_pos) % dimension;

    size_t curr_elem_fin_row = (curr_elem - 1) / dimension;
    size_t curr_elem_fin_col = (curr_elem - 1) % dimension;

    return abs(curr_elem_row - curr_elem_fin_row) + abs(curr_elem_col - curr_elem_fin_col);
}

// AIM: Вычислить расстояние манхеттона для данной конфигурации
void SlidePuzzle::_set_manhattan_distance() {
    _manhat_dist = 0;

    for (size_t row = 0; row < _dimension; ++row) {
        for (size_t col = 0; col < _dimension; ++col) {
            const size_t curr_elem = (*this)(row, col);

            if(curr_elem != 0) {
                size_t curr_elem_fin_row = (curr_elem - 1) / _dimension;
                size_t curr_elem_fin_col = (curr_elem - 1) % _dimension;

                _manhat_dist += abs(row - curr_elem_fin_row) + abs(col - curr_elem_fin_col);
            }
        }
    }
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM : Вернуть четность последовательности конфигурации, не учитывая 0-символ
size_t SlidePuzzle::_num_inversions() const {
    size_t num_inversions = 0;

    for (size_t first_it = 0; first_it < _configuration.size(); ++first_it) {
        for (size_t second_it = first_it + 1; second_it < _configuration.size(); ++second_it) {
            if (_configuration[first_it] != 0 && _configuration[second_it] != 0
                && _configuration[first_it] > _configuration[second_it])
                ++num_inversions;
        }
    }

    return num_inversions;
}

// AIM: Вернуть, возможно ли из данный конфигурации попасть в завершенную
bool SlidePuzzle::is_resolvable() const {
    size_t num_inversions = _num_inversions();

    if (_dimension % 2 != 0) {
        return num_inversions % 2 == 0;
    } else {
        size_t zero_row_from_bot = _dimension - (_zero_pos / _dimension) - 1;
        return (zero_row_from_bot + num_inversions) % 2 == 0;
    }
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM : Вернуть количество Линейных Строковых Конфликтов
size_t SlidePuzzle::_num_lin_row_conflicts() const {
    size_t num_lin_row_confl = 0;

    for (size_t row = 0; row < _dimension; ++row) {
        size_t curr_row_max = 0;

        for (size_t col = 0; col < _dimension; ++col) {
            size_t curr_elem = (*this)(row, col);
            size_t curr_elem_row = (curr_elem - 1) / _dimension;

            if (curr_elem != 0 && row == curr_elem_row) {
                if (curr_elem > curr_row_max){
                    curr_row_max = curr_elem;
                } else {
                    ++num_lin_row_confl;
                }
            }
        }
    }

    return num_lin_row_confl;
}

// AIM : Вернуть количество Линейных Столбцовых Конфликтов
size_t SlidePuzzle::_num_lin_col_conflicts() const {
    size_t num_lin_col_confl = 0;

    for (size_t col = 0; col < _dimension; ++col) {
        size_t curr_col_max = 0;

        for (size_t row = 0; row < _dimension; ++row) {
            size_t curr_elem = (*this)(row, col);
            size_t curr_elem_col = (curr_elem - 1) % _dimension;

            if (curr_elem != 0 && col == curr_elem_col) {
                if (curr_elem > curr_col_max) {
                    curr_col_max = curr_elem;
                } else {
                    ++num_lin_col_confl;
                }
            }
        }
    }

    return num_lin_col_confl;
}

// AIM: Вернуть евристику - расстояние манхеттона c строк-столб конфликтами для данной конфигурации
size_t SlidePuzzle::manhattan_distance_w_lin_rowcol_conflicts() const {
    size_t lrc_manhat_dist = _manhat_dist;

    lrc_manhat_dist += 2 * _num_lin_row_conflicts();
    lrc_manhat_dist += 2 * _num_lin_col_conflicts();

    return lrc_manhat_dist;
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM: Напечатать в STDOUT сведения о данной конфигурации
void SlidePuzzle::puzzle_show() const {
    for (size_t row = 0; row < _dimension; ++row) {
        for (size_t col = 0; col < _dimension; ++col) {
            std::cout << (*this)(row, col) << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

//===========================================================================================================================
// AIM: Обменять местами элементы на местах pos1 и pos2 в векторе vec
void swap(const size_t& pos1, const size_t& pos2, std::vector<size_t>& vec) {
    size_t tmp = vec[pos1];
    vec[pos1] = vec[pos2];
    vec[pos2] = tmp;
}

// AIM: Вернуть вектор конфигураций, которые могут быть получены из данный 1 ходом
void SlidePuzzleNode::get_next_slide_puzzle_nodes(std::priority_queue<SlidePuzzleNode, std::vector<SlidePuzzleNode>,
        SlidePuzzleNodeDistanceComparator>& current_nodes) const {

    size_t zero_pos_row = _zero_pos / _dimension;
    size_t zero_pos_col = _zero_pos % _dimension;

    std::vector<size_t> new_configuration(_configuration);

    // Move 0 Up
    if (zero_pos_row != 0) {
        size_t new_zero_pos = _zero_pos - _dimension;
        swap(_zero_pos, new_zero_pos, new_configuration);

        size_t swapped_elem = new_configuration[_zero_pos];
        size_t new_manhat_dist = _manhat_dist;
        new_manhat_dist += manhattan_delta(swapped_elem, _zero_pos, _dimension);
        new_manhat_dist -= manhattan_delta(swapped_elem, new_zero_pos, _dimension);

        current_nodes.emplace(_dimension, new_configuration,
                                              _prev_moves + "U", new_zero_pos, new_manhat_dist);

        swap(_zero_pos, new_zero_pos, new_configuration);
    }

    // Move 0 Down
    if (zero_pos_row + 1 != _dimension) {
        size_t new_zero_pos = _zero_pos + _dimension;
        swap(_zero_pos, new_zero_pos, new_configuration);

        size_t swapped_elem = new_configuration[_zero_pos];
        size_t new_manhat_dist = _manhat_dist;
        new_manhat_dist += manhattan_delta(swapped_elem, _zero_pos, _dimension);
        new_manhat_dist -= manhattan_delta(swapped_elem, new_zero_pos, _dimension);

        current_nodes.emplace(_dimension, new_configuration,
                                              _prev_moves + "D", new_zero_pos, new_manhat_dist);

        swap(_zero_pos, new_zero_pos, new_configuration);
    }

    // Move 0 Right
    if (zero_pos_col + 1 != _dimension) {
        size_t new_zero_pos = _zero_pos + 1;
        swap(_zero_pos, new_zero_pos, new_configuration);

        size_t swapped_elem = new_configuration[_zero_pos];
        size_t new_manhat_dist = _manhat_dist;
        new_manhat_dist += manhattan_delta(swapped_elem, _zero_pos, _dimension);
        new_manhat_dist -= manhattan_delta(swapped_elem, new_zero_pos, _dimension);

        current_nodes.emplace(_dimension, new_configuration,
                                              _prev_moves + "R", new_zero_pos, new_manhat_dist);

        swap(_zero_pos, new_zero_pos, new_configuration);
    }

    // Move 0 Left
    if (zero_pos_col != 0) {
        size_t new_zero_pos = _zero_pos - 1;
        swap(_zero_pos, new_zero_pos, new_configuration);

        size_t swapped_elem = new_configuration[_zero_pos];
        size_t new_manhat_dist = _manhat_dist;
        new_manhat_dist += manhattan_delta(swapped_elem, _zero_pos, _dimension);
        new_manhat_dist -= manhattan_delta(swapped_elem, new_zero_pos, _dimension);

        current_nodes.emplace(_dimension, new_configuration,
                                              _prev_moves + "L", new_zero_pos, new_manhat_dist);
    }
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM: Напечатать в STDOUT сведения о данной вершине
void SlidePuzzleNode::show() const {
    std::cout << "Depth: " << depth() << std::endl;
    std::cout << "Path:  " << path() << std::endl;
    puzzle_show();
}

//===========================================================================================================================

//===========================================================================================================================
// CLASS: Функтор для уникального сравнения конфигураций для set
class SlidePuzzleNodeUniqueComparator {
public:
    bool operator()(const SlidePuzzleNode& node1, const SlidePuzzleNode& node2) {

        for (size_t row = 0; row < node1.dimension(); ++row) {
            for (size_t col = 0; col < node1.dimension(); ++col) {
                if (node1(row, col) < node2(row, col))
                    return true;
                if (node1(row, col) > node2(row, col))
                    return false;
            }
        }

        return false;
    }
};

//===========================================================================================================================
// AIM: Вернуть по начальной конфигурации строку
// "-1" если данная конфигурация не может быть решенной
// "N" а на следующей строке "W"
// где N - глубина минимального решения,
// а   W - слово, описывающее решение в терминах ходов 0-элемента по матрице
std::string slide_puzzle_astar_solve(const size_t& dimension,
                                     const std::vector<size_t>& start_configuration) {

    std::set<SlidePuzzleNode, SlidePuzzleNodeUniqueComparator> visited_nodes;
    std::priority_queue<SlidePuzzleNode, std::vector<SlidePuzzleNode>,
            SlidePuzzleNodeDistanceComparator> current_nodes;

    SlidePuzzleNode curr_node(dimension, start_configuration);

    if (!curr_node.is_resolvable()) {
        return "-1\n";
    }


    while(!curr_node.is_finished()) {
        if (visited_nodes.find(curr_node) == visited_nodes.end()) {
            curr_node.get_next_slide_puzzle_nodes(current_nodes);
            visited_nodes.insert(curr_node);
        }

        curr_node = current_nodes.top();
        current_nodes.pop();
    }


    std::stringstream result_buffer;
    result_buffer << curr_node.depth() << std::endl << curr_node.path() << std::endl;

    return result_buffer.str();
}

//===========================================================================================================================
// AIM: Считать из puzzle.in конфигурацию
void read_start_configuration(std::vector<size_t>& start_configuration) {
    std::ifstream fin("puzzle.in");

    std::string line = "";
    while(std::getline(fin, line)) {
        std::stringstream curr_line_ss(line);
        size_t elem = 0;

        while(!curr_line_ss.eof()) {
            if (std::isdigit(curr_line_ss.peek())) {
                curr_line_ss >> elem;
                start_configuration.push_back(elem);
            } else {
                curr_line_ss.get();
            }
        }

    }

    fin.close();
}

// AIM: Вывести в puzzle.out решение конфигурации
void output_solution(const std::string& solution) {
    std::ofstream fout("puzzle.out");

    fout << solution;

    fout.close();
}

// AIM: Вычисление минимального решения конфигурации N-нашек
int main() {
    std::vector<size_t> start_configuration;

    read_start_configuration(start_configuration);
    size_t dimension = sqrt(start_configuration.size() + 1);

    std::string solution = slide_puzzle_astar_solve(dimension, start_configuration);

    output_solution(solution);

    return 0;
}