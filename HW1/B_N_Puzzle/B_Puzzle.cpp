#include "slide_puzzle/slide_puzzle_astar_solve.cpp"

#include <fstream>
#include <math.h>

// AIM: Считать из input конфигурацию
void read_start_configuration(std::ifstream& input, std::vector<size_t>& start_configuration) {
    std::string line = "";
    while(std::getline(input, line)) {
        std::stringstream curr_line_ss(line);
        size_t elem = 0;

        while(!curr_line_ss.eof()) {
            if (std::isdigit(curr_line_ss.peek())) {
                curr_line_ss >> elem;
                start_configuration.push_back(elem);
            } else {
                curr_line_ss.get();
            }
        }

    }

// AIM: Вывести в output решение конфигурации
void output_solution(std::ofstream& output, const std::string& solution) {
    output << solution;
}

// AIM: Вычисление минимального решения конфигурации N-нашек
int main() {
    std::ifstream fin("puzzle.in");
    std::ofstream fout("puzzle.out");
    std::vector<size_t> start_configuration;

    read_start_configuration(fin, start_configuration);
    size_t dimension = static_cast<size_t>(sqrt(start_configuration.size() + 1));

    std::string solution = slide_puzzle_astar_solve(dimension, start_configuration);

    output_solution(fout, solution);

    fin.close();
    fout.close();
    return 0;
}