#include "A_Trade_lib.h"

constexpr double WEIGHT_INTO_ITSELF  = 1.0;
constexpr double WEIGHT_DOESNT_EXIST = -1.0;

// AIM: Считать данные из потока STDIN, при этом добавляя супервершину в 0 и
// преобразовывая при этом {weight --> -ln(weight)}
const DenseGraph read_arbitrage() {
    size_t num_vertices = 0;
    double weight = 0.0;
    std::cin >> num_vertices;

    DenseGraph graph(num_vertices + 1, true);
    for (size_t v = 0; v < graph.vertices(); ++v) {
        graph.insert(Edge(0, v, std::log(WEIGHT_DOESNT_EXIST)));
        graph.insert(Edge(v, 0, std::log(1.0)));
    }
    for (size_t v = 1; v < graph.vertices(); ++v) {
        for (size_t e = 1; e < graph.vertices(); ++e) {
            if (v == e) {
                weight = WEIGHT_INTO_ITSELF;
            }
            else {
                std::cin >> weight;
            }
            graph.insert(Edge(v, e, (-1) * std::log(weight)));
        }
    }

    return graph;
}

// AIM: Используя аналог Алгоритма Беллмона-Форда, выяснить, есть ли мультипликативные циклы длины < 1
bool check_for_cycles_less_than_one(const DenseGraph& graph) {
    std::vector<double> distance(graph.vertices(), std::numeric_limits<double>::max());
    double WEIGHT_IF_DOESNT_EXIST = log(-1.0);

    distance[0] = 0.0;
    for (size_t iterations = 0; iterations + 1 < graph.vertices(); ++iterations) {
        bool has_relaxed = false;

        for (size_t v = 0; v < graph.vertices(); ++v) {
            for (size_t e = 0; e < graph.vertices(); ++e) {

                double dist_thru_e = distance[e] + graph.edge_weight(v, e);

                if ((graph.edge_weight(v, e) != WEIGHT_IF_DOESNT_EXIST) && distance[v] > dist_thru_e) {
                    distance[v] = dist_thru_e;
                    has_relaxed = true;
                }
            }
        }

        if (!has_relaxed)
            break;
    }

    for (size_t v = 0; v < graph.vertices(); ++v) {
        for (size_t e = 0; e < graph.vertices(); ++e) {

            double dist_thru_e = distance[e] + graph.edge_weight(v, e);

            if ((graph.edge_weight(v, e) != WEIGHT_IF_DOESNT_EXIST) && distance[v] > dist_thru_e) {
                return true;
            }
        }
    }

    return false;
}