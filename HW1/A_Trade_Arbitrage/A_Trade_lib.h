#ifndef MIPT_3SEM_ALG_A_TRADE_LIB_H_H
#define MIPT_3SEM_ALG_A_TRADE_LIB_H_H

#include <graph/graph.h>

// AIM: Считать данные из потока STDIN, при этом добавляя супервершину в 0 и
// преобразовывая при этом {weight --> -ln(weight)}
const DenseGraph read_arbitrage();

// AIM: Используя аналог Алгоритма Беллмона-Форда, выяснить, есть ли мультипликативные циклы длины < 1
bool check_for_cycles_less_than_one(const DenseGraph& graph);

#endif //MIPT_3SEM_ALG_A_TRADE_LIB_H_H
