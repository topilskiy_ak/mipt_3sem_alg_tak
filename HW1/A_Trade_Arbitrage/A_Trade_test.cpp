#include <gtest/gtest.h>
#include "A_Trade_lib.h"

TEST(A_TradeTests, Test_read_arbitrage) {
    std::cout << "EXPECTED INPUT:" << std::endl;
    std::cout << "2 5 9" << std::endl;
    const DenseGraph graph = read_arbitrage();
    EXPECT_EQ(graph.to_string(), "nan nan nan  0 -0 -1.60944  0 -2.19722 -0  ");
}

TEST(A_TradeTests, Test_check_for_cycles_less_than_one) {
    std::cout << "EXPECTED INPUT:" << std::endl;
    std::cout << "2 10.0 0.09" << std::endl;
    DenseGraph graph = read_arbitrage();
    EXPECT_EQ(graph.to_string(), "nan nan nan  0 -0 -2.30259  0 2.40795 -0  ");
    EXPECT_FALSE(check_for_cycles_less_than_one(graph));

    std::cout << "EXPECTED INPUT:" << std::endl;
    std::cout << "4 32.1 1.50 78.66 0.03  0.04 2.43 0.67 21.22 51.89 0.01 -1 0.02" << std::endl;
    graph = read_arbitrage();
    EXPECT_EQ(graph.to_string(), "nan nan nan nan nan  0 -0 -3.46886 -0.405465 -4.36513  0 3.50656 -0 3.21888 -0.887891  0 0.400478 -3.05494 -0 -3.94913  0 4.60517 -nan 3.91202 -0  ");
    EXPECT_TRUE(check_for_cycles_less_than_one(graph));

    std::cout << "EXPECTED INPUT:" << std::endl;
    std::cout << "3 0.67 -1 -1 78.66 0.02 -1" << std::endl;
    graph = read_arbitrage();
    EXPECT_EQ(graph.to_string(), "nan nan nan nan  0 -0 0.400478 -nan  0 -nan -0 -4.36513  0 3.91202 -nan -0  ");
    EXPECT_TRUE(check_for_cycles_less_than_one(graph));
}