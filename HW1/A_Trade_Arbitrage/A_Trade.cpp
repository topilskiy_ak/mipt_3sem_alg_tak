#include "A_Trade_lib.h"

// AIM: Вычисление возможности арбитража на данной матрице смежностей
int main() {
  const DenseGraph graph = read_arbitrage();

  if (check_for_cycles_less_than_one(graph))
    std::cout << "YES" << std::endl;
  else
    std::cout << "NO" << std::endl;
  return 0;
}