#include <iostream>
#include <memory>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <valarray>
#include <cassert>
#include <initializer_list>
#include <climits>
#include <bitset>
#include <algorithm>

// BAD BIT OF CODING - KNOWN PROBLEM - YANDEX VERSION IS OUTDATED
using namespace std;

//===========================================================================================================================
// Класс Среза
template <typename T>
class slice_iter {
private:
    typedef vector<T> VT;

    VT& v;   // Вектор, к которому применяется Срез
    slice s; // Информация о самом Срезе
    // AIM: Вернуть <i>-ый элемент данного Среза
    typename VT::reference ref(size_t i) const { return (v)[s.start() + i * s.stride()]; }
public:
    typedef T value_type;
    typedef typename       VT::reference       reference;
    typedef typename VT::const_reference const_reference;

    // CONSTRUCTOR INPUT: <v> - Вектор Среза, <s> - информация о данном Срезе
    slice_iter(VT& v, slice s) : v(v), s(s) {}
    // CONSTRUCTOR INPUT: <si> - Срез, копию которого надо сделать
    slice_iter(const slice_iter& si) : v(si.v), s(si.s) {}

    // CONST CONSTRUCTOR FUNCTION: Вернуть константный Срез, созданный на базе <v> Срезом <s>
    static const slice_iter ct(const VT& v, slice s) { return slice_iter(const_cast<VT&>(v), s); }

    // AIM: Вернуть кол-во элементов в данном Срезе
    size_t size() const { return s.size(); }
    // AIM: Вернуть конст-ссылку на <i>-ый элемент Среза
    const_reference operator[](size_t i) const { return ref(i); }
    // AIM: Вернуть ссылку на <i>-ый элемент Среза
    reference operator[](size_t i)       { return ref(i); }

    // AIM: Вернуть результат скалярного произведения данного Среза со Срезом <si>
    T operator*(const slice_iter& si) const;
    // AIM: Умножить данный Среза на <mul>
    // OUT: Ссылка на данный Срез
    const slice_iter& operator*=(const T& mul);
    // AIM: Векторно вычесть из данного Среза Срез <si>
    // OUT: Ссылка на данный Срез
    const slice_iter& operator-=(const slice_iter& si);
};
//---------------------------------------------------------------------------------------------------------------------------
// AIM: Вернуть результат скалярного произведения данного Среза со Срезом <si>
template<typename T>
T slice_iter<T>::operator*(const slice_iter& si) const
{
    assert(size() == si.size());
    // Возвращаемый результат
    T res = 0;
    for (size_t i = 0, s = size(); i < s; i++) {
        res += ref(i) * si[i];
    }
    return res;
}

// AIM: Умножить данный Среза на <mul>
// OUT: Ссылка на данный Срез
template<typename T>
const slice_iter<T>& slice_iter<T>::operator*=(const T& mul)
{
    for (size_t i = 0, s = size(); i < s; i++) {
        (*this)[i] *= mul;
    }
    return *this;
}

// AIM: Векторно вычесть из данного Среза Срез <si>
// OUT: Ссылка на данный Срез
template<typename T>
const slice_iter<T>& slice_iter<T>::operator-=(const slice_iter& si)
{
    assert(size() == si.size());
    for (size_t i = 0, s = size(); i < s; i++) {
        (*this)[i] -= si[i];
    }
    return *this;
}
//===========================================================================================================================
//===========================================================================================================================
// Класс Матриц
template <typename T>
class matrix {
private:
    size_t _h;     // Высота Матрицы
    size_t _w;     // Ширина Матрицы
    vector<T> _m;  // Содержимое Матрицы

    // AIM: Поменять местами строки с нмерами <row_no1> и <row_no2>
    void swap_rows(size_t row_no1, size_t row_no2);
    // AIM: Вечесть из строки <row_no1> строку <row_no2> умноженную на <coeff>
    void row1_minus_row2_mul_coeff(size_t row_no1, size_t row_no2, const T& coeff);

public:
    using vec = slice_iter<T>;
    using value_type = vec;
    using  reference = vec;
    using const_reference = const vec;

    // CONSTRUCTOR: <h> - высота Матрицы, <w> - ширина Матрицы
    matrix(size_t h = 0, size_t w = 0) : _h(h), _w(w), _m(w * h) {}
    // CONSTRUCTOR: <source> - Матрица, с которой делается копия
    matrix(const matrix& source) : _h(source._h), _w(source._w), _m(source._m) {}
    // CONSTRUCTOR: <l> - список списков эелементов, которыми надо инициализировать Матрицу
    matrix(initializer_list<initializer_list<T>> l);

    // AIM: Вернуть кол-во строк в Матрице
    size_t h()    const { return _h; }
    // AIM: Вернуть кол-во столбцов в Матрице
    size_t w()    const { return _w; }
    // AIM: Вернуть кол=во элементов в Матрице
    size_t size() const { return _h * _w; }
    // AIM: Вернуть единичную по умножению Матрицу к данной
    const matrix identity_matrix() const;

    // Вернуть ссылку на <i, j>-ый элемент Матрицы
    typename vec::reference       operator () (size_t i, size_t j)       { return _m[_w * i + j]; }
    // Вернуть конст-ссылку на <i, j>-ый элемент Матрицы
    typename vec::const_reference operator () (size_t i, size_t j) const { return _m[_w * i + j]; }
    // Вернуть ссылку на <i>-ую строку Матрицы
    vec operator[] (size_t i)       { return row(i); }
    // Вернуть конст-ссылку на <i>-ую строку Матрицы
    const vec operator[] (size_t i) const { return row(i); }

    // Вернуть ссылку на <x>-ый столбец Матрицы
    vec col(size_t x)       { return     vec(_m, slice(x, _h, _w)); }
    // Вернуть конст-ссылку на <x>-ый столбец Матрицы
    const vec col(size_t x) const { return vec::ct(_m, slice(x, _h, _w)); }
    // Вернуть ссылку на <y>-ую строку Матрицы
    vec row(size_t y)       { return     vec(_m, slice(y * _w, _w, 1)); }
    // Вернуть конст-ссылку на <y>-ую строку Матрицы
    const vec row(size_t y) const { return vec::ct(_m, slice(y * _w, _w, 1)); }

    // AIM: Оператор присвоения данной Матрице значения Матрицы <m>
    // OUT: Конст-ссылка на данную Матрицу
    const matrix& operator=(const matrix& m);
    // AIM: Оператор перемещения данной Матрице значения Матрицы <m>
    // OUT: Конст-ссылка на данную Матрицу
    const matrix& operator=(const matrix&& m);

    // AIM: Оператор прибавления к данной Матрице по-элементно значения Матрицы <m>
    // OUT: Конст-ссылка на данную Матрицу
    const matrix& operator+=(const matrix& m);
    // AIM: Оператор по-элементного суммирования данной Матрицы с Матрицой <m>
    // OUT: Ссылка на Матрицу суммы
    const matrix   operator+(const matrix& m) const;


    // AIM: Оператор обращения данной Матрицы в плане оперции сложения
    // OUT: Конст-ссылка на данную Матрицу
    const matrix&  operator-();
    // AIM: Оператор вычитания из данной Матрице по-элементно значения Матрицы <m>
    // OUT: Конст-ссылка на данную Матрицу
    const matrix& operator-=(const matrix& m);
    // AIM: Оператор по-элементной разницы данной Матрицы и Матрицы <m>
    // OUT: Ссылка на Матрицу разницы
    const matrix   operator-(const matrix& m) const;

    // AIM: Оператор умножения данной Матрицы на Матрицу <m>
    // OUT: Конст-ссылка на данную Матрицу
    const matrix& operator*=(const matrix& m);
    // AIM: Оператор произведения данной Матрицы на Матрицу <m>
    // OUT: Ссылка на Матрицу произведения
    matrix   operator*(const matrix& m) const;

    // AIM: Оператор деления данной Матрицы на Матрицу <m>
    // OUT: Конст-ссылка на данную Матрицу
    const matrix& operator/=(const matrix& m);
    // AIM: Оператор деления данной Матрицы на Матрицу <m>
    // OUT: Ссылка на Матрицу частного
    matrix   operator/(const matrix& m) const;

    // AIM: Вернуть данную Матрицу, возведенную в целую степень <exp>
    matrix exp(const int& exp) const;
    // AIM: Вернуть транспонированную данную Матрицу
    matrix transpond() const;
    // AIM: Вернуть обратную к данной Матрицу
    matrix inverse()   const;

    // AIM: Вернуть след данной Матрицы
    T trace() const;
    // AIM: Вернуть определитель данной Матрицы
    T det()   const;
};
//---------------------------------------------------------------------------------------------------------------------------
// CONSTRUCTOR: <l> - список списков эелементов, которыми надо инициализировать Матрицу
template <typename T>
matrix<T>::matrix(initializer_list<initializer_list<T>> l)
{
    _h = l.size();
    _w = _h > 0 ? l.begin()->size() : 0;
    _m.resize(_w * _h);
    // Итератор по вектору Матрицы
    size_t pos = 0;
    for (initializer_list<T> const& rowList : l)
    {
        assert(rowList.size() == _w);
        for (const T& value : rowList)
        {
            _m[pos] = value;
            pos++;
        }
    }
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM: Поменять местами строки с нмерами <row_no1> и <row_no2>
template <typename T>
void matrix<T>::swap_rows(size_t row_no1, size_t row_no2)
{
    assert(row_no1 < _h && row_no2 < _h);
    // Первая строка (Срез)
    auto row1 = row(row_no1);
    // Первая строка (Срез)
    auto row2 = row(row_no2);
    for (size_t i = 0; i < _w; ++i)
    {
        // Временное запоминание значения
        auto tmp = row1[i];
        row1[i] = row2[i];
        row2[i] = tmp;
    }
}

// AIM: Вечесть из строки <row_no1> строку <row_no2> умноженную на <coeff>
template <typename T>
void matrix<T>::row1_minus_row2_mul_coeff(size_t row_no1, size_t row_no2, const T& coeff)
{
    // Первая строка (Срез)
    auto row1 = row(row_no1);
    // Вторая строка (Срез)
    auto row2 = row(row_no2);
    for (size_t i = 0; i < _w; ++i)
    {
        row1[i] -= row2[i] * coeff;
    }
}
//---------------------------------------------------------------------------------------------------------------------------
// AIM: Вернуть единичную по умножению Матрицу к данной
template <typename T>
const matrix<T> matrix<T>::identity_matrix() const
{
    assert(_h == _w);
    // Возвращаемая единичная Матрица
    auto id = matrix(_h, _w);
    for (size_t i = 0; i < _h; ++i)
        id(i, i) = 1;
    return id;
}
//---------------------------------------------------------------------------------------------------------------------------
// AIM: Оператор присвоения данной Матрице значения Матрицы <m>
// OUT: Конст-ссылка на данную Матрицу
template <typename T>
const matrix<T>& matrix<T>::operator=(const matrix& m)
{
    _w = m._w;
    _h = m._h;
    _m = m._m;
    return *this;
}

// AIM: Оператор перемещения данной Матрице значения Матрицы <m>
// OUT: Конст-ссылка на данную Матрицу
template <typename T>
const matrix<T>& matrix<T>::operator=(const matrix&& m)
{
    _w = m._w;
    _h = m._h;
    _m = move(m._m);
    return *this;
}
//---------------------------------------------------------------------------------------------------------------------------
// AIM: Оператор прибавления к данной Матрице по-элементно значения Матрицы <m>
// OUT: Конст-ссылка на данную Матрицу
template <typename T>
const matrix<T>& matrix<T>::operator+=(const matrix& m)
{
    assert(_h == m._h && _w == m._w);
    for (size_t i = 0; i < _h; i++)
    {
        for (size_t j = 0; j < _w; j++)
        {
            (*this)(i, j) += m[i][j];
        }
    }
    return *this;
}

// AIM: Оператор по-элементного суммирования данной Матрицы с Матрицой <m>
// OUT: Ссылка на Матрицу суммы
template <typename T>
const matrix<T> matrix<T>::operator+(const matrix& m) const
{
    // Временная копия данной Матрицы
    auto tmp = matrix(*this);
    tmp += m;
    return tmp;
}
//---------------------------------------------------------------------------------------------------------------------------
// AIM: Оператор обращения данной Матрицы в плане оперции сложения
// OUT: Конст-ссылка на данную Матрицу
template <typename T>
const matrix<T>& matrix<T>::operator-()
{
    for (size_t i = 0; i < _h; i++)
    {
        for (size_t j = 0; j < _w; j++)
        {
            (*this)(i, j) *= -1;
        }
    }
    return *this;
}

// AIM: Оператор вычитания из данной Матрице по-элементно значения Матрицы <m>
// OUT: Конст-ссылка на данную Матрицу
template <typename T>
const matrix<T>& matrix<T>::operator-=(const matrix& m)
{
    assert(_h == m._h && _w == m._w);
    for (size_t i = 0; i < _h; i++)
    {
        for (size_t j = 0; j < _w; j++)
        {
            (*this)(i, j) -= m[i][j];
        }
    }
    return *this;
}

// AIM: Оператор по-элементной разницы данной Матрицы и Матрицы <m>
// OUT: Ссылка на Матрицу разницы
template <typename T>
const matrix<T> matrix<T>::operator-(const matrix& m) const
{
    // Временная копия данной Матрицы
    auto tmp = matrix(*this);
    tmp -= m;
    return tmp;
}
//---------------------------------------------------------------------------------------------------------------------------
// AIM: Оператор умножения данной Матрицы на Матрицу <m>
// OUT: Конст-ссылка на данную Матрицу
template <typename T>
const matrix<T>& matrix<T>::operator*=(const matrix& m)
{
    assert(_w == m._h);
    // Матрица произведения
    auto prod = matrix(_h, m._w);
    for (size_t i = 0, h = prod.h(); i < h; i++)
    {
        for (size_t j = 0, w = prod.w(); j < w; j++)
        {
            prod(i, j) = row(i) * m.col(j);
        }
    }
    *this = prod;
    return *this;
}

// AIM: Оператор произведения данной Матрицы на Матрицу <m>
// OUT: Ссылка на Матрицу произведения
template <typename T>
matrix<T> matrix<T>::operator*(const matrix& m) const
{
    auto tmp = matrix(*this);
    tmp *= m;
    return tmp;
}

// AIM: Оператор деления данной Матрицы на Матрицу <m>
// OUT: Конст-ссылка на данную Матрицу
template <typename T>
const matrix<T>& matrix<T>::operator/=(const matrix& m)
{
    *this = (*this) * m.inverse();
    return *this;
}

// AIM: Оператор деления данной Матрицы на Матрицу <m>
// OUT: Ссылка на Матрицу частного
template <typename T>
matrix<T> matrix<T>::operator/(const matrix& m) const
{
    auto tmp = matrix(*this);
    tmp /= m;
    return tmp;
}
//---------------------------------------------------------------------------------------------------------------------------
// AIM: Вернуть данную Матрицу, возведенную в целую степень <exp>
template <typename T>
matrix<T> matrix<T>::exp(const int& exp) const
{
    assert(_w == _h);
    // Возвращаемая Матрица - степень данной
    auto res = matrix();
    if (exp == 0)
        return identity_matrix();
    else if (exp < 0)
        res = inverse();
    else
        res = *this;

    // Число Бит в числе типа <int>
    const static size_t INT_BIT = sizeof(int) * CHAR_BIT;
    // Двоичное разложение числа |exp|
    bitset<INT_BIT> exp_bitset(abs(exp));
    // Итератор по двоичной репрезентации exp
    int i = INT_BIT - 1;
    // Итерируем до первого ненулевого бита
    for (; i >= 0 && !exp_bitset[i]; --i);
    --i;

    // Запоминание изначального значения, которое возводится в степень
    auto res_core = matrix(res);
    for (; i >= 0; --i)
    {
        res *= res;
        if (exp_bitset[i])
            res *= res_core;
    }
    return res;
}
//---------------------------------------------------------------------------------------------------------------------------
// AIM: Вернуть транспонированную данную Матрицу
template <typename T>
matrix<T> matrix<T>::transpond() const
{
    // Возвращаемая транспонированная Матрица
    auto m = matrix(_w, _h);
    for (int i = 0; i < _h; i++) {
        for (int j = 0; j < _w; j++)
            m(j, i) = (*this)(i, j);
    }
    return m;
}
//---------------------------------------------------------------------------------------------------------------------------
// AIM: Вернуть обратную к данной Матрицу
template <typename T>
matrix<T> matrix<T>::inverse() const
{
    assert(_w == _h && det() != 0);
    // Копия данной Матрицы
    auto copy = matrix(*this);
    // Возвращаемая обратная Матрица
    auto inverse = identity_matrix();

    // Ход вниз обращения методом Гаусса-Жордана
    for (size_t diag = 0; diag + 1 < _h; ++diag)
    {
        // Итератор по строкам Матрицы для нахождения строки без нулевого элемента на месте <diag> в строке
        size_t row = diag;
        for (; row < _h && (copy.row(row))[diag] == 0; ++row);
        if (row != diag)
        {
            copy.swap_rows(row, diag);
            inverse.swap_rows(row, diag);
        }

        for (row = diag + 1; row < _h; ++row)
        {
            if (copy(row, diag) != 0)
            {
                // Коэффициент для привидения недиагональных элементов <row, diag> к нулю
                T coeff = (copy(row, diag) / copy(diag, diag));
                copy.row1_minus_row2_mul_coeff(row, diag, coeff);
                inverse.row1_minus_row2_mul_coeff(row, diag, coeff);
            }
        }
    }

    // Привидение диагональных элементов к единичным
    for (size_t diag = 0; diag < _h; ++diag)
    {
        // Коэффициент для деления строк
        T coeff = static_cast<T>(1) / copy(diag, diag);
        copy.row(diag) *= coeff;
        inverse.row(diag) *= coeff;
    }

    // Ход вверх обращения методом Гаусса-Жордана
    for (size_t diag = _h - 1; diag > 0; --diag)
    {
        for (int row = diag - 1; row >= 0; --row)
        {
            if (copy(row, diag) != 0)
            {
                // Коэффициент для привидения недиагональных элементов <row, diag> к нулю
                T coeff = copy(row, diag);
                copy.row1_minus_row2_mul_coeff(row, diag, coeff);
                inverse.row1_minus_row2_mul_coeff(row, diag, coeff);
            }
        }
    }

    return inverse;
}
//---------------------------------------------------------------------------------------------------------------------------
// AIM: Вернуть след данной Матрицы
template <typename T>
T matrix<T>::trace() const
{
    assert(_w == _h);
    // Возвращаемый след Матрицы
    T tr(0);
    for (size_t i = 0; i < _h; i++)
    {
        tr += (*this)(i, i);
    }
    return tr;
}
//---------------------------------------------------------------------------------------------------------------------------
// AIM: Вернуть определитель данной Матрицы
template <typename T>
T matrix<T>::det() const
{
    assert(_w == _h);
    // Копия данной Матрицы
    auto copy = matrix(*this);
    // Возвращаемый определитель матрицы
    T det(1);

    // Поиск определителя сведением Матрицы к Верхне-Треугольному виду Методом Гаусса-Жордана
    for (size_t diag = 0; diag + 1 < _h; ++diag)
    {
        // Итератор по строкам Матрицы для нахождения строки без нулевого элемента на месте <diag> в строке
        size_t row = diag;
        for (; row < _h && (copy.row(row))[diag] == 0; ++row);
        if (row == _h)
        {
            det = 0;
            return det;
        }
        if (row != diag)
        {
            det *= -1;
            copy.swap_rows(row, diag);
        }

        for (row = diag + 1; row < _h; ++row)
        {
            // Коэффициент для привидения недиагональных элементов (ниже диагонали) <row, diag> к нулю
            T coeff = copy(row, diag) / copy(diag, diag);
            if (copy(row, diag) != 0)
                copy.row1_minus_row2_mul_coeff(row, diag, coeff);
        }
    }

    for (size_t i = 0; i < _h; ++i)
        det *= copy(i, i);

    return det;
}
//---------------------------------------------------------------------------------------------------------------------------
// AIM: Вывесту в STDOUT Матрицу
template <typename T>
ostream& operator << (ostream& os, const matrix<T>& m)
{
    for (size_t i = 0, h = m.h(); i < h; i++)
    {
        for (size_t j = 0, w = m.w(); j < w; j++)
        {
            std::cout << setw(3) << m[i][j] << ", ";
        }
        cout << endl;
    }
    return os;
}
//===========================================================================================================================

//===========================================================================================================================
// Структура Ребра Графа
struct Edge {
    static constexpr double BASE_WEIGHT = 1.0;  // Вес невзвешанного ребра

    size_t v1;       // Вершина Начала Ребра
    size_t v2;       // Вершина Конца Ребра
    double weight;  // Вес Ребра

    Edge(size_t v = 0, size_t e = 0, double w = BASE_WEIGHT) : v1(v), v2(e), weight(w) {}
};
//---------------------------------------------------------------------------------------------------------------------------
// Абстрактный Класс Графа
class Graph {
protected:
    bool _is_directed = false; // Является ли Граф Ориентированным
    size_t _num_vertices = 0;  // Число вершин в Графе
    size_t _num_edges = 0;     // Число ребер в Графе

public:
    // STANDART CONSTRUCTOR:
    Graph(const bool is_directed = false, const size_t num_vertices = 0, const size_t num_edges = 0) :
            _is_directed(is_directed), _num_vertices(num_vertices), _num_edges(num_edges) {}

    // AIM: Вернуть кол-во вершин в Графе
    size_t vertices() const { return _num_vertices; }

    // AIM: Вернуть кол-во ребер в Графе
    size_t edges() const { return _num_edges; }

    // AIM: Вернеть является ли Граф Ориентированным
    bool directed() const { return _is_directed; }

    // AIM: Вставить ребро edge в Граф
    virtual void insert(const Edge& edge) = 0;

    // AIM: Удалить ребро edge из Графа (если таковое существует)
    virtual void remove(const Edge& edge) = 0;

    // AIM: Проверить, существует ли ребро между (v,e)
    virtual bool edge(size_t v, size_t e) const = 0;

    // AIM: Вернуть вес существующего ребра между (v,e)
    // WAR: Вернуть DBL_MAX, если ребра не существует
    virtual double edge_weight(size_t v, size_t e) const = 0;

    // AIM: Вывести граф в STDOUT
    virtual void show() const = 0;

    // STANDART DESTRUCTOR:
    virtual ~Graph() {}
};
//===========================================================================================================================
// Граф на Матрице смежности
class DenseGraph : public Graph {
private:
    // Тип элементов матрицы смежности
    struct AdjElem {
        bool exists = false;                            // существование ребра
        double weight = numeric_limits<double>::max();  // его вес (DBL_MAX при его отсутствии)

        // AIM: По Ребру edge строит элемент матрицы в клетке (v, e)
        void operator=(const Edge& edge) { exists = true; weight = edge.weight; }
        // AIM: Обнулить данный элемент
        void reset() { exists = false; weight = numeric_limits<double>::max(); }
    };

    // Тип матрицы которая хранит Ребра
    using AdjMatrix = matrix<AdjElem>;
    // Матрица Ребер Графа
    AdjMatrix _adj;

public:
    // CONSTRUCTOR: Создание оболочки для матрицы смежностей графа (is_directed-ориентиров) с v вершинами
    DenseGraph(size_t v, bool is_directed = false) : _adj(v, v) {
        _is_directed = is_directed;
        _num_vertices = v;
        _num_edges = 0;
    }

    // AIM: Вставить ребро edge в Граф
    void insert(const Edge& edge) {
        size_t v(edge.v1), e(edge.v2);
        if (!_adj[v][e].exists) {
            _num_edges++;
            _adj[v][e].exists = true;
            _adj[v][e] = edge;
            if (!directed()) _adj[e][v] = edge;
        }
    }

    // AIM: Удалить ребро edge из Графа (если таковое существует)
    void remove(const Edge& edge) {
        size_t v(edge.v1), e(edge.v2);
        if (_adj[v][e].exists) {
            _num_edges--;
            _adj[v][e].reset();
            if (!directed()) _adj[e][v].reset();
        }
    }

    // AIM: Проверить, существует ли ребро между (v,e)
    bool edge(size_t v, size_t e) const { return _adj[v][e].exists; }

    // AIM: Вернуть вес существующего ребра между (v,e)
    // WAR: Вернуть DBL_MAX, если ребра не существует
    double edge_weight(size_t v, size_t e) const { return _adj[v][e].weight; }

    // AIM: Вывести граф в STDOUT
    void show() const {
        for (size_t i = 0; i < _num_vertices; i++) {
            for (size_t j = 0; j < _num_vertices; j++) {
                std::cout << setw(3) << _adj[i][j].weight << ", ";
            }
            cout << endl;
        }
        cout << endl;
    }

    // STANDART DESTRUCTOR:
    ~DenseGraph() {}
};

// AIM: Считать данные из потока STDIN, при этом добавляя супервершину в 0 и
// преобразовывая при этом {weight --> 1 / weight}
std::shared_ptr<DenseGraph> read_arbitrage() {
    size_t num_vertices = 0;
    static double WEIGHT_INTO_ITSELF  = 1.0;
    static double WEIGHT_DOESNT_EXIST = -1.0;
    double weight = 0.0;
    std::cin >> num_vertices;

    std::shared_ptr<DenseGraph> graph = std::make_shared<DenseGraph>(DenseGraph(num_vertices + 1, true));
    for (size_t v = 0; v < graph->vertices(); ++v) {
        graph->insert(Edge(0, v, log(WEIGHT_DOESNT_EXIST)));
        graph->insert(Edge(v, 0, log(1.0)));
    }
    for (size_t v = 1; v < graph->vertices(); ++v) {
        for (size_t e = 1; e < graph->vertices(); ++e) {
            if (v == e) {
                weight = WEIGHT_INTO_ITSELF;
            }
            else {
                cin >> weight;
            }
            graph->insert(Edge(v, e, (-1) * log(weight)));
        }
    }

    return graph;
}

// AIM: Используя аналог Алгоритма Беллмона-Форда, выяснить, есть ли мультипликативные циклы длины < 1
bool check_for_cycles_less_than_one(shared_ptr<DenseGraph>& graph) {
    vector<double> distance(graph->vertices(), std::numeric_limits<double>::max());
    double WEIGHT_IF_DOESNT_EXIST = log(-1.0);

    distance[0] = 0.0;
    for (size_t iterations = 0; iterations + 1 < graph->vertices(); ++iterations) {
        bool has_relaxed = false;

        for (size_t v = 0; v < graph->vertices(); ++v) {
            for (size_t e = 0; e < graph->vertices(); ++e) {

                double dist_thru_e = distance[e] + graph->edge_weight(v, e);

                if ((graph->edge_weight(v, e) != WEIGHT_IF_DOESNT_EXIST) && distance[v] > dist_thru_e) {
                    distance[v] = dist_thru_e;
                    has_relaxed = true;
                }
            }
        }

        if (!has_relaxed)
            break;
    }

    for (size_t v = 0; v < graph->vertices(); ++v) {
        for (size_t e = 0; e < graph->vertices(); ++e) {

            double dist_thru_e = distance[e] + graph->edge_weight(v, e);

            if ((graph->edge_weight(v, e) != WEIGHT_IF_DOESNT_EXIST) && distance[v] > dist_thru_e) {
                return true;
            }
        }
    }

    return false;
}

int main() {
    std::shared_ptr<DenseGraph> graph = read_arbitrage();

    if (check_for_cycles_less_than_one(graph))
        cout << "YES" << endl;
    else
        cout << "NO" << endl;
    return 0;
}