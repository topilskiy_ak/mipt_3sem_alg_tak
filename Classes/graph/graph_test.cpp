#include <gtest/gtest.h>
#include "graph.h"

//===========================================================================================================================
TEST(EdgeTests, TestConstructor){
    Edge e1(10, 20, -900);
    Edge e2(8, 9, 200.9);
    Edge e3(900, 762, -9212.213);

    EXPECT_EQ(e1.v1, 10);
    EXPECT_EQ(e2.v2, 9);
    EXPECT_EQ(e3.weight, -9212.213);
}

//===========================================================================================================================
TEST(DenseGraphTests, TestConstructor) {
    DenseGraph dg1;
    DenseGraph dg2(20, true);
}

TEST(DenseGraphTests, TestEdgeOperations) {
    DenseGraph dgu(5, false);

    EXPECT_FALSE(dgu.directed());
    EXPECT_EQ(dgu.vertices(), 5);
    EXPECT_EQ(dgu.edges(), 0);

    EXPECT_FALSE(dgu.has_edge(0, 2));
    EXPECT_EQ(dgu.edge_weight(4, 1), std::numeric_limits<double>::max());

    dgu.insert(Edge(1, 1, 800));
    EXPECT_FALSE(dgu.has_edge(0, 2));
    EXPECT_EQ(dgu.edge_weight(4, 1), std::numeric_limits<double>::max());
    EXPECT_TRUE(dgu.has_edge(1, 1));
    EXPECT_EQ(dgu.edge_weight(1, 1), 800);

    dgu.insert(Edge(1, 3, -200));

    EXPECT_FALSE(dgu.directed());
    EXPECT_EQ(dgu.vertices(), 5);
    EXPECT_EQ(dgu.edges(), 2);

    EXPECT_EQ(dgu.edge_weight(3, 1), -200);
    dgu.remove(Edge(3, 1, 3000));
    EXPECT_FALSE(dgu.has_edge(1, 3));


    DenseGraph dgo(5, true);
    dgo.insert(Edge(1, 3, -200));
    EXPECT_FALSE(dgo.has_edge(3, 1));
    EXPECT_TRUE(dgo.has_edge(1, 3));

    double& edge_ref = dgo.edge_weight_reference(1, 3);
    edge_ref = 9001;
    EXPECT_EQ(9001, dgo.edge_weight(1, 3));
    edge_ref = 8923174;
    EXPECT_EQ(edge_ref, dgo.edge_weight(1, 3));

    dgo.insert(Edge(2, 3, 4));
    dgo.add_to_edge(Edge(2, 3, 999));
    EXPECT_EQ(1003, dgo.edge_weight(2, 3));
    dgo.add_to_edge(Edge(2, 3, -1000));
    EXPECT_EQ(3, dgo.edge_weight(2, 3));
}

TEST(DenseGraphTests, TestToString) {
    DenseGraph d(3, false);
    insertEdges(d, {{0, 0}, {1, 1}, {1, 0}});
    EXPECT_EQ(d.to_string(), "1 1 1.79769e+308  1 1 1.79769e+308  1.79769e+308 1.79769e+308 1.79769e+308  ");
}

//===========================================================================================================================
TEST(SparseGraphTests, TestConstructor) {
    SparseGraph sg1;
    SparseGraph sg2(20, true);
}

TEST(SparseGraphTests, TestEdgeOperations) {
    SparseGraph sgu(5, false);

    EXPECT_FALSE(sgu.directed());
    EXPECT_EQ(sgu.vertices(), 5);
    EXPECT_EQ(sgu.edges(), 0);

    EXPECT_FALSE(sgu.has_edge(0, 2));
    EXPECT_EQ(sgu.edge_weight(4, 1), std::numeric_limits<double>::max());

    sgu.insert(Edge(1, 1, 800));
    EXPECT_FALSE(sgu.has_edge(0, 2));
    EXPECT_EQ(sgu.edge_weight(4, 1), std::numeric_limits<double>::max());
    EXPECT_TRUE(sgu.has_edge(1, 1));
    EXPECT_EQ(sgu.edge_weight(1, 1), 800);

    sgu.insert(Edge(1, 3, -200));

    EXPECT_FALSE(sgu.directed());
    EXPECT_EQ(sgu.vertices(), 5);
    EXPECT_EQ(sgu.edges(), 2);

    EXPECT_EQ(sgu.edge_weight(3, 1), -200);
    sgu.remove(Edge(3, 1, 3000));
    EXPECT_FALSE(sgu.has_edge(1, 3));


    SparseGraph sgo(5, true);
    sgo.insert(Edge(1, 3, -200));
    EXPECT_FALSE(sgo.has_edge(3, 1));
    EXPECT_TRUE(sgo.has_edge(1, 3));


    double& edge_ref = sgo.edge_weight_reference(1, 3);
    edge_ref = 9001;
    EXPECT_EQ(9001, sgo.edge_weight(1, 3));
    edge_ref = 8923174;
    EXPECT_EQ(edge_ref, sgo.edge_weight(1, 3));

    sgo.insert(Edge(2, 3, 4));
    sgo.add_to_edge(Edge(2, 3, 999));
    EXPECT_EQ(1003, sgo.edge_weight(2, 3));
    sgo.add_to_edge(Edge(2, 3, -1000));
    EXPECT_EQ(3, sgo.edge_weight(2, 3));
}

TEST(SparseGraphTests, TestToString) {
    SparseGraph s(3, false);
    insertEdges(s, {{0, 0}, {1, 1}, {1, 0}});
    EXPECT_EQ(s.to_string(), "0: 1 0  1: 0 1  2:  ");
}

//===========================================================================================================================
TEST(DenseGraphLightTests, TestConstructor) {
    DenseGraphLight lg1;
    DenseGraphLight lg2(20, true);
}

TEST(DenseGraphLightTests, TestEdgeOperations) {
    DenseGraphLight lgu(5, false);

    EXPECT_FALSE(lgu.directed());
    EXPECT_EQ(lgu.vertices(), 5);
    EXPECT_EQ(lgu.edges(), 0);

    EXPECT_FALSE(lgu.has_edge(0, 2));
    EXPECT_EQ(lgu.edge_weight(4, 1), DENSE_GRAPH_LIGHT_EDGE_NON_EXISTANT);

    lgu.insert(Edge(1, 1, 800));
    EXPECT_FALSE(lgu.has_edge(0, 2));
    EXPECT_EQ(lgu.edge_weight(4, 1), DENSE_GRAPH_LIGHT_EDGE_NON_EXISTANT);
    EXPECT_TRUE(lgu.has_edge(1, 1));
    EXPECT_EQ(lgu.edge_weight(1, 1), 800);

    EXPECT_THROW(lgu.insert(Edge(1, 3, -200)), std::invalid_argument);
    lgu.insert(Edge(1, 3, 200));

    EXPECT_FALSE(lgu.directed());
    EXPECT_EQ(lgu.vertices(), 5);
    EXPECT_EQ(lgu.edges(), 2);

    EXPECT_EQ(lgu.edge_weight(3, 1), 200);
    lgu.remove(Edge(3, 1, 3000));
    EXPECT_FALSE(lgu.has_edge(1, 3));


    DenseGraphLight lgo(5, true);
    lgo.insert(Edge(1, 3, 200));
    EXPECT_FALSE(lgo.has_edge(3, 1));
    EXPECT_TRUE(lgo.has_edge(1, 3));

    double& edge_ref = lgo.edge_weight_reference(1, 3);
    edge_ref = 9001;
    EXPECT_EQ(9001, lgo.edge_weight(1, 3));
    edge_ref = 8923174;
    EXPECT_EQ(edge_ref, lgo.edge_weight(1, 3));

    lgo.insert(Edge(2, 3, 4));
    lgo.add_to_edge(Edge(2, 3, 999));
    EXPECT_EQ(1003, lgo.edge_weight(2, 3));
    EXPECT_THROW(lgo.add_to_edge(Edge(2, 3, -1004)), std::invalid_argument);
    lgo.add_to_edge(Edge(2, 3, -1002));
    EXPECT_EQ(1, lgo.edge_weight(2, 3));
}

TEST(DenseGraphLightTests, TestToString) {
    DenseGraphLight l(3, false);
    insertEdges(l, {{0, 0}, {1, 1}, {1, 0}});
    EXPECT_EQ(l.to_string(), "1 1 -1  1 1 -1  -1 -1 -1  ");
}

//===========================================================================================================================
TEST(GraphTests, TestShow) {
    std::cout << std::endl;

    DenseGraph d(4, true);
    insertEdges(d, {
        { 0, 1 },{ 0, 2 },{ 0, 3 },
        { 1, 2 },{ 1, 0 },
        { 2, 1 },
        { 3, 3 },
    });

    d.show();


    SparseGraph s(4, true);
    insertEdges(s, {
            { 0, 1 },{ 0, 2 },{ 0, 3 },
            { 1, 2 },{ 1, 0 },
            { 2, 1 },
            { 3, 3 },
    });

    s.show();


    DenseGraphLight l(4, true);
    insertEdges(l, {
            { 0, 1 },{ 0, 2 },{ 0, 3 },
            { 1, 2 },{ 1, 0 },
            { 2, 1 },
            { 3, 3 },
    });

    l.show();
}

//===========================================================================================================================
