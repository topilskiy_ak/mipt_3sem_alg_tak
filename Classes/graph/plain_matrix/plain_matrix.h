#ifndef MIPT_3SEM_ALG_PLAIN_plain_matrix_PLAIN_plain_matrix_H
#define MIPT_3SEM_ALG_PLAIN_plain_matrix_PLAIN_plain_matrix_H

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <sstream>
#include <stdexcept>
#include <initializer_list>

//===========================================================================================================================
// Класс Матриц
template <typename T>
class plain_matrix {
private:
    size_t _h;     // Высота Матрицы
    size_t _w;     // Ширина Матрицы
    std::vector<T> _m;  // Содержимое Матрицы

public:
    // CONSTRUCTOR: <h> - высота Матрицы, <w> - ширина Матрицы
    plain_matrix(size_t h = 0, size_t w = 0) : _h(h), _w(w), _m(w * h) {}
    // CONSTRUCTOR: <source> - Матрица, с которой делается копия
    plain_matrix(const plain_matrix& source) : _h(source._h), _w(source._w), _m(source._m) {}
    // CONSTRUCTOR: <l> - список списков эелементов, которыми надо инициализировать Матрицу
    plain_matrix(std::initializer_list<std::initializer_list<T>> l);

    // AIM: Вернуть кол-во строк в Матрице
    size_t h()    const { return _h; }
    // AIM: Вернуть кол-во столбцов в Матрице
    size_t w()    const { return _w; }
    // AIM: Вернуть кол=во элементов в Матрице
    size_t size() const { return _h * _w; }

    // Вернуть ссылку на <i, j>-ый элемент Матрицы
    T& operator () (size_t i, size_t j);
    // Вернуть конст-ссылку на <i, j>-ый элемент Матрицы
    const T& operator () (size_t i, size_t j) const;

    // AIM: Оператор присвоения данной Матрице значения Матрицы <m>
    // OUT: Конст-ссылка на данную Матрицу
    const plain_matrix& operator=(const plain_matrix& m);
    // AIM: Оператор перемещения данной Матрице значения Матрицы <m>
    // OUT: Конст-ссылка на данную Матрицу
    const plain_matrix& operator=(const plain_matrix&& m);

    // AIM: Вернуть строку содержимого матрицы
    std::string to_string() const;
};
//---------------------------------------------------------------------------------------------------------------------------
// CONSTRUCTOR: <l> - список списков эелементов, которыми надо инициализировать Матрицу
template <typename T>
plain_matrix<T>::plain_matrix(std::initializer_list<std::initializer_list<T>> l) {
    _h = l.size();
    _w = _h > 0 ? l.begin()->size() : 0;
    _m.resize(_w * _h);
    // Итератор по вектору Матрицы
    size_t pos = 0;
    for (std::initializer_list<T> const& rowList : l) {
        for (const T& value : rowList) 
        {
            _m[pos] = value;
            pos++;
        }
    }
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM: Оператор присвоения данной Матрице значения Матрицы <m>
// OUT: Конст-ссылка на данную Матрицу
template <typename T>
const plain_matrix<T>& plain_matrix<T>::operator=(const plain_matrix& m) {
    _w = m._w;
    _h = m._h;
    _m = m._m;
    return *this;
}

// AIM: Оператор перемещения данной Матрице значения Матрицы <m>
// OUT: Конст-ссылка на данную Матрицу
template <typename T>
const plain_matrix<T>& plain_matrix<T>::operator=(const plain_matrix&& m) {
    _w = m._w;
    _h = m._h;
    _m = move(m._m);
    return *this;
}

//---------------------------------------------------------------------------------------------------------------------------
// Вернуть ссылку на <i, j>-ый элемент Матрицы
template <typename T>
T& plain_matrix<T>::operator()(size_t i, size_t j) {
    if (i + 1 > _w || j + 1 > _h) {
        throw std::out_of_range("No such place in plain_matrix.");
    } else {
        return _m[_w * i + j];
    }
}

// Вернуть конст-ссылку на <i, j>-ый элемент Матрицы
template <typename T>
const T& plain_matrix<T>::operator () (size_t i, size_t j) const {
    if (i + 1 > _w || j + 1 > _h) {
      throw std::out_of_range("No such place in plain_matrix.");
    } else {
      return _m[_w * i + j];
    }
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM: Вернуть строку содержимого матрицы
template <typename T>
std::string plain_matrix<T>::to_string() const {
    std::stringstream output;

    for (size_t i = 0; i < _h; i++) {
        for (size_t j = 0; j < _w; j++) {
          output << (*this)(i, j) << " ";
        }
        output << " ";
    }

    return output.str();
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM: Вывесту в STDOUT Матрицу
template <typename T>
std::ostream& operator << (std::ostream& os, const plain_matrix<T>& m) {
    for (size_t i = 0, h = m.h(); i < h; i++) {
        for (size_t j = 0, w = m.w(); j < w; j++) {
            std::cout << std::setw(3) << m(i, j) << ", ";
        }
        std::cout << std::endl;
    }
    return os;
}
//===========================================================================================================================

#endif // MIPT_3SEM_ALG_PLAIN_plain_matrix_PLAIN_plain_matrix_H