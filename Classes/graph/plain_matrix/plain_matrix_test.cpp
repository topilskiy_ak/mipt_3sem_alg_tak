#include <gtest/gtest.h>
#include "plain_matrix.h"

TEST(PlainMatrixTests, TestConstructors) {
    plain_matrix<bool>   tmp0;
    plain_matrix<int>    tmp1{ {} };
    plain_matrix<double> tmp2(5, 4);
}

TEST(PlainMatrixTests, TestSizeOperations) {
    plain_matrix<double> mtx0by0;
    EXPECT_EQ(mtx0by0.h(), 0);
    EXPECT_EQ(mtx0by0.w(), 0);
    EXPECT_EQ(mtx0by0.size(), 0);

    plain_matrix<int> mtx2by3{
        { 1, 2, 3 },
        { 2, 2, 3 }
    };
    EXPECT_EQ(mtx2by3.h(), 2);
    EXPECT_EQ(mtx2by3.w(), 3);
    EXPECT_EQ(mtx2by3.size(), 2 * 3);

    plain_matrix<bool> mtx199by256(199, 256);
    EXPECT_EQ(mtx199by256.h(), 199);
    EXPECT_EQ(mtx199by256.w(), 256);
    EXPECT_EQ(mtx199by256.size(), 199 * 256);
}

TEST(PlainMatrixTests, TestAccessOperations) {
    plain_matrix<double> mtx{
        { -1, -4, 0, 0 },
        {  0,  1, 1, 5 },
        {  3,  1, 7, 1 },
        { -1,  0, 4, 2 }
    };

    EXPECT_FLOAT_EQ(mtx(0, 0), -1);
    mtx(0, 0) = 200;
    EXPECT_FLOAT_EQ(mtx(0, 0), 200);
    EXPECT_FLOAT_EQ(mtx(2, 3),  1);
    EXPECT_FLOAT_EQ(mtx(1, 3),  5);
    mtx(1, 3) = -99;
    EXPECT_FLOAT_EQ(mtx(1, 3), -99);
    EXPECT_THROW(mtx(4, 1), std::out_of_range);


    const plain_matrix<int> cmtx{
        { 10, 9, -8, 4 },
        { -3,  4, 1, 6 },
        { -9,  2, 5, 8 },
        {  1,  1, 1, 1 }
    };

    EXPECT_FLOAT_EQ(cmtx(0, 0), 10);
    EXPECT_FLOAT_EQ(cmtx(2, 3),  8);
    EXPECT_FLOAT_EQ(cmtx(2, 2),  5);
    EXPECT_FLOAT_EQ(cmtx(1, 3),  6);
    EXPECT_THROW(cmtx(4, 1), std::out_of_range);
}

TEST(PlainMatrixTests, TestOperationEq) {
    plain_matrix<double> tmp;

    auto mtx01 = plain_matrix<double>{
        { 1, 2, 3 },
        { 0, 5, 6 },
        { 0, 0, 9 }
    };

    tmp = mtx01;
    EXPECT_EQ(tmp(1, 2), mtx01(1, 2));
    EXPECT_EQ(tmp(1, 1), mtx01(1, 1));
    EXPECT_EQ(tmp(1, 0), mtx01(1, 0));


    auto mtx02 = plain_matrix<double>{
        { -1, -4, 0, 0 },
        {  0,  1, 1, 5 },
        {  3,  1, 7, 1 },
        { -1,  0, 4, 2 }
    };

    tmp = std::move(mtx02);
    EXPECT_EQ(tmp(1, 2), 1);
    EXPECT_EQ(tmp(2, 2), 7);
    EXPECT_EQ(tmp(1, 0), 0);
}

TEST(PlainMatrixTests, TestToString) {
    auto mtx = plain_matrix<double>{
       { 1, 2, 3 },
       { 0, 5, 6 },
       { 0, 0, 9 }
    };

    EXPECT_EQ(mtx.to_string(), "1 2 3  0 5 6  0 0 9  ");
}

TEST(PlainMatrixTests, TestOperatorStream) {
    auto mtx = plain_matrix<double>{
        { -1, -4, 0, 0 },
        {  0,  1, 1, 5 },
        {  3,  1, 7, 1 },
        { -1,  0, 4, 2 }
    };

    std::cout << mtx << std::endl;
}