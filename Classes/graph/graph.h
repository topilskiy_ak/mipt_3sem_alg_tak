#ifndef MIPT_3SEM_ALG_GRAPH_GRAPH_H
#define MIPT_3SEM_ALG_GRAPH_GRAPH_H

#include "plain_matrix/plain_matrix.h"
#include <forward_list>
#include <algorithm>
#include <set>
#include <type_traits>

//===========================================================================================================================
// Структура Ребра Графа
struct Edge {
    static constexpr double BASE_WEIGHT = 1.0;  // Вес невзвешанного ребра

    size_t v1;       // Вершина Начала Ребра
    size_t v2;       // Вершина Конца Ребра
    double weight;   // Вес Ребра

    Edge(size_t v = 0, size_t e = 0, double w = BASE_WEIGHT) : v1(v), v2(e), weight(w) {}
};
//---------------------------------------------------------------------------------------------------------------------------
// Абстрактный Класс Графа
class Graph {
protected:
    bool _is_directed = false; // Является ли Граф Ориентированным
    size_t _num_vertices = 0;  // Число вершин в Графе
    size_t _num_edges = 0;     // Число ребер в Графе

public:
    // STANDART CONSTRUCTOR:
    Graph(const bool is_directed = false, const size_t num_vertices = 0, const size_t num_edges = 0) :
            _is_directed(is_directed), _num_vertices(num_vertices), _num_edges(num_edges) {}

    // AIM: Вернуть кол-во вершин в Графе
    size_t vertices() const { return _num_vertices; }

    // AIM: Вернуть кол-во ребер в Графе
    size_t edges() const { return _num_edges; }

    // AIM: Вернеть является ли Граф Ориентированным
    bool directed() const { return _is_directed; }

    // AIM: Вставить ребро edge в Граф
    virtual void insert(const Edge& edge) = 0;
    // AIM: Вставить ребро Edge(v, e, weight) в Граф
    virtual void insert(const size_t& v, const size_t& e, const double& weight) = 0;

    // AIM: Вставить ребро edge в Граф (или увеличить его вес, если оно существует)
    virtual void add_to_edge(const Edge& edge) = 0;
    // AIM: Вставить ребро Edge(v, e, weight) в Граф (или увеличить его вес, если оно существует)
    virtual void add_to_edge(const size_t& v, const size_t& e, const double& weight) = 0;

    // AIM: Удалить ребро edge из Графа (если таковое существует)
    virtual void remove(const Edge& edge) = 0;
    // AIM: Удалить ребро на вершинах (v1, v2) из Графа (если таковое существует)
    virtual void remove(const size_t& v, const size_t e) = 0;

    // AIM: Проверить, существует ли ребро между (v,e)
    virtual bool has_edge(size_t v, size_t e) const = 0;

    // AIM: Вернуть вес существующего ребра между (v,e)
    // WAR: Вернуть DBL_MAX, если ребра не существует
    virtual double edge_weight(size_t v, size_t e) const = 0;

    // AIM: Вернуть ссылку на вес существующего ребра между (v,e)
    // WAR: Бросить std::out_of_range, если данное ребро не существует
    virtual double& edge_weight_reference(size_t v, size_t e) = 0;

    // AIM: Вывести граф в STDOUT
    virtual void show() const = 0;

    // AIM: Вернуть строку содержимого матрицы
    virtual std::string to_string() const = 0;

    // STANDARD DESTRUCTOR:
    virtual ~Graph() {}
};
//---------------------------------------------------------------------------------------------------------------------------
// AIM: Вставить в Граф список Ребер
template <class G>
void insertEdges( G& g, std::initializer_list<Edge>&& es) {
    for( auto&& e : es ) {
        g.insert(e);
    }
}

//===========================================================================================================================
//===========================================================================================================================
// Граф на Матрице смежности
class DenseGraph : public Graph {
private:
    // Тип элементов матрицы смежности
    struct AdjElem {
        bool exists = false;                                 // существование ребра
        double weight = std::numeric_limits<double>::max();  // его вес (DBL_MAX при его отсутствии)

        // AIM: По Ребру edge строит элемент матрицы в клетке (v, e)
        void operator=(const Edge& edge) { exists = true; weight = edge.weight; }
        // AIM: Обнулить данный элемент
        void reset() { exists = false; weight = std::numeric_limits<double>::max(); }
    };

    // Тип матрицы которая хранит Ребра
    using AdjMatrix = plain_matrix<AdjElem>;
    // Матрица Ребер Графа
    AdjMatrix _adj;

public:
    // CONSTRUCTOR: Создание оболочки для матрицы смежностей графа (is_directed-ориентиров) с v вершинами
    DenseGraph(size_t v = 0, bool is_directed = false) : _adj(v, v) {
        _is_directed = is_directed;
        _num_vertices = v;
        _num_edges = 0;
    }

    // AIM: Вставить ребро edge в Граф
    void insert(const Edge& edge) { insert(edge.v1, edge.v2, edge.weight); }
    // AIM: Вставить ребро Edge(v1, v2, weight) в Граф
    void insert(const size_t& v, const size_t& e, const double& weight) {
        if (!_adj(v, e).exists) {
            _num_edges++;
            _adj(v, e).exists = true;
            _adj(v, e).weight = weight;
            if (!directed()) {
                _adj(e, v).exists = true;
                _adj(e, v).weight = weight;
            }
        }
    }

    // AIM: Вставить ребро edge в Граф (или увеличить его вес, если оно существует)
    virtual void add_to_edge(const Edge& edge) { add_to_edge(edge.v1, edge.v2, edge.weight); }
    // AIM: Вставить ребро Edge(v, e, weight) в Граф (или увеличить его вес, если оно существует)
    void add_to_edge(const size_t& v, const size_t& e, const double& weight) {
        if (!_adj(v, e).exists) {
            insert(v, e, weight);
        } else {
            _adj(v, e).weight += weight;
            if (!directed()) _adj(e, v).weight += weight;
        }
    }

    // AIM: Удалить ребро edge из Графа (если таковое существует)
    virtual void remove(const Edge& edge) { remove(edge.v1, edge.v2); }
    // AIM: Удалить ребро на вершинах (v1, v2) из Графа (если таковое существует)
    virtual void remove(const size_t& v, const size_t e) {
        if (_adj(v, e).exists) {
            _num_edges--;
            _adj(v, e).reset();
            if (!directed()) _adj(e, v).reset();
        }
    }

    // AIM: Проверить, существует ли ребро между (v,e)
    bool has_edge(size_t v, size_t e) const { return _adj(v, e).exists; }

    // AIM: Вернуть вес существующего ребра между (v,e)
    // WAR: Вернуть DBL_MAX, если ребра не существует
    double edge_weight(size_t v, size_t e) const { return _adj(v, e).weight; }

    // AIM: Вернуть ссылку на вес существующего ребра между (v,e)
    // WAR: Бросить std::out_of_range, если данное ребро не существует
    double& edge_weight_reference(size_t v, size_t e) {
        if (!has_edge(v, e)) {
            throw std::out_of_range("Cannot return reference: Edge does not exist");
        } else {
            return _adj(v, e).weight;
        }
    }

    // AIM: Вывести граф в STDOUT
    void show() const {
        std::cout << std::endl;
        for (size_t i = 0; i < _num_vertices; i++) {
            for (size_t j = 0; j < _num_vertices; j++) {
                std::cout << std::setw(3) << _adj(i, j).weight << ", ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }

    // AIM: Вернуть строку содержимого графа
    std::string to_string() const {
        std::stringstream output;

        for (size_t i = 0; i < _adj.h(); i++) {
          for (size_t j = 0; j < _adj.w(); j++) {
            output << _adj(i, j).weight << " ";
          }
          output << " ";
        }

        return output.str();
    }

    // STANDARD DESTRUCTOR:
    ~DenseGraph() {}
};

//===========================================================================================================================
// Граф на списке смежности вершин.
class SparseGraph : public Graph {
private:
    // Тип элементов списка смежности
    struct ListElem {
        size_t v2 = 0;                                      // вторая вершина ребра
        double weight = std::numeric_limits<double>::max();  // его вес (DBL_MAX при его отсутствии)

        // AIM: По данным строит элемент списка (v, weight)
        ListElem(const size_t v = 0, const double weight = std::numeric_limits<double>::max())
                : v2(v), weight(weight) {}
        // AIM: Сравнить по полю v2 данный элемент с ListElem
        bool operator== (const ListElem& list_elem) const {
            return list_elem.v2 == v2;
        }
    };

    using AdjList  = std::forward_list<ListElem>;
    using AdjLists = std::vector<AdjList>;
    AdjLists _adj;

public:
    SparseGraph(size_t v = 0, bool is_directed = false) : _adj(v) {
        _is_directed = is_directed;
        _num_vertices = v;
        _num_edges = 0;
    }

    // AIM: Расширить граф для содержания еще more_vertices вершин
    void expand(size_t more_vertices) {
        _adj.resize(_adj.size() + more_vertices);
    }

    // AIM: Вставить ребро edge в Граф
    void insert(const Edge& edge) { insert(edge.v1, edge.v2, edge.weight); }
    // AIM: Вставить ребро Edge(v, e, weight) в Граф
    void insert(const size_t& v, const size_t& e, const double& weight) {
        if (!has_edge(v,e)) {
            if (v > _adj.size() || e > _adj.size())
                expand(std::max(v, e) - _adj.size() + 1);

            _adj[v].emplace_front(e, weight);
            _num_edges++;

            if (v != e && !directed()) {
                _adj[e].emplace_front(v, weight);
            }
        }
    }

    // AIM: Вставить ребро edge в Граф (или увеличить его вес, если оно существует)
    void add_to_edge(const Edge& edge) { add_to_edge(edge.v1, edge.v2, edge.weight); }
    // AIM: Вставить ребро Edge(v, e, weight) в Граф (или увеличить его вес, если оно существует)
    void add_to_edge(const size_t& v, const size_t& e, const double& weight) {
        if (!has_edge(v, e)) {
            insert(v, e, weight);
        } else {
            edge_weight_reference(v, e) += weight;
            if (!directed()) edge_weight_reference(e, v) += weight;
        }
    }

    // AIM: Удалить ребро edge из Графа (если таковое существует)
    void remove(const Edge& edge) { remove(edge.v1, edge.v2); }
    // AIM: Удалить ребро на вершинах (v1, v2) из Графа (если таковое существует)
    void remove(const size_t& v, const size_t e) {
        AdjList& v_edges = _adj[v];
        AdjList& e_edges = _adj[e];
        auto adj_elem = find(v_edges.begin(), v_edges.end(), ListElem(e));

        if(adj_elem != v_edges.end()) {
            v_edges.remove(ListElem(e));
            _num_edges--;

            if(!directed()) {
                e_edges.remove(ListElem(v));
            }
        }
    }

    // AIM: Проверить, существует ли ребро между (v,e)
    bool has_edge(size_t v, size_t e) const {
        const AdjList& v_edges = _adj[v];
        auto adj_elem = find(v_edges.begin(), v_edges.end(), ListElem(e));
        return adj_elem != v_edges.end();
    }

    // AIM: Вернуть вес существующего ребра между (v,e)
    // WAR: Вернуть DBL_MAX, если ребра не существует
    double edge_weight(size_t v, size_t e) const {
        if (has_edge(v, e)) {
            const AdjList& v_edges = _adj[v];
            auto adj_elem = find(v_edges.begin(), v_edges.end(), ListElem(e));
            return adj_elem->weight;
        } else {
            return std::numeric_limits<double>::max();
        }
    }

    // AIM: Вернуть ссылку на вес существующего ребра между (v,e)
    // WAR: Бросить std::out_of_range, если данное ребро не существует
    double& edge_weight_reference(size_t v, size_t e) {
        if (!has_edge(v, e)) {
            throw std::out_of_range("Cannot return reference: Edge does not exist");
        } else {
            AdjList& v_edges = _adj[v];
            auto adj_elem = find(v_edges.begin(), v_edges.end(), ListElem(e));
            return adj_elem->weight;
        }
    }

    // AIM: Вывести граф в STDOUT
    void show() const {
        std::cout << std::endl;
        std::cout << "v: " << vertices() << std::endl;
        std::cout << "e: " << edges() << std::endl;
        for(size_t v = 0; v < vertices(); v++ ) {
            std::cout << std::setw(2) << v << ":";
            for(auto list_elem : _adj[v] ) {
                std::cout << std::setw(2) << list_elem.v2 << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }

    // AIM: Вернуть строку содержимого графа
    std::string to_string() const {
        std::stringstream output;

        for(size_t v = 0; v < vertices(); v++ ) {
            output << v << ": ";
            for(auto list_elem : _adj[v] ) {
                output << list_elem.v2 << " ";
            }
            output << " ";
        }

        return output.str();
    }

    // STANDARD DESTRUCTOR:
    ~SparseGraph() {}
};

//===========================================================================================================================
// Отрицательное число, означающее, что ребро не существует
constexpr double DENSE_GRAPH_LIGHT_EDGE_NON_EXISTANT = -1;

// Легкая версия графа на Матрице смежности (с положением, что все ребра положительные)
class DenseGraphLight : public Graph {
private:
    // Тип матрицы которая хранит Ребра
    using AdjMatrix = plain_matrix<double>;
    // Матрица Ребер Графа
    AdjMatrix _adj;

    // AIM: Вернуть, существует ли ребро на данных вершинах
    bool _exists(const size_t& v, const size_t& e) const {
        return _adj(v, e) != DENSE_GRAPH_LIGHT_EDGE_NON_EXISTANT;
    }

    // AIM: Стереть ребро (v, e)
    void _reset(const size_t& v, const size_t& e) { _adj(v, e) = DENSE_GRAPH_LIGHT_EDGE_NON_EXISTANT; }

public:
    // CONSTRUCTOR: Создание оболочки для матрицы смежностей графа (is_directed-ориентиров) с v вершинами
    DenseGraphLight(size_t v = 0, bool is_directed = false) : _adj(v, v) {
        _is_directed = is_directed;
        _num_vertices = v;
        _num_edges = 0;

        for (size_t i = 0; i < v; ++i) {
            for (size_t j = 0; j < v; ++j) {
                _adj(i, j) = DENSE_GRAPH_LIGHT_EDGE_NON_EXISTANT;
            }
        }
    }

    // CONSTRUCTOR: Создание графа над матрицей смежностей
    DenseGraphLight(plain_matrix<double>&& graph_matrix, bool is_directed = false) {
        _adj = graph_matrix;
        _num_vertices = _adj.h();
        _num_edges = 0;
    }

    // AIM: Вставить ребро edge в Граф
    void insert(const Edge& edge) { insert(edge.v1, edge.v2, edge.weight); }
    // AIM: Вставить ребро Edge(v1, v2, weight) в Граф
    void insert(const size_t& v, const size_t& e, const double& weight) {
        if (weight <= DENSE_GRAPH_LIGHT_EDGE_NON_EXISTANT) {
            throw std::invalid_argument("DenseGraphLight doesn't accept negative edges.");
        }
        if (!_exists(v, e)) {
            _num_edges++;
            _adj(v, e) = weight;
            if (!directed()) {
                _adj(e, v) = weight;
            }
        }
    }

    // AIM: Вставить ребро edge в Граф (или увеличить его вес, если оно существует)
    virtual void add_to_edge(const Edge& edge) { add_to_edge(edge.v1, edge.v2, edge.weight); }
    // AIM: Вставить ребро Edge(v, e, weight) в Граф (или увеличить его вес, если оно существует)
    void add_to_edge(const size_t& v, const size_t& e, const double& weight) {
        if (!_exists(v, e)) {
            insert(v, e, weight);
        } else {
            if (_adj(v, e) + weight <= DENSE_GRAPH_LIGHT_EDGE_NON_EXISTANT) {
                throw std::invalid_argument("DenseGraphLight doesn't accept negative edges.");
            }
            _adj(v, e) += weight;
            if (!directed()) _adj(e, v) += weight;
        }
    }

    // AIM: Удалить ребро edge из Графа (если таковое существует)
    virtual void remove(const Edge& edge) { remove(edge.v1, edge.v2); }
    // AIM: Удалить ребро на вершинах (v1, v2) из Графа (если таковое существует)
    virtual void remove(const size_t& v, const size_t e) {
        if (_exists(v, e)) {
            _num_edges--;
            _reset(v, e);
            if (!directed()) _reset(e, v);
        }
    }

    // AIM: Проверить, существует ли ребро между (v,e)
    bool has_edge(size_t v, size_t e) const { return _exists(v, e); }

    // AIM: Вернуть вес существующего ребра между (v,e)
    // WAR: Вернуть DBL_MAX, если ребра не существует
    double edge_weight(size_t v, size_t e) const { return _adj(v, e); }

    // AIM: Вернуть ссылку на вес существующего ребра между (v,e)
    // WAR: Бросить std::out_of_range, если данное ребро не существует
    double& edge_weight_reference(size_t v, size_t e) {
        if (!has_edge(v, e)) {
            throw std::out_of_range("Cannot return reference: Edge does not exist");
        } else {
            return _adj(v, e);
        }
    }

    // AIM: Вывести граф в STDOUT
    void show() const {
        std::cout << std::endl;
        for (size_t i = 0; i < _num_vertices; i++) {
            for (size_t j = 0; j < _num_vertices; j++) {
                std::cout << std::setw(3) << _adj(i, j) << ", ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }

    // AIM: Вернуть строку содержимого графа
    std::string to_string() const {
        std::stringstream output;

        for (size_t i = 0; i < _adj.h(); i++) {
            for (size_t j = 0; j < _adj.w(); j++) {
                output << _adj(i, j) << " ";
            }
            output << " ";
        }

        return output.str();
    }

    // STANDARD DESTRUCTOR:
    ~DenseGraphLight() {}
};
//===========================================================================================================================
//===========================================================================================================================



#endif // MIPT_3SEM_ALG_GRAPH_GRAPH_H
