#include <gtest/gtest.h>
#include "tree.h"

//===========================================================================================================================
using InputVector = std::vector<std::pair<int, int>>;

Treap input_to_treap(const InputVector& input) {
    Treap treap;

    for (const auto& elem : input) {
        treap.insert(elem.first, elem.second);
    }

    return treap;
}

BinaryTree input_to_bin_tree(const InputVector& input) {
    BinaryTree bin_tree;

    for (const auto& elem : input) {
        bin_tree.insert(elem.first);
    }

    return bin_tree;
}

//===========================================================================================================================
const static InputVector input00
        { {5, 11}, {18, 8}, {25, 7}, {50, 12}, {30, 30}, {15, 15}, {20, 10}, {22, 5}, {40, 20}, {45, 9} };
const static InputVector input01
        { {38, 19}, {37, 5}, {47, 15}, {35, 0}, {12, 3}, {0, 42}, {31, 37}, {21, 45}, {30, 26}, {41, 6} };

//===========================================================================================================================
TEST(TreeTests, get_height_test) {
    EXPECT_EQ(2, input_to_bin_tree((input00)).get_height() - input_to_treap(input00).get_height());
    EXPECT_EQ(2, input_to_bin_tree((input01)).get_height() - input_to_treap(input01).get_height());
}

TEST(TreeTests, get_max_width_test) {
    EXPECT_EQ(1, input_to_treap((input00)).get_max_width() - input_to_bin_tree(input00).get_max_width());
    EXPECT_EQ(1, input_to_treap((input01)).get_max_width() - input_to_bin_tree(input01).get_max_width());
}

//===========================================================================================================================
