#include <gtest/gtest.h>
#include "random_num_gen.h"

#include <iostream>

TEST(RANDOM_NUM_GEN_TESTS, UNIFORM_NUMBER_GENERATOR_OF_INTERVAL_TEST) {
    static const size_t MANY_NUMS = 100;
    static const double START_INTERVAL = -100;
    static const double END_INTEVAL    =  100;
    static const double MAX_DIVERGENCE_EPSILON = (END_INTEVAL - START_INTERVAL) / 2;

    double checksum = 0.0;

    std::cout << "\n\n";
    std::cout << "EXPECT RANDOM NUMBERS IN RANGE [-100, 100]: " << std::endl;
    for (size_t index = 0; index < MANY_NUMS; ++index) {
        double tmp = uniform_number_generator_of_interval(START_INTERVAL, END_INTEVAL);
        checksum += tmp;

        std::cout << tmp << " ";
    }
    std::cout << std::endl;
    std::cout << "EXPECT CHECKSUM NUMBER RELATIVELY CLOSE TO 0.0 (IN RANGE [-10, 10]):" << std::endl;
    std::cout << checksum / MANY_NUMS << "\n\n";
    EXPECT_NEAR(checksum / MANY_NUMS, (END_INTEVAL + START_INTERVAL) / 2, MAX_DIVERGENCE_EPSILON);
}

TEST(RANDOM_NUM_GEN_TESTS, UNIFORM_NUMBER_GENERATOR_OF_INTERVAL_0_1_1K_TEST) {
    static const size_t MANY_NUMS = 1000;
    static const double START_INTERVAL = 0.0;
    static const double END_INTEVAL    = 1.0;
    static const double MAX_DIVERGENCE_EPSILON = 0.05;

    double checksum = 0.0;

    std::cout << std::endl;
    for (size_t index = 0; index < MANY_NUMS; ++index) {
        checksum += uniform_number_generator_of_interval(START_INTERVAL, END_INTEVAL);
    }
    std::cout << "EXPECT CHECKSUM NUMBER RELATIVELY CLOSE TO 0.5 (+-0.05):" << std::endl;
    std::cout << checksum / MANY_NUMS << "\n\n";
    EXPECT_NEAR(checksum / MANY_NUMS, (END_INTEVAL + START_INTERVAL) / 2, MAX_DIVERGENCE_EPSILON);
}

TEST(RANDOM_NUM_GEN_TESTS, NORMAL_DISTRIBUION_NUMBER_PAIR_GENERATOR_TEST) {
    static const size_t MANY_NUMS = 100;
    static const double MAX_DIVERGENCE_EPSILON = 0.5;

    double checksum_first_num = 0.0;
    double checksum_second_num = 0.0;

    std::cout << std::endl;
    std::cout << "EXPECT RANDOM PAIRS MOSTLY IN RANGE OF [-2, 2]^2: " << std::endl;
    for (size_t index = 0; index < MANY_NUMS; ++index) {
        NormalDistributionNumberPair tmp_pair = normal_distribution_number_pair_generator();

        checksum_first_num  += tmp_pair.first_num;
        checksum_second_num += tmp_pair.second_num;

        std::cout << tmp_pair.first_num << "  " << tmp_pair.second_num << std::endl;
    }
    std::cout << std::endl;
    std::cout << "EXPECT CHECKSUM NUMBERs RELATIVELY CLOSE TO 0.0 (+- 0.5):" << std::endl;
    std::cout << checksum_first_num / MANY_NUMS << " " << checksum_second_num / MANY_NUMS << "\n\n";
    EXPECT_NEAR(checksum_first_num  / MANY_NUMS, 0.0, MAX_DIVERGENCE_EPSILON);
    EXPECT_NEAR(checksum_second_num / MANY_NUMS, 0.0, MAX_DIVERGENCE_EPSILON);
}