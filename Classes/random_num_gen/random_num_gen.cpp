#include "random_num_gen.h"
#include <random>
#include <climits>

// AIM:  Generate a random number in the interval [start_interval, end_interval]
// NOTE: The multitude of numbers generated this way follows a UNIFORM distribution, given by the interval
double uniform_number_generator_of_interval(const double& start_interval, const double& end_interval) {
    std::random_device tmp_random_device;
    std::mt19937       tmp_mersenne_twister_engine(tmp_random_device());
    std::uniform_real_distribution<double> uniform_real_distr(start_interval, end_interval);
    return uniform_real_distr(tmp_mersenne_twister_engine);
}

// AIM:  Generate a random number in the interval [0, 1]
// NOTE: The multitude of numbers generated this way follows a UNIFORM distribution, given by the interval
double uniform_number_generator_of_interval_zero_one() {
    return uniform_number_generator_of_interval(0.0, 1.0);
}

// AIM:  Generate a pair of numbers of the euclidian plane (through the Box-Muller algorithm)
// NOTE: The multitude of numbers generated this way follows a NORMAL distribution
//       with the mathematical expectation of (0,0)
NormalDistributionNumberPair normal_distribution_number_pair_generator() {
    double first_rand  = -1.0 + 2.0 * uniform_number_generator_of_interval_zero_one();
    double second_rand = -1.0 + 2.0 * uniform_number_generator_of_interval_zero_one();
    double coeff_rand  = first_rand * first_rand + second_rand * second_rand;

    while (coeff_rand >= 1.0) {
        first_rand  = -1.0 + 2.0 * uniform_number_generator_of_interval_zero_one();
        second_rand = -1.0 + 2.0 * uniform_number_generator_of_interval_zero_one();
        coeff_rand  = first_rand * first_rand + second_rand * second_rand;
    }

    coeff_rand = sqrt( (-2.0 * log(coeff_rand)) / coeff_rand);

    double first_num  = first_rand  * coeff_rand;
    double second_num = second_rand * coeff_rand;

    return NormalDistributionNumberPair(first_num, second_num);
}