#ifndef MIPT_3SEM_ALG_RANDOM_NUM_GEN_H
#define MIPT_3SEM_ALG_RANDOM_NUM_GEN_H

// Struct for a pair of numbers, which are part of a normal distribution
struct NormalDistributionNumberPair {
    double first_num;
    double second_num;

    // STANDARD CONSTRUCTOR:
    NormalDistributionNumberPair(const double& first_num_init, const double& second_num_init) :
            first_num(first_num_init), second_num(second_num_init) {}
};

// AIM:  Generate a random number in the interval [start_interval, end_interval]
// NOTE: The multitude of numbers generated this way follows a UNIFORM distribution, given by the interval
double uniform_number_generator_of_interval(const double& start_interval, const double& end_interval);

// AIM:  Generate a pair of numbers of the euclidian plane (through the Box-Muller algorithm)
// NOTE: The multitude of numbers generated this way follows a NORMAL distribution
//       with the mathematical expectation of (0,0)
NormalDistributionNumberPair normal_distribution_number_pair_generator();

#endif //MIPT_3SEM_ALG_RANDOM_NUM_GEN_H
