cmake_minimum_required(VERSION 3.6)
project(random_num_gen_test)

#===========================================================================================================================
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(HEADER_FILES random_num_gen.h gtest.h)
set(SOURCE_FILES random_num_gen_test.cpp)

add_executable(random_num_gen_test ${SOURCE_FILES})
target_link_libraries(random_num_gen_test random_num_gen gtest_main)

#===========================================================================================================================
add_library(random_num_gen random_num_gen.cpp)

#===========================================================================================================================
