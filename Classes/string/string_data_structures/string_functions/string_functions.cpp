//===========================================================================================================================
#include <set>

#include "string_functions.h"

//===========================================================================================================================
// AIM: Return the prefix function of text
std::vector<size_t> string_to_prefix_func(const std::string& text) {
    if (text.size() == 0) {
        return std::vector<size_t>(0);
    }

    std::vector<size_t> prefix_func(text.size(), 0);

    for (size_t index_prefix_func = 1; index_prefix_func < prefix_func.size(); ++index_prefix_func) {
        size_t max_previous_prefix_length = prefix_func[index_prefix_func - 1];

        while (max_previous_prefix_length > 0 &&
               text[index_prefix_func] != text[max_previous_prefix_length]) {
            max_previous_prefix_length = prefix_func[max_previous_prefix_length - 1];
        }

        prefix_func[index_prefix_func] = max_previous_prefix_length;

        if (text[index_prefix_func] == text[max_previous_prefix_length]) {
            prefix_func[index_prefix_func]++;
        }
    }

    return prefix_func;
}

// AIM: Return the zed function of text
std::vector<size_t> string_to_zed_func(const std::string& text) {
    if (text.size() == 0) {
        return std::vector<size_t>(0);
    }

    std::vector<size_t> zed_func(text.size(), 0);

    size_t left_border = 0;
    size_t right_border = 0;
    for (size_t index_zed_func = 1; index_zed_func < zed_func.size(); ++index_zed_func) {
        if (right_border > index_zed_func) {
            if (zed_func[index_zed_func - left_border] < right_border - index_zed_func) {
                zed_func[index_zed_func] = zed_func[index_zed_func - left_border];
                continue;
            } else if (zed_func[index_zed_func - left_border] > right_border - index_zed_func) {
                zed_func[index_zed_func] = right_border - index_zed_func;
                continue;
            } else /* zed_func[index_zed_func - left_border] == right_border - index_zed_func */ {
                zed_func[index_zed_func] = right_border - index_zed_func;
            }
        } else /* (right_border <= index_zed_func) */ {
            zed_func[index_zed_func] = 0;
        }

        while (index_zed_func + zed_func[index_zed_func] < text.size() &&
               text[zed_func[index_zed_func]] == text[index_zed_func + zed_func[index_zed_func]]) {
                  ++zed_func[index_zed_func];
        }

        if (index_zed_func + zed_func[index_zed_func] >= right_border) {
            left_border  = index_zed_func;
            right_border = index_zed_func + zed_func[index_zed_func];
        }
    }

    zed_func[0] = text.size();

    return zed_func;
}

//===========================================================================================================================
// AIM: Return the zed function derived from the given prefix function
std::vector<size_t> prefix_func_to_zed_func(const std::vector<size_t>& prefix_func) {
    if (prefix_func.size() == 0) {
        return std::vector<size_t>(0);
    }

    std::vector<size_t> zed_func(prefix_func.size(), 0);

    for (size_t index_prefix_func = 0; index_prefix_func < prefix_func.size(); ++index_prefix_func) {
        if (prefix_func[index_prefix_func] > 0) {
            size_t index_zed_func = index_prefix_func - (prefix_func[index_prefix_func] - 1);
            zed_func[index_zed_func] = prefix_func[index_prefix_func];
        }
    }

    zed_func[0] = prefix_func.size();

    size_t index_zed_func = 1;
    while (index_zed_func < zed_func.size()) {
        size_t new_index_zed_func = index_zed_func + 1;

        if (zed_func[index_zed_func] > 0) {
            for (size_t index_prefix = 1; index_prefix < zed_func[index_zed_func]; ++index_prefix) {
                if (zed_func[index_zed_func + index_prefix] > zed_func[index_prefix]) {
                    break;
                } else {
                    zed_func[index_zed_func + index_prefix] =
                            std::min(zed_func[index_prefix], zed_func[index_zed_func] - index_prefix);
                }
                ++new_index_zed_func;
            }
        }

        index_zed_func = new_index_zed_func;
    }

    return zed_func;
}

// AIM: Return the prefix function derived from the given zed function
std::vector<size_t> zed_func_to_prefix_func(const std::vector<size_t>& zed_func) {
    if (zed_func.size() == 0) {
        return std::vector<size_t>(0);
    }

    std::vector<size_t> prefix_func(zed_func.size(), 0);

    for (size_t index_zed_func = 1; index_zed_func < zed_func.size(); ++index_zed_func) {
        if (zed_func[index_zed_func] > 0) {
            size_t index_prefix = zed_func[index_zed_func];
            do {
                --index_prefix;
                if (prefix_func[index_zed_func + index_prefix] > 0) {
                    break;
                } else {
                    prefix_func[index_zed_func + index_prefix] = index_prefix + 1;
                }
            } while (index_prefix > 0);
        }
    }

    return prefix_func;
}

//===========================================================================================================================
// AIM: Return the minimum (lexicographically) string derivable from the given prefix function
std::string prefix_func_to_min_string(const std::vector<size_t>& prefix_func) {
    if (prefix_func.size() == 0) {
        return "";
    }

    char last_char_used = ALPHABET_START;
    std::set<char> used_characters;

    std::string min_string(prefix_func.size(), '\0');
    min_string[0] = last_char_used;

    for (size_t index_prefix_func = 1; index_prefix_func < prefix_func.size(); ++index_prefix_func) {
        if (prefix_func[index_prefix_func] > 0) {
            min_string[index_prefix_func] = min_string[prefix_func[index_prefix_func] - 1];

        } else {

            for (size_t max_previous_prefix_length = prefix_func[index_prefix_func - 1];
                        max_previous_prefix_length > 0;
                        max_previous_prefix_length = prefix_func[max_previous_prefix_length - 1]) {

                used_characters.insert(min_string[max_previous_prefix_length]);
            }

            for (char index_char = ALPHABET_START + 1; index_char <= last_char_used; ++index_char) {
                if (used_characters.find(index_char) == used_characters.end()) {
                    min_string[index_prefix_func] = index_char;
                    break;
                }
            }

            if (min_string[index_prefix_func] == '\0') {
                last_char_used++;
                min_string[index_prefix_func] = last_char_used;
            }

            used_characters.clear();
        }
    }

    return min_string;
}

// AIM: Return the minimum (lexicographically) string derivable from the given zed function
std::string zed_func_to_min_string(const std::vector<size_t>& zed_func) {
    if (zed_func.size() == 0) {
        return "";
    }

    size_t index_copy_beginning = 0, num_chars_left_to_copy = 0;
    // ATTENTION: This vector for each character contains the biggest index in min_string
    //   for which it is banned. If a prefix has finished copying, and the read value of the
    //   zed function of current_index (the index straight after the copy has ended) is 0,
    //   then this array contains current_index at exactly the characters which are banned from
    //   being written in min_string[current_index], due to them (the characters)
    //   continuing a prefix that ends right before current_index
    //   If we allow these characters, the zed_func of those previous indexes will be incorrect
    std::vector<size_t> used_characters(ALPHABET_SIZE, 0);

    std::string min_string(zed_func.size(), '\0');
    min_string[0] = ALPHABET_START;

    for (size_t index_zed_func = 1; index_zed_func < zed_func.size(); ++index_zed_func) {
        if (zed_func[index_zed_func] >= num_chars_left_to_copy) {
            num_chars_left_to_copy = zed_func[index_zed_func];
            index_copy_beginning = index_zed_func;

            if (zed_func[index_zed_func] < index_zed_func) {
                char char_after_copy = min_string[zed_func[index_zed_func]];
                size_t index_char_after_copy = static_cast<size_t>(char_after_copy - ALPHABET_START);
                used_characters[index_char_after_copy] = index_zed_func + zed_func[index_zed_func];
            }
        }

        if (num_chars_left_to_copy > 0) {
            min_string[index_zed_func] = min_string[index_zed_func - index_copy_beginning];
            --num_chars_left_to_copy;
        } else {
            for (size_t index_char = 1; index_char < used_characters.size(); ++index_char) {
                if (used_characters[index_char] != index_zed_func) {
                    min_string[index_zed_func] = static_cast<char>(ALPHABET_START + index_char);
                    break;
                }
            }
        }
    }

    return min_string;
}

//===========================================================================================================================
