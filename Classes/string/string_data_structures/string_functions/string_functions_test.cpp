//===========================================================================================================================
#include <gtest/gtest.h>
#include "string_functions.h"

//===========================================================================================================================
#include <sstream>

// AIM: Convert vec to a testable string format
std::string to_string(const std::vector<size_t>& vec) {
    std::stringstream ss_vec;

    for (const size_t& elem : vec) {
        ss_vec << elem << " ";
    }

    return ss_vec.str();
}

//===========================================================================================================================
const static std::string text00 = "";
const static std::string text01 = "abcdabscabcdabca";
const static std::string text02 = "abcabcd";
const static std::string text03 = "asdfbasbdfaasdfb";
const static std::string text04 = "ddddddddd";
const static std::string text05 = "aabaaabaabijkaabaaabaaijk";
const static std::string text06 = "aaabaaabaabaab";
const static std::string text07 = "abcdabcabaabcdabc";
const static std::string text08 = "abcddcbaabcddcbaabcddcbaabcjdcba";
const static std::string text09 = "ffjkifjkffjffjkifght";

const static std::string text10 = "ffjkifjkffjfffjkffjkifjkffjfifjkffjffjkffjkifjkffjfifght";
const static std::string text11 = "abcdeijklmnoaabcdeabababpqrabcdeuvxabyzab";
const static std::string text12 = "xxxdxxxdzxxxdxxxxxs";

const static std::vector<size_t> vecp0 = {};
const static std::vector<size_t> vecp1 = { 0, 1, 2, 3, 0 };
const static std::vector<size_t> vecp2 = { 0, 0, 0, 1, 2, 3, 0 };
const static std::vector<size_t> vecp3 = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
const static std::vector<size_t> vecp4 = { 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 1, 1, 2, 3, 4, 5 };
const static std::vector<size_t> vecp5 = { 0, 0, 0, 1, 2, 1, 2, 3, 0, 1, 1, 1, 2, 1, 2, 3, 0, 1 };
const static std::vector<size_t> vecp6 = { 0, 0, 0, 0, 0 };
const static std::vector<size_t> vecp7 = { 0, 1, 0, 1, 2, 3, 4, 5, 2 };

const static std::vector<size_t> vecz0 = {};
const static std::vector<size_t> vecz1 = { 5, 3, 2, 1, 0 };
const static std::vector<size_t> vecz2 = { 7, 0, 0, 3, 0, 0, 0 };
const static std::vector<size_t> vecz3 = { 14, 2, 1, 0, 6, 2, 1, 0, 2, 1, 0, 2, 1, 0 };
const static std::vector<size_t> vecz4 = { 25, 1, 0, 2, 5, 1, 0, 3, 1, 0, 0, 0, 0, 9, 1, 0, 2, 5, 1, 0, 2, 1, 0, 0, 0 };
const static std::vector<size_t> vecz5 = { 17, 0, 0, 0, 3, 0, 0, 2, 0, 1, 7, 0, 0, 0, 3, 0, 0 };
const static std::vector<size_t> vecz6 = { 32, 0, 0, 0, 0, 0, 0, 1, 19, 0, 0, 0, 0, 0, 0, 1, 11, 0, 0, 0, 0, 0, 0, 1, 3, 0, 0, 0, 0, 0, 0, 1 };

//===========================================================================================================================
TEST(StringFunctionsTests, string_to_prefix_func_test) {
    EXPECT_EQ(to_string(string_to_prefix_func(text00)), "");
    EXPECT_EQ(to_string(string_to_prefix_func(text01)), "0 0 0 0 1 2 0 0 1 2 3 4 5 6 3 1 ");
    EXPECT_EQ(to_string(string_to_prefix_func(text02)), "0 0 0 1 2 3 0 ");
    EXPECT_EQ(to_string(string_to_prefix_func(text03)), "0 0 0 0 0 1 2 0 0 0 1 1 2 3 4 5 ");
    EXPECT_EQ(to_string(string_to_prefix_func(text04)), "0 1 2 3 4 5 6 7 8 ");
    EXPECT_EQ(to_string(string_to_prefix_func(text05)), "0 1 0 1 2 2 3 4 5 3 0 0 0 1 2 3 4 5 6 7 8 9 0 0 0 ");
    EXPECT_EQ(to_string(string_to_prefix_func(text06)), "0 1 2 0 1 2 3 4 5 6 0 1 2 0 ");
    EXPECT_EQ(to_string(string_to_prefix_func(text07)), "0 0 0 0 1 2 3 1 2 1 1 2 3 4 5 6 7 ");
    EXPECT_EQ(to_string(string_to_prefix_func(text08)), "0 0 0 0 0 0 0 1 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 0 0 0 0 1 ");
    EXPECT_EQ(to_string(string_to_prefix_func(text09)), "0 1 0 0 0 1 0 0 1 2 3 1 2 3 4 5 6 0 0 0 ");
}

TEST(StringFunctionsTests, string_to_zed_func_test) {
    EXPECT_EQ(to_string(string_to_zed_func(text00)), "");
    EXPECT_EQ(to_string(string_to_zed_func(text01)), "16 0 0 0 2 0 0 0 6 0 0 0 3 0 0 1 ");
    EXPECT_EQ(to_string(string_to_zed_func(text02)), "7 0 0 3 0 0 0 ");
    EXPECT_EQ(to_string(string_to_zed_func(text03)), "16 0 0 0 0 2 0 0 0 0 1 5 0 0 0 0 ");
    EXPECT_EQ(to_string(string_to_zed_func(text04)), "9 8 7 6 5 4 3 2 1 ");
    EXPECT_EQ(to_string(string_to_zed_func(text05)), "25 1 0 2 5 1 0 3 1 0 0 0 0 9 1 0 2 5 1 0 2 1 0 0 0 ");
    EXPECT_EQ(to_string(string_to_zed_func(text06)), "14 2 1 0 6 2 1 0 2 1 0 2 1 0 ");
    EXPECT_EQ(to_string(string_to_zed_func(text07)), "17 0 0 0 3 0 0 2 0 1 7 0 0 0 3 0 0 ");
    EXPECT_EQ(to_string(string_to_zed_func(text08)), "32 0 0 0 0 0 0 1 19 0 0 0 0 0 0 1 11 0 0 0 0 0 0 1 3 0 0 0 0 0 0 1 ");
    EXPECT_EQ(to_string(string_to_zed_func(text09)), "20 1 0 0 0 1 0 0 3 1 0 6 1 0 0 0 1 0 0 0 ");
}

//===========================================================================================================================
TEST(StringFunctionsTests, prefix_func_to_zed_func) {
    EXPECT_EQ(to_string(string_to_zed_func(text00)), to_string(prefix_func_to_zed_func(string_to_prefix_func(text00))));
    EXPECT_EQ(to_string(string_to_zed_func(text01)), to_string(prefix_func_to_zed_func(string_to_prefix_func(text01))));
    EXPECT_EQ(to_string(string_to_zed_func(text02)), to_string(prefix_func_to_zed_func(string_to_prefix_func(text02))));
    EXPECT_EQ(to_string(string_to_zed_func(text03)), to_string(prefix_func_to_zed_func(string_to_prefix_func(text03))));
    EXPECT_EQ(to_string(string_to_zed_func(text04)), to_string(prefix_func_to_zed_func(string_to_prefix_func(text04))));
    EXPECT_EQ(to_string(string_to_zed_func(text05)), to_string(prefix_func_to_zed_func(string_to_prefix_func(text05))));
    EXPECT_EQ(to_string(string_to_zed_func(text06)), to_string(prefix_func_to_zed_func(string_to_prefix_func(text06))));
    EXPECT_EQ(to_string(string_to_zed_func(text07)), to_string(prefix_func_to_zed_func(string_to_prefix_func(text07))));
    EXPECT_EQ(to_string(string_to_zed_func(text08)), to_string(prefix_func_to_zed_func(string_to_prefix_func(text08))));
    EXPECT_EQ(to_string(string_to_zed_func(text09)), to_string(prefix_func_to_zed_func(string_to_prefix_func(text09))));
}

TEST(StringFunctionsTests, zed_func_to_prefix_func) {
    EXPECT_EQ(to_string(string_to_prefix_func(text00)), to_string(zed_func_to_prefix_func(string_to_zed_func(text00))));
    EXPECT_EQ(to_string(string_to_prefix_func(text01)), to_string(zed_func_to_prefix_func(string_to_zed_func(text01))));
    EXPECT_EQ(to_string(string_to_prefix_func(text02)), to_string(zed_func_to_prefix_func(string_to_zed_func(text02))));
    EXPECT_EQ(to_string(string_to_prefix_func(text03)), to_string(zed_func_to_prefix_func(string_to_zed_func(text03))));
    EXPECT_EQ(to_string(string_to_prefix_func(text04)), to_string(zed_func_to_prefix_func(string_to_zed_func(text04))));
    EXPECT_EQ(to_string(string_to_prefix_func(text05)), to_string(zed_func_to_prefix_func(string_to_zed_func(text05))));
    EXPECT_EQ(to_string(string_to_prefix_func(text06)), to_string(zed_func_to_prefix_func(string_to_zed_func(text06))));
    EXPECT_EQ(to_string(string_to_prefix_func(text07)), to_string(zed_func_to_prefix_func(string_to_zed_func(text07))));
    EXPECT_EQ(to_string(string_to_prefix_func(text08)), to_string(zed_func_to_prefix_func(string_to_zed_func(text08))));
    EXPECT_EQ(to_string(string_to_prefix_func(text09)), to_string(zed_func_to_prefix_func(string_to_zed_func(text09))));
}

//===========================================================================================================================
TEST(StringFunctionsTests, prefix_func_to_min_string) {
    EXPECT_EQ(prefix_func_to_min_string(vecp0), "");
    EXPECT_EQ(to_string(string_to_prefix_func(prefix_func_to_min_string(vecp0))), to_string(vecp0));

    EXPECT_EQ(prefix_func_to_min_string(vecp1), "aaaab");
    EXPECT_EQ(to_string(string_to_prefix_func(prefix_func_to_min_string(vecp1))), to_string(vecp1));

    EXPECT_EQ(prefix_func_to_min_string(vecp2), "abbabbb");
    EXPECT_EQ(to_string(string_to_prefix_func(prefix_func_to_min_string(vecp2))), to_string(vecp2));

    EXPECT_EQ(prefix_func_to_min_string(vecp3), "aaaaaaaaa");
    EXPECT_EQ(to_string(string_to_prefix_func(prefix_func_to_min_string(vecp3))), to_string(vecp3));

    EXPECT_EQ(prefix_func_to_min_string(vecp4), "abbbbabcbbaabbbb");
    EXPECT_EQ(to_string(string_to_prefix_func(prefix_func_to_min_string(vecp4))), to_string(vecp4));

    EXPECT_EQ(prefix_func_to_min_string(vecp5), "abbababbbaaababbba");
    EXPECT_EQ(to_string(string_to_prefix_func(prefix_func_to_min_string(vecp5))), to_string(vecp5));

    EXPECT_EQ(prefix_func_to_min_string(vecp6), "abbbb");
    EXPECT_EQ(to_string(string_to_prefix_func(prefix_func_to_min_string(vecp6))), to_string(vecp6));

    EXPECT_EQ(prefix_func_to_min_string(vecp7), "aabaabaaa");
    EXPECT_EQ(to_string(string_to_prefix_func(prefix_func_to_min_string(vecp7))), to_string(vecp7));
}

TEST(StringFunctionsTests, zed_func_to_min_string) {
    EXPECT_EQ(zed_func_to_min_string(vecz0), "");
    EXPECT_EQ(to_string(string_to_zed_func(zed_func_to_min_string(vecz0))), to_string(vecz0));
    EXPECT_EQ(zed_func_to_min_string(vecz0), prefix_func_to_min_string(zed_func_to_prefix_func(vecz0)));

    EXPECT_EQ(zed_func_to_min_string(vecz1), "aaaab");
    EXPECT_EQ(to_string(string_to_zed_func(zed_func_to_min_string(vecz1))), to_string(vecz1));
    EXPECT_EQ(zed_func_to_min_string(vecz1), prefix_func_to_min_string(zed_func_to_prefix_func(vecz1)));

    EXPECT_EQ(zed_func_to_min_string(vecz2), "abbabbb");
    EXPECT_EQ(to_string(string_to_zed_func(zed_func_to_min_string(vecz2))), to_string(vecz2));
    EXPECT_EQ(zed_func_to_min_string(vecz2), prefix_func_to_min_string(zed_func_to_prefix_func(vecz2)));

    EXPECT_EQ(zed_func_to_min_string(vecz3), "aaabaaabaabaab");
    EXPECT_EQ(to_string(string_to_zed_func(zed_func_to_min_string(vecz3))), to_string(vecz3));
    EXPECT_EQ(zed_func_to_min_string(vecz3), prefix_func_to_min_string(zed_func_to_prefix_func(vecz3)));

    EXPECT_EQ(zed_func_to_min_string(vecz4), "aabaaabaabbbbaabaaabaacbb");
    EXPECT_EQ(to_string(string_to_zed_func(zed_func_to_min_string(vecz4))), to_string(vecz4));
    EXPECT_EQ(zed_func_to_min_string(vecz4), prefix_func_to_min_string(zed_func_to_prefix_func(vecz4)));

    EXPECT_EQ(zed_func_to_min_string(vecz5), "abbbabbabaabbbabb");
    EXPECT_EQ(to_string(string_to_zed_func(zed_func_to_min_string(vecz5))), to_string(vecz5));
    EXPECT_EQ(zed_func_to_min_string(vecz5), prefix_func_to_min_string(zed_func_to_prefix_func(vecz5)));

    EXPECT_EQ(zed_func_to_min_string(vecz6), "abbbbbbaabbbbbbaabbbbbbaabbcbbba");
    EXPECT_EQ(to_string(string_to_zed_func(zed_func_to_min_string(vecz6))), to_string(vecz6));
    EXPECT_EQ(zed_func_to_min_string(vecz6), prefix_func_to_min_string(zed_func_to_prefix_func(vecz6)));


    EXPECT_EQ(zed_func_to_min_string(string_to_zed_func(text00)),
              prefix_func_to_min_string(zed_func_to_prefix_func(string_to_zed_func(text00))));
    EXPECT_EQ(zed_func_to_min_string(string_to_zed_func(text01)),
              prefix_func_to_min_string(zed_func_to_prefix_func(string_to_zed_func(text01))));
    EXPECT_EQ(zed_func_to_min_string(string_to_zed_func(text02)),
              prefix_func_to_min_string(zed_func_to_prefix_func(string_to_zed_func(text02))));
    EXPECT_EQ(zed_func_to_min_string(string_to_zed_func(text03)),
              prefix_func_to_min_string(zed_func_to_prefix_func(string_to_zed_func(text03))));
    EXPECT_EQ(zed_func_to_min_string(string_to_zed_func(text04)),
              prefix_func_to_min_string(zed_func_to_prefix_func(string_to_zed_func(text04))));
    EXPECT_EQ(zed_func_to_min_string(string_to_zed_func(text05)),
              prefix_func_to_min_string(zed_func_to_prefix_func(string_to_zed_func(text05))));
    EXPECT_EQ(zed_func_to_min_string(string_to_zed_func(text06)),
              prefix_func_to_min_string(zed_func_to_prefix_func(string_to_zed_func(text06))));
    EXPECT_EQ(zed_func_to_min_string(string_to_zed_func(text07)),
              prefix_func_to_min_string(zed_func_to_prefix_func(string_to_zed_func(text07))));
    EXPECT_EQ(zed_func_to_min_string(string_to_zed_func(text08)),
              prefix_func_to_min_string(zed_func_to_prefix_func(string_to_zed_func(text08))));
    EXPECT_EQ(zed_func_to_min_string(string_to_zed_func(text09)),
              prefix_func_to_min_string(zed_func_to_prefix_func(string_to_zed_func(text09))));
    EXPECT_EQ(zed_func_to_min_string(string_to_zed_func(text10)),
              prefix_func_to_min_string(zed_func_to_prefix_func(string_to_zed_func(text10))));
    EXPECT_EQ(zed_func_to_min_string(string_to_zed_func(text11)),
              prefix_func_to_min_string(zed_func_to_prefix_func(string_to_zed_func(text11))));
    EXPECT_EQ(zed_func_to_min_string(string_to_zed_func(text12)),
              prefix_func_to_min_string(zed_func_to_prefix_func(string_to_zed_func(text12))));
}

//===========================================================================================================================
