#ifndef MIPT_3SEM_ALG_STRING_FUNCTIONS_H
#define MIPT_3SEM_ALG_STRING_FUNCTIONS_H

//===========================================================================================================================
#include <vector>
#include <string>

constexpr char   ALPHABET_START = 'a';
constexpr size_t ALPHABET_SIZE  = 26;

//===========================================================================================================================
// AIM: Return the prefix function of text
std::vector<size_t> string_to_prefix_func(const std::string& text);

// AIM: Return the zed function of text
std::vector<size_t> string_to_zed_func(const std::string& text);

//===========================================================================================================================
// AIM: Return the zed function derived from the given prefix function
std::vector<size_t> prefix_func_to_zed_func(const std::vector<size_t>& prefix_func);

// AIM: Return the prefix function derived from the given zed function
std::vector<size_t> zed_func_to_prefix_func(const std::vector<size_t>& zed_func);

//===========================================================================================================================
// AIM: Return the minimum (lexicographically) string derivable from the given prefix function
std::string prefix_func_to_min_string(const std::vector<size_t>& prefix_func);

// AIM: Return the minimum (lexicographically) string derivable from the given zed function
std::string zed_func_to_min_string(const std::vector<size_t>& zed_func);

//===========================================================================================================================

#endif //MIPT_3SEM_ALG_STRING_FUNCTIONS_H
