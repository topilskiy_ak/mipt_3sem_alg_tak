cmake_minimum_required(VERSION 3.6)
project(aho_corasick_automaton_test)

#===========================================================================================================================
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(HEADER_FILES aho_corasick_automaton.h gtest.h)
set(SOURCE_FILES aho_corasick_automaton_test.cpp)

add_executable(aho_corasick_automaton_test ${SOURCE_FILES})
target_link_libraries(aho_corasick_automaton_test aho_corasick_automaton gtest_main)

#===========================================================================================================================
add_library(aho_corasick_automaton aho_corasick_automaton.cpp)

#===========================================================================================================================

