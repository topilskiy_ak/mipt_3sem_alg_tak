#ifndef MIPT_3SEM_ALG_AHO_CORASICK_AUTOMATON_H
#define MIPT_3SEM_ALG_AHO_CORASICK_AUTOMATON_H

//===========================================================================================================================
#include <map>
#include <vector>
#include <limits>

//===========================================================================================================================
// Class that implements the Aho-Corasick Automaton for the Aho-Corasick Algorithm
class AhoCorasickAutomaton {
protected:
    static constexpr size_t NULL_LINK = std::numeric_limits<size_t>::max();

    // Struct for the State of the Automaton
    struct State {
        size_t parent_state = 0;
        char   char_from_parent = '\0';

        size_t sufflink = NULL_LINK;
        size_t condensed_sufflink = NULL_LINK;

        std::map<char, size_t> children_states_by_direct_chars;
        std::map<char, size_t> children_states_by_links;

        std::vector<size_t> terminal_pattern_numbers;


        // STANDARD CONSTRUCTOR:
        explicit State(const size_t& parent_state_init) : parent_state(parent_state_init) {}


        // AIM: check whether this state has an initialized sufflink
        bool has_sufflink() { return sufflink != NULL_LINK; }

        // AIM: check whether this state has an initialized condensed_sufflink
        bool has_condensed_sufflink() { return condensed_sufflink != NULL_LINK; }

        // AIM: check whether this state has a direct link by character
        bool has_direct_link(const char& linking_character) {
            return !(children_states_by_direct_chars.find(linking_character) ==
                     children_states_by_direct_chars.end());
        }

        // AIM: check whether this state has any link by character
        bool has_any_link(const char& linking_character) {
            return !(children_states_by_links.find(linking_character) ==
                     children_states_by_links.end());
        }
    };

    std::vector<State> _states;
    std::vector<std::string> _patterns;

    // AIM: Return the next_state, reachable from state by the way of linking_character
    //      (calculate it if needed)
    size_t _get_any_link(const size_t& state, const char& linking_character);

    // AIM: Return the sufflink of the state (calculate it if needed)
    size_t _get_sufflink(const size_t& state);

    // AIM: Return the condensed sufflink of the state (calculate it if needed)
    size_t _get_condensed_sufflink(const size_t& state);

public:
    // EMPTY CONSTRUCTOR:
    AhoCorasickAutomaton();

    // STANDARD CONSTRUCTOR:
    AhoCorasickAutomaton(const std::vector<std::string>& patterns);

    // AIM: Add the pattern to the automaton
    void add_pattern(const std::string& pattern);

    // AIM: Return the patterns recorded in the automaton
    const std::vector<std::string> get_patterns() const { return _patterns; }


    // AIM: Return the vector which contains for every point in the text
    //      the vector of indexes of patterns which begin in that point
    std::vector<std::vector<size_t>> get_vector_of_patterns_in_text(const std::string& text);

    // AIM: Return map, which reflects every point in the text
    //      to the vector of indexes of patterns which begin in that point
    std::map<size_t, std::vector<size_t>> get_map_of_patterns_in_text(const std::string& text);
};

//===========================================================================================================================

#endif //MIPT_3SEM_ALG_AHO_CORASICK_AUTOMATON_H
