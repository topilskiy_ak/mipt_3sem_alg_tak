//===========================================================================================================================
#include "aho_corasick_automaton.h"

//===========================================================================================================================
// AIM: Return the next_state, reachable from state by the way of linking_character
//      (calculate it if needed)
size_t AhoCorasickAutomaton::_get_any_link(const size_t& state, const char& linking_character) {
    if (!_states[state].has_any_link(linking_character)) {
        if (_states[state].has_direct_link(linking_character)) {
            _states[state].children_states_by_links[linking_character] =
                    _states[state].children_states_by_direct_chars[linking_character];
        } else {
            if (state == 0) {
                _states[state].children_states_by_links[linking_character] = 0;
            } else {
                _states[state].children_states_by_links[linking_character] =
                        _get_any_link(_get_sufflink(state), linking_character);
            }
        }
    }

    return _states[state].children_states_by_links[linking_character];
}

// AIM: Return the sufflink of the state (calculate it if needed)
size_t AhoCorasickAutomaton::_get_sufflink(const size_t& state) {
    if (!_states[state].has_sufflink()) {
        if (_states[state].parent_state == 0) {
            _states[state].sufflink = 0;
        } else {
            _states[state].sufflink = _get_any_link(_get_sufflink(_states[state].parent_state),
                                                    _states[state].char_from_parent);
        }
    }

    return _states[state].sufflink;
}

// AIM: Return the condensed sufflink of the state (calculate it if needed)
size_t AhoCorasickAutomaton::_get_condensed_sufflink(const size_t& state) {
    if (!_states[state].has_condensed_sufflink()) {
        size_t sufflink_state = _get_sufflink(state);

        if (_states[sufflink_state].terminal_pattern_numbers.empty()) {
            _states[state].condensed_sufflink = _get_condensed_sufflink(sufflink_state);
        } else {
            _states[state].condensed_sufflink = sufflink_state;
        }
    }

    return _states[state].condensed_sufflink;
}

//---------------------------------------------------------------------------------------------------------------------------
// EMPTY CONSTRUCTOR:
AhoCorasickAutomaton::AhoCorasickAutomaton() {
    _states.emplace_back(0);

    _states[0].sufflink = 0;
    _states[0].condensed_sufflink = 0;
}

// STANDARD CONSTRUCTOR:
AhoCorasickAutomaton::AhoCorasickAutomaton(const std::vector<std::string>& patterns) : AhoCorasickAutomaton() {
    for (const std::string& pattern : patterns) {
        add_pattern(pattern);
    }
}

// AIM: Add the pattern to the automaton
void AhoCorasickAutomaton::add_pattern(const std::string& pattern) {
    size_t current_state_index = 0;

    for (const char& character : pattern) {
        if (!_states[current_state_index].has_direct_link(character)) {
            _states[current_state_index].children_states_by_direct_chars[character] = _states.size();
            _states.emplace_back(current_state_index);
            _states.back().char_from_parent = character;
        }

        current_state_index = _states[current_state_index].children_states_by_direct_chars[character];
    }

    _states[current_state_index].terminal_pattern_numbers.push_back(_patterns.size());
    _patterns.push_back(pattern);
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM: Return the vector which contains for every point in the text
//      the vector of indexes of patterns which begin in that point
std::vector<std::vector<size_t>> AhoCorasickAutomaton::get_vector_of_patterns_in_text(
        const std::string& text) {
    std::vector<std::vector<size_t>> patterns_in_text(text.size(), std::vector<size_t>(0));

    size_t current_state = 0;
    for (size_t text_index = 0; text_index < text.size(); ++text_index) {
        char character = text[text_index];
        current_state = _get_any_link(current_state, character);

        size_t condensed_sufflink_state = current_state;
        while (condensed_sufflink_state != 0) {
            for (const size_t& pattern_index : _states[condensed_sufflink_state].terminal_pattern_numbers) {
                size_t index_of_pattern_start = text_index - _patterns[pattern_index].size() + 1;
                patterns_in_text[index_of_pattern_start].push_back(pattern_index);
            }

            condensed_sufflink_state = _get_condensed_sufflink(condensed_sufflink_state);
        }
    }

    return patterns_in_text;
}

// AIM: Return map, which reflects every point in the text
//      to the vector of indexes of patterns which begin in that point
std::map<size_t, std::vector<size_t>> AhoCorasickAutomaton::get_map_of_patterns_in_text(
        const std::string& text) {
    std::map<size_t, std::vector<size_t>> patterns_in_text;

    size_t current_state = 0;
    for (size_t text_index = 0; text_index < text.size(); ++text_index) {
        char character = text[text_index];
        current_state = _get_any_link(current_state, character);

        size_t condensed_sufflink_state = current_state;
        while (condensed_sufflink_state != 0) {
            for (const size_t& pattern_index : _states[condensed_sufflink_state].terminal_pattern_numbers) {
                size_t index_of_pattern_start = text_index - _patterns[pattern_index].size() + 1;
                patterns_in_text[index_of_pattern_start].push_back(pattern_index);
            }

            condensed_sufflink_state = _get_condensed_sufflink(condensed_sufflink_state);
        }
    }

    return patterns_in_text;
};

//===========================================================================================================================
