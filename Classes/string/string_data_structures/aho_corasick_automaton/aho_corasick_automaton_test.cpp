//===========================================================================================================================
#include <gtest/gtest.h>

#include "aho_corasick_automaton.h"

//===========================================================================================================================
#include <sstream>

// AIM: Convert vecvec to a testable string format
std::string to_string(const std::vector<std::vector<size_t>>& vecvec) {
    std::stringstream ss_vec;

    for (const std::vector<size_t>& vec : vecvec) {
        for (const size_t& elem : vec) {
            ss_vec << elem << " ";
        }
        ss_vec << "$ ";
    }

    return ss_vec.str();
}

// AIM: Convert vec to a testable string format
std::string to_string(const std::map<size_t, std::vector<size_t>>& mapvec) {
    std::stringstream ss_vec;

    for (const std::pair<size_t, std::vector<size_t>>& vec : mapvec) {
        for (const size_t& elem : vec.second) {
            ss_vec << elem << " ";
        }
        ss_vec << "$ ";
    }

    return ss_vec.str();
}


//===========================================================================================================================
static const std::vector<std::string> tp00 {};
static const std::vector<std::string> tp01 { "a", "b", "c" };
static const std::vector<std::string> tp02 { "a", "b", "c", "d", "e", "a", "b"};
static const std::vector<std::string> tp03 { "aa", "aab", "bb", "abc" };
static const std::vector<std::string> tp04 { "abcabc", "abc", "aa", "zzzz", "yolo" };
static const std::vector<std::string> tp05 { "i", "ii", "iii", "iiii" };

static const std::string t00 = "abc";
static const std::string t01 = "abcabc";
static const std::string t02 = "aabcabcaabbcc";
static const std::string t03 = "aaabcababcababcaabcab";
static const std::string t04 = "yoloyoloyoloolololzzzzzololzzz";
static const std::string t05 = "abciiiiiiiiiiabcghasdifq";

//===========================================================================================================================
TEST(AhoCorasickAutomatonTests, constructor_test) {
    AhoCorasickAutomaton aca_tp00(tp00);
    AhoCorasickAutomaton aca_tp01(tp01);
    AhoCorasickAutomaton aca_tp02(tp02);
    AhoCorasickAutomaton aca_tp03(tp03);
    AhoCorasickAutomaton aca_tp04(tp04);
    AhoCorasickAutomaton aca_tp05(tp05);
}

TEST(AhoCorasickAutomatonTests, get_vector_of_patterns_in_text_test) {
    AhoCorasickAutomaton aca_tp00(tp00);
    EXPECT_EQ(to_string(aca_tp00.get_vector_of_patterns_in_text(t00)), "$ $ $ ");
    EXPECT_EQ(to_string(aca_tp00.get_vector_of_patterns_in_text(t01)), "$ $ $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp00.get_vector_of_patterns_in_text(t02)), "$ $ $ $ $ $ $ $ $ $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp00.get_vector_of_patterns_in_text(t03)), "$ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp00.get_vector_of_patterns_in_text(t04)), "$ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp00.get_vector_of_patterns_in_text(t05)), "$ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ ");

    AhoCorasickAutomaton aca_tp01(tp01);
    EXPECT_EQ(to_string(aca_tp01.get_vector_of_patterns_in_text(t00)), "0 $ 1 $ 2 $ ");
    EXPECT_EQ(to_string(aca_tp01.get_vector_of_patterns_in_text(t01)), "0 $ 1 $ 2 $ 0 $ 1 $ 2 $ ");
    EXPECT_EQ(to_string(aca_tp01.get_vector_of_patterns_in_text(t02)), "0 $ 0 $ 1 $ 2 $ 0 $ 1 $ 2 $ 0 $ 0 $ 1 $ 1 $ 2 $ 2 $ ");
    EXPECT_EQ(to_string(aca_tp01.get_vector_of_patterns_in_text(t03)), "0 $ 0 $ 0 $ 1 $ 2 $ 0 $ 1 $ 0 $ 1 $ 2 $ 0 $ 1 $ 0 $ 1 $ 2 $ 0 $ 0 $ 1 $ 2 $ 0 $ 1 $ ");
    EXPECT_EQ(to_string(aca_tp01.get_vector_of_patterns_in_text(t04)), "$ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp01.get_vector_of_patterns_in_text(t05)), "0 $ 1 $ 2 $ $ $ $ $ $ $ $ $ $ $ 0 $ 1 $ 2 $ $ $ 0 $ $ $ $ $ $ ");

    AhoCorasickAutomaton aca_tp02(tp02);
    EXPECT_EQ(to_string(aca_tp02.get_vector_of_patterns_in_text(t00)), "0 5 $ 1 6 $ 2 $ ");
    EXPECT_EQ(to_string(aca_tp02.get_vector_of_patterns_in_text(t01)), "0 5 $ 1 6 $ 2 $ 0 5 $ 1 6 $ 2 $ ");
    EXPECT_EQ(to_string(aca_tp02.get_vector_of_patterns_in_text(t02)), "0 5 $ 0 5 $ 1 6 $ 2 $ 0 5 $ 1 6 $ 2 $ 0 5 $ 0 5 $ 1 6 $ 1 6 $ 2 $ 2 $ ");
    EXPECT_EQ(to_string(aca_tp02.get_vector_of_patterns_in_text(t03)), "0 5 $ 0 5 $ 0 5 $ 1 6 $ 2 $ 0 5 $ 1 6 $ 0 5 $ 1 6 $ 2 $ 0 5 $ 1 6 $ 0 5 $ 1 6 $ 2 $ 0 5 $ 0 5 $ 1 6 $ 2 $ 0 5 $ 1 6 $ ");
    EXPECT_EQ(to_string(aca_tp02.get_vector_of_patterns_in_text(t04)), "$ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp02.get_vector_of_patterns_in_text(t05)), "0 5 $ 1 6 $ 2 $ $ $ $ $ $ $ $ $ $ $ 0 5 $ 1 6 $ 2 $ $ $ 0 5 $ $ 3 $ $ $ $ ");

    AhoCorasickAutomaton aca_tp03(tp03);
    EXPECT_EQ(to_string(aca_tp03.get_vector_of_patterns_in_text(t00)), "3 $ $ $ ");
    EXPECT_EQ(to_string(aca_tp03.get_vector_of_patterns_in_text(t01)), "3 $ $ $ 3 $ $ $ ");
    EXPECT_EQ(to_string(aca_tp03.get_vector_of_patterns_in_text(t02)), "0 1 $ 3 $ $ $ 3 $ $ $ 0 1 $ $ 2 $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp03.get_vector_of_patterns_in_text(t03)), "0 $ 0 1 $ 3 $ $ $ $ $ 3 $ $ $ $ $ 3 $ $ $ 0 1 $ 3 $ $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp03.get_vector_of_patterns_in_text(t04)), "$ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp03.get_vector_of_patterns_in_text(t05)), "3 $ $ $ $ $ $ $ $ $ $ $ $ $ 3 $ $ $ $ $ $ $ $ $ $ $ ");

    AhoCorasickAutomaton aca_tp04(tp04);
    EXPECT_EQ(to_string(aca_tp04.get_vector_of_patterns_in_text(t00)), "1 $ $ $ ");
    EXPECT_EQ(to_string(aca_tp04.get_vector_of_patterns_in_text(t01)), "1 0 $ $ $ 1 $ $ $ ");
    EXPECT_EQ(to_string(aca_tp04.get_vector_of_patterns_in_text(t02)), "2 $ 1 0 $ $ $ 1 $ $ $ 2 $ $ $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp04.get_vector_of_patterns_in_text(t03)), "2 $ 2 $ 1 $ $ $ $ $ 1 $ $ $ $ $ 1 $ $ $ 2 $ 1 $ $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp04.get_vector_of_patterns_in_text(t04)), "4 $ $ $ $ 4 $ $ $ $ 4 $ $ $ $ $ $ $ $ $ $ 3 $ 3 $ $ $ $ $ $ $ $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp04.get_vector_of_patterns_in_text(t05)), "1 $ $ $ $ $ $ $ $ $ $ $ $ $ 1 $ $ $ $ $ $ $ $ $ $ $ ");

    AhoCorasickAutomaton aca_tp05(tp05);
    EXPECT_EQ(to_string(aca_tp05.get_vector_of_patterns_in_text(t00)), "$ $ $ ");
    EXPECT_EQ(to_string(aca_tp05.get_vector_of_patterns_in_text(t01)), "$ $ $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp05.get_vector_of_patterns_in_text(t02)), "$ $ $ $ $ $ $ $ $ $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp05.get_vector_of_patterns_in_text(t03)), "$ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp05.get_vector_of_patterns_in_text(t04)), "$ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ ");
    EXPECT_EQ(to_string(aca_tp05.get_vector_of_patterns_in_text(t05)), "$ $ $ 0 1 2 3 $ 0 1 2 3 $ 0 1 2 3 $ 0 1 2 3 $ 0 1 2 3 $ 0 1 2 3 $ 0 1 2 3 $ 0 1 2 $ 0 1 $ 0 $ $ $ $ $ $ $ $ $ 0 $ $ $ ");
}

TEST(AhoCorasickAutomatonTests, get_map_of_patterns_in_text) {
    AhoCorasickAutomaton aca_tp00(tp00);
    AhoCorasickAutomaton aca_tp01(tp01);
    AhoCorasickAutomaton aca_tp02(tp02);
    AhoCorasickAutomaton aca_tp03(tp03);
    AhoCorasickAutomaton aca_tp04(tp04);
    AhoCorasickAutomaton aca_tp05(tp05);

    EXPECT_EQ(to_string(aca_tp00.get_map_of_patterns_in_text(t00)), "");
    EXPECT_EQ(to_string(aca_tp01.get_map_of_patterns_in_text(t01)), "0 $ 1 $ 2 $ 0 $ 1 $ 2 $ ");
    EXPECT_EQ(to_string(aca_tp02.get_map_of_patterns_in_text(t02)), "0 5 $ 0 5 $ 1 6 $ 2 $ 0 5 $ 1 6 $ 2 $ 0 5 $ 0 5 $ 1 6 $ 1 6 $ 2 $ 2 $ ");
    EXPECT_EQ(to_string(aca_tp03.get_map_of_patterns_in_text(t03)), "0 $ 0 $ 1 $ 3 $ 3 $ 3 $ 0 $ 1 $ 3 $ ");
    EXPECT_EQ(to_string(aca_tp04.get_map_of_patterns_in_text(t04)), "4 $ 4 $ 4 $ 3 $ 3 $ ");
    EXPECT_EQ(to_string(aca_tp05.get_map_of_patterns_in_text(t05)), "0 $ 1 0 $ 2 1 0 $ 3 2 1 0 $ 3 2 1 0 $ 3 2 1 0 $ 3 2 1 0 $ 3 2 1 0 $ 3 2 1 0 $ 3 2 1 0 $ 0 $ ");
}

//===========================================================================================================================
