//===========================================================================================================================
#include <gtest/gtest.h>

#include "aho_corasick_specialized.h"

//===========================================================================================================================
#include <sstream>

// AIM: Convert vec to a testable string format
std::string to_string(const std::vector<size_t>& vec) {
    std::stringstream ss_vec;

    for (const size_t& elem : vec) {
        ss_vec << elem << " ";
    }

    return ss_vec.str();
}

//===========================================================================================================================
const static std::string text00 = "ababacaba";
const static std::string pattern00 = "ab??aba";
const static std::string pattern01 = "a?";
const static std::string pattern02 = "aba";
const static std::string pattern03 = "?ba";

const static std::string text01 = "dogdigdogdugddidjdd";
const static std::string pattern10 = "dog";
const static std::string pattern11 = "d?g";
const static std::string pattern12 = "?";
const static std::string pattern13 = "???";
const static std::string pattern14 = "?gd?g";

const static std::string text02 = "abcacbaaabcadcaadc";
const static std::string pattern20 = "abc";
const static std::string pattern21 = "a??";
const static std::string pattern22 = "a??a";
const static std::string pattern23 = "??b?a?c?";

TEST(AhoCorasickSpecializedTest, locate_question_pattern_in_text_test) {
    EXPECT_EQ(to_string(locate_question_pattern_in_text(pattern00, text00)), "2 ");
    EXPECT_EQ(to_string(locate_question_pattern_in_text(pattern01, text00)), "0 2 4 6 ");
    EXPECT_EQ(to_string(locate_question_pattern_in_text(pattern02, text00)), "0 2 6 ");
    EXPECT_EQ(to_string(locate_question_pattern_in_text(pattern03, text00)), "0 2 6 ");

    EXPECT_EQ(to_string(locate_question_pattern_in_text(pattern10, text01)), "0 6 ");
    EXPECT_EQ(to_string(locate_question_pattern_in_text(pattern11, text01)), "0 3 6 9 ");
    EXPECT_EQ(to_string(locate_question_pattern_in_text(pattern12, text01)), "0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 ");
    EXPECT_EQ(to_string(locate_question_pattern_in_text(pattern13, text01)), "0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 ");
    EXPECT_EQ(to_string(locate_question_pattern_in_text(pattern14, text01)), "1 4 7 ");

    EXPECT_EQ(to_string(locate_question_pattern_in_text(pattern20, text02)), "0 8 ");
    EXPECT_EQ(to_string(locate_question_pattern_in_text(pattern21, text02)), "0 3 6 7 8 11 14 15 ");
    EXPECT_EQ(to_string(locate_question_pattern_in_text(pattern22, text02)), "0 3 8 11 ");
    EXPECT_EQ(to_string(locate_question_pattern_in_text(pattern23, text02)), "7 ");
}

//===========================================================================================================================
