//===========================================================================================================================
#include "aho_corasick_specialized.h"

//===========================================================================================================================
// Separate the pattern string into pieces with no question marks
// and add them to the AhoCorasickAutomaton
void LocateQuestionPatternInTextWrapper::_separate_pattern_into_pieces() {
    std::string reading_string = "";
    for (size_t pattern_index = 0; pattern_index < _pattern.size(); ++pattern_index) {
        if (_pattern[pattern_index] != '?') {
            reading_string += _pattern[pattern_index];
        } else if (!reading_string.empty()) {
            add_pattern(reading_string);
            _pattern_pieces_end_offset.push_back(pattern_index);
            reading_string.clear();
        }
    }

    if (!reading_string.empty()) {
        add_pattern(reading_string);
        _pattern_pieces_end_offset.push_back(_pattern.size());
    }
}

// Return the number of pattern pieces in each index of text
// using the starts of the pieces and information on their place in the pattern
std::vector<size_t> LocateQuestionPatternInTextWrapper::_calculate_pattern_pieces_beginning() {
    std::vector<size_t> _pattern_pieces_beginning(_text.size(), 0);

    size_t current_state = 0;
    for (size_t text_index = 0; text_index < _text.size(); ++text_index) {
        char character = _text[text_index];
        current_state = _get_any_link(current_state, character);

        size_t condensed_sufflink_state = current_state;
        while (condensed_sufflink_state != 0) {
            for (const size_t& pattern_piece_index :
                    _states[condensed_sufflink_state].terminal_pattern_numbers) {
                if (text_index + 1 >= _pattern_pieces_end_offset[pattern_piece_index]) {
                    ++_pattern_pieces_beginning[text_index + 1 - _pattern_pieces_end_offset[pattern_piece_index]];
                }
            }

            condensed_sufflink_state = _get_condensed_sufflink(condensed_sufflink_state);
        }
    }

    return _pattern_pieces_beginning;
}

//===========================================================================================================================
// Return all the instances of pattern (with question marks in mind) in text
std::vector<size_t> LocateQuestionPatternInTextWrapper::calculate_pattern_locations_in_text() {
    std::vector<size_t> _pattern_pieces_beginning = _calculate_pattern_pieces_beginning();
    std::vector<size_t> pattern_locations_in_text;

    for (size_t text_index = 0;
         text_index + _pattern.size() <= _pattern_pieces_beginning.size();
         ++text_index) {

        if(_patterns.size() == _pattern_pieces_beginning[text_index]) {
            pattern_locations_in_text.push_back(text_index);
        }
    }

    return pattern_locations_in_text;
}
//===========================================================================================================================
// Find all the occurrences of pattern (with masked question marks) in text
std::vector<size_t> locate_question_pattern_in_text(const std::string& pattern, const std::string& text) {
    return LocateQuestionPatternInTextWrapper(pattern, text).calculate_pattern_locations_in_text();
}

//===========================================================================================================================
