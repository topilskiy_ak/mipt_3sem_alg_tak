#ifndef MIPT_3SEM_ALG_AHO_CORASICK_SPECIALIZED_H
#define MIPT_3SEM_ALG_AHO_CORASICK_SPECIALIZED_H

//===========================================================================================================================
#include "string/string_data_structures/aho_corasick_automaton/aho_corasick_automaton.h"

//===========================================================================================================================
// Wrapper for the algorithm of finding a pattern with question marks in a text
class LocateQuestionPatternInTextWrapper : private AhoCorasickAutomaton {
private:
    const std::string& _pattern;
    const std::string& _text;

    std::vector<size_t> _pattern_pieces_end_offset;

    // Separate the pattern string into pieces with no question marks
    // and add them to the AhoCorasickAutomaton
    void _separate_pattern_into_pieces();

    // Return the number of pattern pieces in each index of text
    // using the starts of the pieces and information on their place in the pattern
    std::vector<size_t> _calculate_pattern_pieces_beginning();

public:
    // STANDARD CONSTRUCTOR
    LocateQuestionPatternInTextWrapper(const std::string& pattern_init, const std::string& text_init) :
            AhoCorasickAutomaton(), _pattern(pattern_init), _text(text_init) { _separate_pattern_into_pieces(); }

    // Return all the instances of pattern (with question marks in mind) in text
    std::vector<size_t> calculate_pattern_locations_in_text();
};

//===========================================================================================================================
// Find all the occurrences of pattern (with masked question marks) in text
std::vector<size_t> locate_question_pattern_in_text(const std::string& pattern, const std::string& text);

//===========================================================================================================================

#endif //MIPT_3SEM_ALG_AHO_CORASICK_SPECIALIZED_H
