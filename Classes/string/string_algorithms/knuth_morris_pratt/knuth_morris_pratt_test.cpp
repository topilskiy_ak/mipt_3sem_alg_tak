#include <gtest/gtest.h>
#include "knuth_morris_pratt.h"

//===========================================================================================================================
#include <sstream>

// AIM: Convert vec to a testable string format
std::string to_string(const std::vector<size_t>& vec) {
    std::stringstream ss_vec;

    for (const size_t& elem : vec) {
        ss_vec << elem << " ";
    }

    return ss_vec.str();
}

//===========================================================================================================================
const static std::string text00 = "";
const static std::string text01 = "abcdabscabcdabca";
const static std::string text02 = "abacababa";
const static std::string text03 = "asdfbasbdfaasdfb";
const static std::string text04 = "ddddddddd";
const static std::string text05 = "aabaaabaabijkaabaaabaaijk";
const static std::string text06 = "aaabaaabaabaab";
const static std::string text07 = "abcdabcabaabcdabc";
const static std::string text08 = "abcddcbaabcddcbaabcddcbaabcjdcba";
const static std::string text09 = "ffjkifjkffjffjkifght";

const static std::string pattern00 = "";
const static std::string pattern01 = "abc";
const static std::string pattern02 = "aba";
const static std::string pattern03 = "sdf";
const static std::string pattern04 = "ddd";
const static std::string pattern05 = "aa";
const static std::string pattern06 = "aab";
const static std::string pattern07 = "abc";
const static std::string pattern08 = "abcdd";
const static std::string pattern09 = "ff";

//===========================================================================================================================
TEST(KnuthMorrisPrattTests, knuth_morris_pratt) {
    EXPECT_EQ(to_string(knuth_morris_pratt(text00, pattern00)), "");
    EXPECT_EQ(to_string(knuth_morris_pratt(text01, pattern01)), "0 8 12 ");
    EXPECT_EQ(to_string(knuth_morris_pratt(text02, pattern02)), "0 4 6 ");
    EXPECT_EQ(to_string(knuth_morris_pratt(text03, pattern03)), "1 12 ");
    EXPECT_EQ(to_string(knuth_morris_pratt(text04, pattern04)), "0 1 2 3 4 5 6 ");
    EXPECT_EQ(to_string(knuth_morris_pratt(text05, pattern05)), "0 3 4 7 13 16 17 20 ");
    EXPECT_EQ(to_string(knuth_morris_pratt(text06, pattern06)), "1 5 8 11 ");
    EXPECT_EQ(to_string(knuth_morris_pratt(text07, pattern07)), "0 4 10 14 ");
    EXPECT_EQ(to_string(knuth_morris_pratt(text08, pattern08)), "0 8 16 ");
    EXPECT_EQ(to_string(knuth_morris_pratt(text09, pattern09)), "0 8 11 ");
}

//===========================================================================================================================
