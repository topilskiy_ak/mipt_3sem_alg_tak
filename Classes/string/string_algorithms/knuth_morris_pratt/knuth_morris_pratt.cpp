//===========================================================================================================================
#include "knuth_morris_pratt.h"
#include <string/string_data_structures/string_functions/string_functions.h>

//===========================================================================================================================
// AIM: Return the vector of places where the patter exists the text (by the means of the KMT algorithm)
std::vector<size_t> knuth_morris_pratt(const std::string& text, const std::string& pattern) {
    std::vector<size_t> indexes_of_pattern_in_text;

    std::vector<size_t> prefix_func = string_to_prefix_func(pattern + "$");
    size_t prefix_func_of_text = 0;

    for (size_t index_text = 0; index_text < text.size(); ++index_text) {
        size_t max_previous_prefix_length = prefix_func_of_text;

        while (max_previous_prefix_length > 0 &&
               text[index_text] != pattern[max_previous_prefix_length]) {
            max_previous_prefix_length = prefix_func[max_previous_prefix_length - 1];
        }

        prefix_func_of_text = max_previous_prefix_length;

        if (text[index_text] == pattern[max_previous_prefix_length]) {
            prefix_func_of_text++;
        }

        if (prefix_func_of_text == pattern.size()) {
            indexes_of_pattern_in_text.push_back(index_text - pattern.size() + 1);
        }
    }

    return indexes_of_pattern_in_text;
}

//===========================================================================================================================
