#ifndef MIPT_3SEM_ALG_KNUTH_MORRIS_PRATT_H
#define MIPT_3SEM_ALG_KNUTH_MORRIS_PRATT_H

//===========================================================================================================================
#include <vector>
#include <string>

//===========================================================================================================================
// AIM: Return the vector of places where the patter exists the text (by the means of the KMT algorithm)
std::vector<size_t> knuth_morris_pratt(const std::string& text, const std::string& pattern);

//===========================================================================================================================

#endif //MIPT_3SEM_ALG_KNUTH_MORRIS_PRATT_H
