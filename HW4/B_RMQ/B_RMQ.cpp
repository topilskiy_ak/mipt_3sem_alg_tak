#include <fstream>
#include <limits>
#include <vector>

constexpr int INFINITY = std::numeric_limits<int>::max();

class SegmentTree {
    struct Vertice { int min_elem, second_min_elem; };
    
    std::vector<Vertice> tree;

public:
    struct Range { int begin, end; };

    SegmentTree(std::vector<int>& array_to_segment) {
        size_t nearest_power_of_two = 1;
        while (nearest_power_of_two < array_to_segment.size()) {
            nearest_power_of_two *= 2;
        }
        
        array_to_segment.resize(nearest_power_of_two, INFINITY);
        tree.resize(2 * nearest_power_of_two - 1, {INFINITY, INFINITY});
        
        for (size_t array_index = 0; array_index < array_to_segment.size(); ++array_index) {
            tree[array_to_segment.size() - 1 + array_index] = { array_to_segment[array_index], INFINITY };
        }

        for (int reverse_tree_index = (int)array_to_segment.size() - 2;
                 reverse_tree_index >= 0;
               --reverse_tree_index) {
            int min_elem =  std::min(tree[2 * reverse_tree_index + 1].min_elem,
                                     tree[2 * reverse_tree_index + 2].min_elem);

            int second_min_elem = std::min(         tree[2 * reverse_tree_index + 1].second_min_elem,
                                           std::min(tree[2 * reverse_tree_index + 2].second_min_elem,
                                                   (tree[2 * reverse_tree_index + 1].min_elem == min_elem) ?
                                                         tree[2 * reverse_tree_index + 2].min_elem :
                                                         tree[2 * reverse_tree_index + 1].min_elem));
            tree[reverse_tree_index] = {min_elem, second_min_elem};
        }
    }

    int second_min(const Range& range) {
        int left_min = INFINITY, right_min = INFINITY;
        int left_second_min = INFINITY, right_second_min = INFINITY;
        int left  = (int)(tree.size() - 1) / 2 + std::min(range.begin, range.end) - 1;
        int right = (int)(tree.size() - 1) / 2 + std::max(range.end, range.begin) - 1;

        if (left == right - 1) {
            return std::max(tree[left].min_elem, tree[right].min_elem);
        }

        while (left < right - 1) {

            if (left >= (tree.size() - 1) / 2) {
                if (left % 2 == 1) {
                    left_min =        std::min(tree[left].min_elem, tree[left + 1].min_elem);
                    left_second_min = std::max(tree[left].min_elem, tree[left + 1].min_elem);
                } else {
                    left_min =                 tree[left].min_elem;
                }

            } else {
                if (left % 2 == 1) {
                    if (left_min >= tree[left + 1].min_elem) {
                        left_second_min = std::min(left_min, tree[left + 1].second_min_elem);
                        left_min = tree[left + 1].min_elem;
                    } else {
                        if (left_second_min > tree[left + 1].min_elem) {
                            left_second_min = tree[left + 1].min_elem;
                        }
                    }
                }
            }

            if (right % 2 == 0) {
                if (right >= (tree.size() - 1) / 2) {
                    right_min = std::min(tree[right].min_elem, tree[right - 1].min_elem);
                    right_second_min = std::max(tree[right].min_elem, tree[right - 1].min_elem);
                } else {
                    if (right_min >= tree[right - 1].min_elem) {
                        right_second_min = std::min(right_min, tree[right - 1].second_min_elem);
                        right_min = tree[right - 1].min_elem;
                    } else {
                        if (right_second_min > tree[right - 1].min_elem) {
                            right_second_min = tree[right - 1].min_elem;
                        }
                    }
                }

            } else {
                if (right >= (tree.size() - 1) / 2) {
                    right_min = tree[right].min_elem;
                }
            }

            left  = (left % 2 == 0)  ? (left - 2)  / 2 : (left - 1)  / 2;
            right = (right % 2 == 0) ? (right - 2) / 2 : (right - 1) / 2;
        }

        if (left_min == right_min) { return left_min; }
        if (left_min < right_min) {
            return (left_second_min <= right_min) ? left_second_min : right_min;
        }
        return (right_second_min <= left_min) ? right_second_min : left_min;
    }
};

int main() {
    std::ifstream fin("input.txt");
    std::ofstream fout("output.txt");

    size_t array_length, number_of_ranges;
    fin >> array_length >> number_of_ranges;

    std::vector<int> array_to_segment(array_length);
    for (size_t array_index = 0; array_index < array_length; ++array_index) {
        fin >> array_to_segment[array_index];
    }

    SegmentTree tree(array_to_segment);

    SegmentTree::Range range;
    for (size_t read_ranges = 0; read_ranges < number_of_ranges; ++read_ranges) {
        fin >> range.begin >> range.end;
        fout << tree.second_min(range) << std::endl;
    }

    fin.close();
    fout.close();
    return 0;
}