#include <fstream>
#include <tree/tree.h>

int main() {
    std::ifstream fin("input.txt");
    std::ofstream fout("output.txt");

    int input_length = 0;
    int key = 0, priority = 0;

    fin >> input_length;
    fin >> key >> priority;

    Treap treap(key, priority);
    BinaryTree bin_tree(key);

    while (--input_length > 0) {
        fin >> key >> priority;
        treap.insert(key, priority);
        bin_tree.insert(key);
    }

    fout << bin_tree.get_height() - treap.get_height();

    fin.close();
    fout.close();
    return 0;
}