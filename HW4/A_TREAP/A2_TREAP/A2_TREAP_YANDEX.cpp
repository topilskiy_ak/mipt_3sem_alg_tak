#include <fstream>
#include <memory>
#include <vector>

//===========================================================================================================================
// A basic Tree that can insert and check its max height/width
template<typename Node>
class Tree {
protected:
    using NodePtr = std::shared_ptr<Node>;

private:
    int _get_height(NodePtr root, int current_height) const {
        if (root == nullptr) { return current_height; }
        return std::max(_get_height(root->left,  current_height + 1),
                        _get_height(root->right, current_height + 1));
    }

    void _get_all_widths(NodePtr root, int current_height, std::vector<int>& widths) const {
        if (root == nullptr) { return; }

        if (current_height >= widths.size()) { widths.push_back(1); }
        else { ++widths[current_height]; }

        _get_all_widths(root->left,  current_height + 1, widths);
        _get_all_widths(root->right, current_height + 1, widths);
    }

protected:
    NodePtr _root = nullptr;

    virtual void _insert(NodePtr& root, NodePtr elem) = 0;

public:
    Tree() {}
    Tree(const Node& root_init) : _root(std::make_shared<Node>(root_init)) {}

    void insert(const Node& elem) { _insert(_root, std::make_shared<Node>(elem)); }

    int get_height() const { return _get_height(_root, 0); }

    int get_max_width() const {
        std::vector<int> widths;
        _get_all_widths(_root, 0, widths);

        int max_width = 0;
        for (const int& width : widths) { if (width > max_width) { max_width = width; } }

        return max_width;
    }
};

//===========================================================================================================================
// A Node for a Treap Tree
struct TreapNode {
    int key, priority;
    std::shared_ptr<TreapNode> left, right;

    TreapNode(const int& key_init = 0, const int& priority_init = 0) :
            key(key_init), priority(priority_init), left(nullptr), right(nullptr) {}
};

// Treap class that can insert and check its max height/width
class Treap : public Tree<TreapNode> {
private:
    void _split(NodePtr root, const int& split_key, NodePtr& split_left, NodePtr& split_right) {
        if (root == nullptr) {
            split_left = nullptr;
            split_right = nullptr;
        } else {
            if (split_key < root->key) {
                _split(root->left,  split_key, split_left,   root->left);
                split_right = root;
            } else {
                _split(root->right, split_key, root->right, split_right);
                split_left = root;
            }
        }
    }

    void _insert(NodePtr& root, NodePtr elem) override {
        if (root == nullptr) {
            root = elem;
        } else {
            if (elem->priority > root->priority) {
                _split(root, elem->key, elem->left, elem->right);
                root = elem;
            } else if (elem->key < root->key) {
                _insert(root->left, elem);
            } else { // elem->priority <= root->priority && elem->key >= root->key
                _insert(root->right, elem);
            }
        }
    }

public:
    Treap(const int& root_key_init = 0, const int& root_priority_init = 0) :
            Tree(TreapNode(root_key_init, root_priority_init)) {}

    void insert(const int& key, const int& priority) {
        _insert(_root, std::make_shared<TreapNode>(key, priority));
    }
};

//===========================================================================================================================
// A Node for a Binary Tree
struct BinaryTreeNode {
    int key;
    std::shared_ptr<BinaryTreeNode> left, right;

    BinaryTreeNode(const int& key_init = 0) : key(key_init), left(nullptr), right(nullptr) {}
};

// BinaryTree class that can insert and check its max height/width
class BinaryTree : public Tree<BinaryTreeNode> {
private:
    void _insert(NodePtr& root, NodePtr elem) override {
        if (root == nullptr) {
            root = elem;
        } else {
            if     (elem->key >= root->key)   { _insert(root->right, elem); }
            else /* elem->key <  root->key */ { _insert(root->left,  elem); }
        }
    }

public:
    BinaryTree(const int& root_key_init = 0) : Tree(BinaryTreeNode(root_key_init)) {}

    void insert(const int& key) { _insert(_root, std::make_shared<BinaryTreeNode>(key)); }
};

int main() {
    std::ifstream fin("input.txt");
    std::ofstream fout("output.txt");

    int input_length = 0;
    int key = 0, priority = 0;

    fin >> input_length;
    fin >> key >> priority;

    Treap treap(key, priority);
    BinaryTree bin_tree(key);

    while (--input_length > 0) {
        fin >> key >> priority;
        treap.insert(key, priority);
        bin_tree.insert(key);
    }

    fout << treap.get_max_width() - bin_tree.get_max_width();

    fin.close();
    fout.close();
    return 0;
}