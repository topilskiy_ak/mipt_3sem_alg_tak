#include <fstream>
#include <vector>
#include <string>
#include <set>

// AIM: Return the minimum (lexicographically) string derivable from the given prefix function
std::string prefix_func_to_min_string(const std::vector<size_t>& prefix_func) {
    std::string min_string(prefix_func.size(), '\0');

    char last_char_used = 'a';
    std::set<char> used_characters;

    if (prefix_func.size() == 0) {
        return min_string;
    } else {
        min_string[0] = last_char_used;
    }

    for (size_t index_prefix_func = 1; index_prefix_func < prefix_func.size(); ++index_prefix_func) {
        if (prefix_func[index_prefix_func] > 0) {
            min_string[index_prefix_func] = min_string[prefix_func[index_prefix_func] - 1];

        } else {

            for (size_t max_previous_prefix_length = prefix_func[index_prefix_func - 1];
                 max_previous_prefix_length > 0;
                 max_previous_prefix_length = prefix_func[max_previous_prefix_length - 1]) {

                used_characters.insert(min_string[max_previous_prefix_length]);
            }

            for (char index_char = 'a' + 1; index_char <= last_char_used; ++index_char) {
                if (used_characters.find(index_char) == used_characters.end()) {
                    min_string[index_prefix_func] = index_char;
                    break;
                }
            }

            if (min_string[index_prefix_func] == '\0') {
                last_char_used++;
                min_string[index_prefix_func] = last_char_used;
            }

            used_characters.clear();
        }
    }

    return min_string;
}

// AIM: Read a prefix function from CIN, output the derived minimum string to COUT
int main() {
    std::ifstream fin("input.txt");
    std::ofstream fout("output.txt");

    std::vector<size_t> pref_function;
    size_t value_read;

    while (fin >> value_read) {
        pref_function.push_back(value_read);
    }

    fout << prefix_func_to_min_string(pref_function);

    return 0;
}