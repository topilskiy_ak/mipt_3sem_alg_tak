#include <fstream>

#include <string/string_data_structures/string_functions/string_functions.h>

// AIM: Read a zed function from CIN, output the derived minimum string to COUT
int main() {
    std::ifstream fin("input.txt");
    std::ofstream fout("output.txt");

    std::vector<size_t> zed_function;
    size_t value_read;

    while (fin >> value_read) {
        zed_function.push_back(value_read);
    }

    fout << zed_func_to_min_string(zed_function);

    fin.close();
    fout.close();
    return 0;
}