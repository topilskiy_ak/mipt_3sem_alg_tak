#include <fstream>
#include <set>
#include <vector>
#include <string>

static const char ALPHABET_START = 'a';

// AIM: Return the minimum (lexicographically) string derivable from the given zed function
std::string zed_func_to_min_string(const std::vector<size_t>& zed_func) {
    if (zed_func.size() == 0) {
        return "";
    }

    std::vector<size_t> prefix_func(zed_func.size(), 0);

    for (size_t index_zed_func = 1; index_zed_func < zed_func.size(); ++index_zed_func) {
        if (zed_func[index_zed_func] > 0) {
            size_t index_prefix = zed_func[index_zed_func];
            do {
                --index_prefix;
                if (prefix_func[index_zed_func + index_prefix] > 0) {
                    break;
                } else {
                    prefix_func[index_zed_func + index_prefix] = index_prefix + 1;
                }
            } while (index_prefix > 0);
        }
    }

    char last_char_used = ALPHABET_START;
    std::set<char> used_characters;

    std::string min_string(prefix_func.size(), '\0');
    min_string[0] = last_char_used;

    for (size_t index_prefix_func = 1; index_prefix_func < prefix_func.size(); ++index_prefix_func) {
        if (prefix_func[index_prefix_func] > 0) {
            min_string[index_prefix_func] = min_string[prefix_func[index_prefix_func] - 1];

        } else {

            for (size_t max_previous_prefix_length = prefix_func[index_prefix_func - 1];
                 max_previous_prefix_length > 0;
                 max_previous_prefix_length = prefix_func[max_previous_prefix_length - 1]) {

                used_characters.insert(min_string[max_previous_prefix_length]);
            }

            for (char index_char = ALPHABET_START + 1; index_char <= last_char_used; ++index_char) {
                if (used_characters.find(index_char) == used_characters.end()) {
                    min_string[index_prefix_func] = index_char;
                    break;
                }
            }

            if (min_string[index_prefix_func] == '\0') {
                last_char_used++;
                min_string[index_prefix_func] = last_char_used;
            }

            used_characters.clear();
        }
    }

    return min_string;
}

// AIM: Read a prefix function from CIN, output the derived minimum string to COUT
int main() {
    std::ifstream fin("input.txt");
    std::ofstream fout("output.txt");

    std::vector<size_t> zed_function;
    size_t value_read;

    while (fin >> value_read) {
        zed_function.push_back(value_read);
    }

    fout << zed_func_to_min_string(zed_function);

    return 0;
}