#include <fstream>

#include <string/string_algorithms/aho_corasick_specialized/aho_corasick_specialized.h>

// Read a pattern and text from file,
// Output all the occurrences of pattern in text to file
int main() {
    std::ifstream fin("input.txt");
    std::ofstream fout("output.txt");

    std::string pattern;
    std::string text;

    fin >> pattern >> text;

    std::vector<size_t> indexes_of_pattern_in_text = locate_question_pattern_in_text(pattern, text);

    for (const size_t& elem : indexes_of_pattern_in_text) {
        fout << elem << " ";
    }

    fin.close();
    fout.close();
    return 0;
}