cmake_minimum_required(VERSION 3.6)
project(C_PAT)

#===========================================================================================================================
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})

#===========================================================================================================================
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(HEADER_FILES aho_corasick_specialized.h)
set(SOURCE_FILES C_PAT.cpp)

add_executable(C_PAT ${SOURCE_FILES})
target_link_libraries(C_PAT aho_corasick_specialized)

#===========================================================================================================================

