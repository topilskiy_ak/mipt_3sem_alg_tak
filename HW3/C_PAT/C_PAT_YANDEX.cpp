//===========================================================================================================================
#include <fstream>
#include <map>
#include <vector>
#include <limits>

//===========================================================================================================================
// Class that implements the Aho-Corasick Automaton for the Aho-Corasick Algorithm
class AhoCorasickAutomaton {
protected:
    static constexpr int NULL_LINK = std::numeric_limits<int>::max();

    // Struct for the State of the Automaton
    struct State {
        int parent_state = 0;
        char   char_from_parent = '\0';

        int sufflink = NULL_LINK;
        int condensed_sufflink = NULL_LINK;

        std::map<char, int> children_states_by_direct_chars;
        std::map<char, int> children_states_by_links;

        std::vector<int> terminal_pattern_numbers;


        // STANDARD CONSTRUCTOR:
        explicit State(const int& parent_state_init) : parent_state(parent_state_init) {}


        // AIM: check whether this state has an initialized sufflink
        bool has_sufflink() { return sufflink != NULL_LINK; }

        // AIM: check whether this state has an initialized condensed_sufflink
        bool has_condensed_sufflink() { return condensed_sufflink != NULL_LINK; }

        // AIM: check whether this state has a direct link by character
        bool has_direct_link(const char& linking_character) {
            return !(children_states_by_direct_chars.find(linking_character) ==
                     children_states_by_direct_chars.end());
        }

        // AIM: check whether this state has any link by character
        bool has_any_link(const char& linking_character) {
            return !(children_states_by_links.find(linking_character) ==
                     children_states_by_links.end());
        }
    };

    std::vector<State> _states;
    std::vector<std::string> _patterns;

    // AIM: Return the next_state, reachable from state by the way of linking_character
    //      (calculate it if needed)
    int _get_any_link(const int& state, const char& linking_character);

    // AIM: Return the sufflink of the state (calculate it if needed)
    int _get_sufflink(const int& state);

    // AIM: Return the condensed sufflink of the state (calculate it if needed)
    int _get_condensed_sufflink(const int& state);

public:
    // EMPTY CONSTRUCTOR:
    AhoCorasickAutomaton();

    // STANDARD CONSTRUCTOR:
    AhoCorasickAutomaton(const std::vector<std::string>& patterns);

    // AIM: Add the pattern to the automaton
    void add_pattern(const std::string& pattern);

    // AIM: Return the patterns recorded in the automaton
    const std::vector<std::string> get_patterns() const { return _patterns; }


    // AIM: Return the vector which contains for every point in the text
    //      the vector of indexes of patterns which begin in that point
    std::vector<std::vector<int>> get_vector_of_patterns_in_text(const std::string& text);

    // AIM: Return map, which reflects every point in the text
    //      to the vector of indexes of patterns which begin in that point
    std::map<int, std::vector<int>> get_map_of_patterns_in_text(const std::string& text);
};


//===========================================================================================================================

//===========================================================================================================================
// AIM: Return the next_state, reachable from state by the way of linking_character
//      (calculate it if needed)
int AhoCorasickAutomaton::_get_any_link(const int& state, const char& linking_character) {
    if (!_states[state].has_any_link(linking_character)) {
        if (_states[state].has_direct_link(linking_character)) {
            _states[state].children_states_by_links[linking_character] =
                    _states[state].children_states_by_direct_chars[linking_character];
        } else {
            if (state == 0) {
                _states[state].children_states_by_links[linking_character] = 0;
            } else {
                _states[state].children_states_by_links[linking_character] =
                        _get_any_link(_get_sufflink(state), linking_character);
            }
        }
    }

    return _states[state].children_states_by_links[linking_character];
}

// AIM: Return the sufflink of the state (calculate it if needed)
int AhoCorasickAutomaton::_get_sufflink(const int& state) {
    if (!_states[state].has_sufflink()) {
        if (_states[state].parent_state == 0) {
            _states[state].sufflink = 0;
        } else {
            _states[state].sufflink = _get_any_link(_get_sufflink(_states[state].parent_state),
                                                    _states[state].char_from_parent);
        }
    }

    return _states[state].sufflink;
}

// AIM: Return the condensed sufflink of the state (calculate it if needed)
int AhoCorasickAutomaton::_get_condensed_sufflink(const int& state) {
    if (!_states[state].has_condensed_sufflink()) {
        int sufflink_state = _get_sufflink(state);

        if (_states[sufflink_state].terminal_pattern_numbers.empty()) {
            _states[state].condensed_sufflink = _get_condensed_sufflink(sufflink_state);
        } else {
            _states[state].condensed_sufflink = sufflink_state;
        }
    }

    return _states[state].condensed_sufflink;
}

//---------------------------------------------------------------------------------------------------------------------------
// EMPTY CONSTRUCTOR:
AhoCorasickAutomaton::AhoCorasickAutomaton() {
    _states.emplace_back(0);

    _states[0].sufflink = 0;
    _states[0].condensed_sufflink = 0;
}

// STANDARD CONSTRUCTOR:
AhoCorasickAutomaton::AhoCorasickAutomaton(const std::vector<std::string>& patterns) : AhoCorasickAutomaton() {
    for (const std::string& pattern : patterns) {
        add_pattern(pattern);
    }
}

// AIM: Add the pattern to the automaton
void AhoCorasickAutomaton::add_pattern(const std::string& pattern) {
    int current_state_index = 0;

    for (const char& character : pattern) {
        if (!_states[current_state_index].has_direct_link(character)) {
            _states[current_state_index].children_states_by_direct_chars[character] = _states.size();
            _states.emplace_back(current_state_index);
            _states.back().char_from_parent = character;
        }

        current_state_index = _states[current_state_index].children_states_by_direct_chars[character];
    }

    _states[current_state_index].terminal_pattern_numbers.push_back(_patterns.size());
    _patterns.push_back(pattern);
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM: Return the vector which contains for every point in the text
//      the vector of indexes of patterns which begin in that point
std::vector<std::vector<int>> AhoCorasickAutomaton::get_vector_of_patterns_in_text(
        const std::string& text) {
    std::vector<std::vector<int>> patterns_in_text(text.size(), std::vector<int>(0));

    int current_state = 0;
    for (int text_index = 0; text_index < text.size(); ++text_index) {
        char character = text[text_index];
        current_state = _get_any_link(current_state, character);

        int condensed_sufflink_state = current_state;
        while (condensed_sufflink_state != 0) {
            for (const int& pattern_index : _states[condensed_sufflink_state].terminal_pattern_numbers) {
                int index_of_pattern_start = text_index - _patterns[pattern_index].size() + 1;
                patterns_in_text[index_of_pattern_start].push_back(pattern_index);
            }

            condensed_sufflink_state = _get_condensed_sufflink(condensed_sufflink_state);
        }
    }

    return patterns_in_text;
}

// AIM: Return map, which reflects every point in the text
//      to the vector of indexes of patterns which begin in that point
std::map<int, std::vector<int>> AhoCorasickAutomaton::get_map_of_patterns_in_text(
        const std::string& text) {
    std::map<int, std::vector<int>> patterns_in_text;

    int current_state = 0;
    for (int text_index = 0; text_index < text.size(); ++text_index) {
        char character = text[text_index];
        current_state = _get_any_link(current_state, character);

        int condensed_sufflink_state = current_state;
        while (condensed_sufflink_state != 0) {
            for (const int& pattern_index : _states[condensed_sufflink_state].terminal_pattern_numbers) {
                int index_of_pattern_start = text_index - _patterns[pattern_index].size() + 1;
                patterns_in_text[index_of_pattern_start].push_back(pattern_index);
            }

            condensed_sufflink_state = _get_condensed_sufflink(condensed_sufflink_state);
        }
    }

    return patterns_in_text;
};

//===========================================================================================================================
// Wrapper for the algorithm of finding a pattern with question marks in a text
class LocateQuestionPatternInTextWrapper : private AhoCorasickAutomaton {
private:
    const std::string& _pattern;
    const std::string& _text;

    std::vector<int> _pattern_pieces_end_offset;

    // Separate the pattern string into pieces with no question marks
    // and add them to the AhoCorasickAutomaton
    void _separate_pattern_into_pieces();

    // Return the number of pattern pieces in each index of text
    // using the starts of the pieces and information on their place in the pattern
    std::vector<int> _calculate_pattern_pieces_beginning();

public:
    // STANDARD CONSTRUCTOR
    LocateQuestionPatternInTextWrapper(const std::string& pattern_init, const std::string& text_init) :
            AhoCorasickAutomaton(), _pattern(pattern_init), _text(text_init) { _separate_pattern_into_pieces(); }

    // Return all the instances of pattern (with question marks in mind) in text
    std::vector<int> calculate_pattern_locations_in_text();
};

//===========================================================================================================================
// Separate the pattern string into pieces with no question marks
// and add them to the AhoCorasickAutomaton
void LocateQuestionPatternInTextWrapper::_separate_pattern_into_pieces() {
    std::string reading_string = "";
    for (int pattern_index = 0; pattern_index < _pattern.size(); ++pattern_index) {
        if (_pattern[pattern_index] != '?') {
            reading_string += _pattern[pattern_index];
        } else if (!reading_string.empty()) {
            add_pattern(reading_string);
            _pattern_pieces_end_offset.push_back(pattern_index);
            reading_string.clear();
        }
    }

    if (!reading_string.empty()) {
        add_pattern(reading_string);
        _pattern_pieces_end_offset.push_back(_pattern.size());
    }
}

// Return the number of pattern pieces in each index of text
// using the starts of the pieces and information on their place in the pattern
std::vector<int> LocateQuestionPatternInTextWrapper::_calculate_pattern_pieces_beginning() {
    std::vector<int> _pattern_pieces_beginning(_text.size(), 0);

    int current_state = 0;
    for (int text_index = 0; text_index < _text.size(); ++text_index) {
        char character = _text[text_index];
        current_state = _get_any_link(current_state, character);

        int condensed_sufflink_state = current_state;
        while (condensed_sufflink_state != 0) {
            for (const int& pattern_piece_index :
                    _states[condensed_sufflink_state].terminal_pattern_numbers) {
                if (text_index + 1 >= _pattern_pieces_end_offset[pattern_piece_index]) {
                    ++_pattern_pieces_beginning[text_index + 1 - _pattern_pieces_end_offset[pattern_piece_index]];
                }
            }

            condensed_sufflink_state = _get_condensed_sufflink(condensed_sufflink_state);
        }
    }

    return _pattern_pieces_beginning;
}

//===========================================================================================================================
// Return all the instances of pattern (with question marks in mind) in text
std::vector<int> LocateQuestionPatternInTextWrapper::calculate_pattern_locations_in_text() {
    std::vector<int> _pattern_pieces_beginning = _calculate_pattern_pieces_beginning();
    std::vector<int> pattern_locations_in_text;

    for (int text_index = 0;
         text_index + _pattern.size() <= _pattern_pieces_beginning.size();
         ++text_index) {

        if(_patterns.size() == _pattern_pieces_beginning[text_index]) {
            pattern_locations_in_text.push_back(text_index);
        }
    }

    return pattern_locations_in_text;
}
//===========================================================================================================================
// Find all the occurrences of pattern (with masked question marks) in text
std::vector<int> locate_question_pattern_in_text(const std::string& pattern, const std::string& text) {
    return LocateQuestionPatternInTextWrapper(pattern, text).calculate_pattern_locations_in_text();
}

//===========================================================================================================================


//===========================================================================================================================

// Read a pattern and text from file,
// Output all the occurrences of pattern in text to file
int main() {
    std::ifstream fin("input.txt");
    std::ofstream fout("output.txt");

    std::string pattern;
    std::string text;

    fin >> pattern >> text;

    std::vector<int> indexes_of_pattern_in_text = locate_question_pattern_in_text(pattern, text);

    for (const int& elem : indexes_of_pattern_in_text) {
        fout << elem << " ";
    }

    fin.close();
    fout.close();
    return 0;
}