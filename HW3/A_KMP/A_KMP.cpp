#include <fstream>

#include <string/string_algorithms/knuth_morris_pratt/knuth_morris_pratt.h>

// AIM: Read a pattern and text from File,
//      then output all the occurrences of the pattern in the text
int main() {
    std::ifstream fin("input.txt");
    std::ofstream fout("output.txt");

    std::string pattern;
    std::string text;

    fin >> pattern >> text;

    std::vector<size_t> indexes_of_pattern_in_text = knuth_morris_pratt(text, pattern);

    for (const size_t& elem : indexes_of_pattern_in_text) {
        fout << elem << " ";
    }

    fin.close();
    fout.close();
    return 0;
}