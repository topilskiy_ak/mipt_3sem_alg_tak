#include <fstream>
#include <vector>
#include <string>

static const char ALPHABET_START = 'a';

//===========================================================================================================================
// AIM: Return the prefix function of text
std::vector<size_t> string_to_prefix_func(const std::string& text) {
    if (text.size() == 0) {
        return std::vector<size_t>(0);
    }

    std::vector<size_t> prefix_func(text.size(), 0);

    for (size_t index_prefix_func = 1; index_prefix_func < prefix_func.size(); ++index_prefix_func) {
        size_t max_previous_prefix_length = prefix_func[index_prefix_func - 1];

        while (max_previous_prefix_length > 0 &&
               text[index_prefix_func] != text[max_previous_prefix_length]) {
            max_previous_prefix_length = prefix_func[max_previous_prefix_length - 1];
        }

        prefix_func[index_prefix_func] = max_previous_prefix_length;

        if (text[index_prefix_func] == text[max_previous_prefix_length]) {
            prefix_func[index_prefix_func]++;
        }
    }

    return prefix_func;
}


//===========================================================================================================================
// AIM: Return the vector of places where the patter exists the text (by the means of the KMT algorithm)
std::vector<size_t> knuth_morris_pratt(const std::string& text, const std::string& pattern) {
    std::vector<size_t> indexes_of_pattern_in_text;

    std::vector<size_t> prefix_func = string_to_prefix_func(pattern + "$");
    size_t prefix_func_of_text = 0;

    for (size_t index_text = 0; index_text < text.size(); ++index_text) {
        size_t max_previous_prefix_length = prefix_func_of_text;

        while (max_previous_prefix_length > 0 &&
               text[index_text] != pattern[max_previous_prefix_length]) {
            max_previous_prefix_length = prefix_func[max_previous_prefix_length - 1];
        }

        prefix_func_of_text = max_previous_prefix_length;

        if (text[index_text] == pattern[max_previous_prefix_length]) {
            prefix_func_of_text++;
        }

        if (prefix_func_of_text == pattern.size()) {
            indexes_of_pattern_in_text.push_back(index_text - pattern.size() + 1);
        }
    }

    return indexes_of_pattern_in_text;
}

//===========================================================================================================================

// AIM: Read a prefix function from CIN, output the derived minimum string to COUT
int main() {
    std::ifstream fin("input.txt");
    std::ofstream fout("output.txt");

    std::string pattern;
    std::string text;

    fin >> pattern >> text;

    std::vector<size_t> indexes_of_pattern_in_text = knuth_morris_pratt(text, pattern);

    for (const size_t& elem : indexes_of_pattern_in_text) {
        fout << elem << " ";
    }

    fin.close();
    fout.close();
    return 0;
}