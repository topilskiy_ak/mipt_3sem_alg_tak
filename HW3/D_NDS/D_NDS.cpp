#include <fstream>
#include <string>
#include <vector>

static constexpr size_t ALPHABET_SIZE = 'z' - 'a' + 1;
static constexpr size_t ALPHABET_START = 'a' - 1;

class NumberDifferentSubstringsWrapper {
private:
    std::string _text;

    std::vector<std::vector<size_t>> _list_suffix_classes;
    std::vector<size_t> _suffix_array;
    std::vector<size_t> _lcp_array;

    void _construct_primal_suffix_array() {
        std::vector<size_t> offsets(ALPHABET_SIZE, 0);
        for (size_t text_index = 0; text_index < _text.size(); ++text_index) {
            if (_text[text_index] == '$') {
                ++offsets[0];
            } else {
                ++offsets[_text[text_index] - ALPHABET_START];
            }
        }
        for (size_t offsets_index = 1; offsets_index < ALPHABET_SIZE; ++offsets_index) {
            offsets[offsets_index] += offsets[offsets_index - 1];
        }
        for (size_t text_index = 0; text_index < _text.size(); ++text_index) {
            if (_text[text_index] == '$') {
                _suffix_array[--offsets[0]] = text_index;
            } else {
                _suffix_array[--offsets[_text[text_index] - ALPHABET_START]] = text_index;
            }
        }

        std::vector<size_t> suffix_classes(_text.size(), 0);
        suffix_classes[_suffix_array[0]] = 0;
        size_t num_suffix_classes = 1;
        for (size_t suffix_array_index = 1; suffix_array_index < _text.size(); ++suffix_array_index) {
            if (_text[_suffix_array[suffix_array_index]] != _text[_suffix_array[suffix_array_index - 1]]) {
                ++num_suffix_classes;
            }
            suffix_classes[_suffix_array[suffix_array_index]] = num_suffix_classes - 1;
        }
        _list_suffix_classes.push_back(suffix_classes);
    }

    void _construct_2pow_suffix_array(const size_t& index_suffix_classes) {
        std::vector<size_t>& _prev_suffix_array = _list_suffix_classes[index_suffix_classes];
        size_t current_offset = (1UL << index_suffix_classes);

        std::vector<size_t> _tmp_suffix_array(_text.size(), 0);
        for (size_t suffix_array_index = 0; suffix_array_index < _text.size(); ++suffix_array_index) {
            if (current_offset > _suffix_array[suffix_array_index]) {
                _tmp_suffix_array[suffix_array_index] = _suffix_array[suffix_array_index] + _text.size()
                                                        - current_offset;
            } else {
                _tmp_suffix_array[suffix_array_index] = _suffix_array[suffix_array_index] - current_offset;
            }
        }

        std::vector<size_t> offsets(ALPHABET_SIZE, 0);
        for (size_t offsets_index = 0; offsets_index < _text.size(); ++offsets_index) {
            ++offsets[_prev_suffix_array[_tmp_suffix_array[offsets_index]]];
        }
        for (size_t offsets_index = 1; offsets_index < ALPHABET_SIZE; ++offsets_index) {
            offsets[offsets_index] += offsets[offsets_index - 1];
        }
        for (long offsets_index = _text.size() - 1; offsets_index >= 0; --offsets_index) {
            _suffix_array[--offsets[_prev_suffix_array[_tmp_suffix_array[offsets_index]]]] =
                    _tmp_suffix_array[offsets_index];
        }

        std::vector<size_t> suffix_classes(_text.size(), 0);
        suffix_classes[_suffix_array[0]] = 0;
        size_t num_suffix_classes = 1;
        for (size_t suffix_array_index = 1; suffix_array_index < _text.size(); ++suffix_array_index) {
            size_t current_prev_suffix_array_index =
                    (_suffix_array[suffix_array_index] + current_offset) % _text.size();
            size_t neighbor_prev_suffix_array_index =
                    (_suffix_array[suffix_array_index - 1] + current_offset) % _text.size();
            if ((_prev_suffix_array[_suffix_array[suffix_array_index]] !=
                 _prev_suffix_array[_suffix_array[suffix_array_index - 1]])
                ||
                (_prev_suffix_array[current_prev_suffix_array_index] !=
                 _prev_suffix_array[neighbor_prev_suffix_array_index])) {
                ++num_suffix_classes; 
            }

            suffix_classes[_suffix_array[suffix_array_index]] = num_suffix_classes - 1;
        }
        _list_suffix_classes.push_back(suffix_classes);
    }

    void _construct_suffix_array() {
        _construct_primal_suffix_array();

        // quickly calculate 2^k by using bit-operations: 1 << k
        for (size_t index_suffix_classes = 0;
             (1UL << index_suffix_classes) < _text.size();
             ++index_suffix_classes) {
            _construct_2pow_suffix_array(index_suffix_classes);
        }
    }

    void _construct_lcp_array() {
        std::vector<size_t> suffix_positions(_text.size());
        for (size_t suffix_index = 0; suffix_index < _text.size(); ++suffix_index) {
            suffix_positions[_suffix_array[suffix_index]] = suffix_index;
        }

        size_t max_neighbor_common_suffix = 0;
        for (size_t suffix_positions_index = 0; suffix_positions_index < _text.size(); ++suffix_positions_index) {
            if (max_neighbor_common_suffix > 0) {
                --max_neighbor_common_suffix;
            }

            size_t text_index = suffix_positions[suffix_positions_index];

            if (text_index == _text.size() - 1) {
                _lcp_array[text_index] = 0;
                max_neighbor_common_suffix = 0;
            } else {
                size_t neighbor_text_index = _suffix_array[text_index + 1];

                for (;  suffix_positions_index + max_neighbor_common_suffix < _text.size() &&
                        neighbor_text_index + max_neighbor_common_suffix < _text.size()    &&
                        _text[suffix_positions_index + max_neighbor_common_suffix] ==
                        _text[neighbor_text_index    + max_neighbor_common_suffix];
                       ++max_neighbor_common_suffix);

                _lcp_array[text_index] = max_neighbor_common_suffix;
            }
        }
    }

public:
    NumberDifferentSubstringsWrapper(const std::string& text_init) :
            _text(text_init + '$'),
            _list_suffix_classes(0),
            _suffix_array       (_text.size()),
            _lcp_array          (_text.size())  {
        _construct_suffix_array();
        _construct_lcp_array();
    }

    size_t get_number_of_different_substrings() {
        size_t number_of_different_substrings = 0;

        for (size_t array_index = 1; array_index < _text.size() - 1; ++array_index) {
            number_of_different_substrings += _text.size() - 1
                                              - _suffix_array[array_index]
                                              - _lcp_array[array_index];
        }

        number_of_different_substrings += _text.size() - 1 - _suffix_array[_text.size() - 1];

        return number_of_different_substrings;
    }
};

// Read a text from file,
// Output the number of different substrings in text
int main() {
    std::ifstream fin("input.txt");
    std::ofstream fout("output.txt");

    std::string text;

    fin >> text;

    fout << NumberDifferentSubstringsWrapper(text).get_number_of_different_substrings();

    fin.close();
    fout.close();
    return 0;
}