#include <fstream>
#include <queue>
#include <forward_list>
#include <algorithm>
#include <set>
#include <type_traits>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <sstream>
#include <stdexcept>
#include <initializer_list>

//===========================================================================================================================
// Класс Матриц
template <typename T>
class plain_matrix {
private:
    size_t _h;     // Высота Матрицы
    size_t _w;     // Ширина Матрицы
    std::vector<T> _m;  // Содержимое Матрицы

public:
    // CONSTRUCTOR: <h> - высота Матрицы, <w> - ширина Матрицы
    plain_matrix(size_t h = 0, size_t w = 0) : _h(h), _w(w), _m(w * h) {}
    // CONSTRUCTOR: <source> - Матрица, с которой делается копия
    plain_matrix(const plain_matrix& source) : _h(source._h), _w(source._w), _m(source._m) {}
    // CONSTRUCTOR: <l> - список списков эелементов, которыми надо инициализировать Матрицу
    plain_matrix(std::initializer_list<std::initializer_list<T>> l);

    // AIM: Вернуть кол-во строк в Матрице
    size_t h()    const { return _h; }
    // AIM: Вернуть кол-во столбцов в Матрице
    size_t w()    const { return _w; }
    // AIM: Вернуть кол=во элементов в Матрице
    size_t size() const { return _h * _w; }

    // Вернуть ссылку на <i, j>-ый элемент Матрицы
    T& operator () (size_t i, size_t j);
    // Вернуть конст-ссылку на <i, j>-ый элемент Матрицы
    const T& operator () (size_t i, size_t j) const;

    // AIM: Оператор присвоения данной Матрице значения Матрицы <m>
    // OUT: Конст-ссылка на данную Матрицу
    const plain_matrix& operator=(const plain_matrix& m);
    // AIM: Оператор перемещения данной Матрице значения Матрицы <m>
    // OUT: Конст-ссылка на данную Матрицу
    const plain_matrix& operator=(const plain_matrix&& m);

    // AIM: Вернуть строку содержимого матрицы
    std::string to_string() const;
};
//---------------------------------------------------------------------------------------------------------------------------
// CONSTRUCTOR: <l> - список списков эелементов, которыми надо инициализировать Матрицу
template <typename T>
plain_matrix<T>::plain_matrix(std::initializer_list<std::initializer_list<T>> l) {
    _h = l.size();
    _w = _h > 0 ? l.begin()->size() : 0;
    _m.resize(_w * _h);
    // Итератор по вектору Матрицы
    size_t pos = 0;
    for (std::initializer_list<T> const& rowList : l) {
        for (const T& value : rowList)
        {
            _m[pos] = value;
            pos++;
        }
    }
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM: Оператор присвоения данной Матрице значения Матрицы <m>
// OUT: Конст-ссылка на данную Матрицу
template <typename T>
const plain_matrix<T>& plain_matrix<T>::operator=(const plain_matrix& m) {
    _w = m._w;
    _h = m._h;
    _m = m._m;
    return *this;
}

// AIM: Оператор перемещения данной Матрице значения Матрицы <m>
// OUT: Конст-ссылка на данную Матрицу
template <typename T>
const plain_matrix<T>& plain_matrix<T>::operator=(const plain_matrix&& m) {
    _w = m._w;
    _h = m._h;
    _m = move(m._m);
    return *this;
}

//---------------------------------------------------------------------------------------------------------------------------
// Вернуть ссылку на <i, j>-ый элемент Матрицы
template <typename T>
T& plain_matrix<T>::operator()(size_t i, size_t j) {
    if (i + 1 > _w || j + 1 > _h) {
        throw std::out_of_range("No such place in plain_matrix.");
    } else {
        return _m[_w * i + j];
    }
}

// Вернуть конст-ссылку на <i, j>-ый элемент Матрицы
template <typename T>
const T& plain_matrix<T>::operator () (size_t i, size_t j) const {
    if (i + 1 > _w || j + 1 > _h) {
        throw std::out_of_range("No such place in plain_matrix.");
    } else {
        return _m[_w * i + j];
    }
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM: Вернуть строку содержимого матрицы
template <typename T>
std::string plain_matrix<T>::to_string() const {
    std::stringstream output;

    for (size_t i = 0; i < _h; i++) {
        for (size_t j = 0; j < _w; j++) {
            output << (*this)(i, j) << " ";
        }
        output << " ";
    }

    return output.str();
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM: Вывесту в STDOUT Матрицу
template <typename T>
std::ostream& operator << (std::ostream& os, const plain_matrix<T>& m) {
    for (size_t i = 0, h = m.h(); i < h; i++) {
        for (size_t j = 0, w = m.w(); j < w; j++) {
            std::cout << std::setw(3) << m(i, j) << ", ";
        }
        std::cout << std::endl;
    }
    return os;
}
//===========================================================================================================================
//===========================================================================================================================
// Структура Ребра Графа
struct Edge {
    static constexpr double BASE_WEIGHT = 1.0;  // Вес невзвешанного ребра

    size_t v1;       // Вершина Начала Ребра
    size_t v2;       // Вершина Конца Ребра
    double weight;   // Вес Ребра

    Edge(size_t v = 0, size_t e = 0, double w = BASE_WEIGHT) : v1(v), v2(e), weight(w) {}
};
//---------------------------------------------------------------------------------------------------------------------------
// Абстрактный Класс Графа
class Graph {
protected:
    bool _is_directed = false; // Является ли Граф Ориентированным
    size_t _num_vertices = 0;  // Число вершин в Графе
    size_t _num_edges = 0;     // Число ребер в Графе

public:
    // STANDART CONSTRUCTOR:
    Graph(const bool is_directed = false, const size_t num_vertices = 0, const size_t num_edges = 0) :
            _is_directed(is_directed), _num_vertices(num_vertices), _num_edges(num_edges) {}

    // AIM: Вернуть кол-во вершин в Графе
    size_t vertices() const { return _num_vertices; }

    // AIM: Вернуть кол-во ребер в Графе
    size_t edges() const { return _num_edges; }

    // AIM: Вернеть является ли Граф Ориентированным
    bool directed() const { return _is_directed; }

    // AIM: Вставить ребро edge в Граф
    virtual void insert(const Edge& edge) = 0;

    // AIM: Удалить ребро edge из Графа (если таковое существует)
    virtual void remove(const Edge& edge) = 0;

    // AIM: Проверить, существует ли ребро между (v,e)
    virtual bool has_edge(size_t v, size_t e) const = 0;

    // AIM: Вернуть вес существующего ребра между (v,e)
    // WAR: Вернуть DBL_MAX, если ребра не существует
    virtual double edge_weight(size_t v, size_t e) const = 0;

    // AIM: Вывести граф в STDOUT
    virtual void show() const = 0;

    // AIM: Вернуть строку содержимого матрицы
    virtual std::string to_string() const = 0;

    // STANDARD DESTRUCTOR:
    virtual ~Graph() {}
};
//---------------------------------------------------------------------------------------------------------------------------
// AIM: Вставить в Граф список Ребер
template <class G>
void insertEdges( G& g, std::initializer_list<Edge>&& es) {
    for( auto&& e : es ) {
        g.insert(e);
    }
}

//===========================================================================================================================
//===========================================================================================================================
// Граф на Матрице смежности
class DenseGraph : public Graph {
private:
    // Тип элементов матрицы смежности
    struct AdjElem {
        bool exists = false;                                 // существование ребра
        double weight = std::numeric_limits<double>::max();  // его вес (DBL_MAX при его отсутствии)

        // AIM: По Ребру edge строит элемент матрицы в клетке (v, e)
        void operator=(const Edge& edge) { exists = true; weight = edge.weight; }
        // AIM: Обнулить данный элемент
        void reset() { exists = false; weight = std::numeric_limits<double>::max(); }
    };

    // Тип матрицы которая хранит Ребра
    using AdjMatrix = plain_matrix<AdjElem>;
    // Матрица Ребер Графа
    AdjMatrix _adj;

public:
    // CONSTRUCTOR: Создание оболочки для матрицы смежностей графа (is_directed-ориентиров) с v вершинами
    DenseGraph(size_t v = 0, bool is_directed = false) : _adj(v, v) {
        _is_directed = is_directed;
        _num_vertices = v;
        _num_edges = 0;
    }

    // AIM: Вставить ребро edge в Граф
    void insert(const Edge& edge) {
        size_t v(edge.v1), e(edge.v2);
        if (!_adj(v, e).exists) {
            _num_edges++;
            _adj(v, e).exists = true;
            _adj(v, e) = edge;
            if (!directed()) _adj(e, v) = edge;
        }
    }

    // AIM: Удалить ребро edge из Графа (если таковое существует по вершинам)
    void remove(const Edge& edge) {
        size_t v(edge.v1), e(edge.v2);
        if (_adj(v, e).exists) {
            _num_edges--;
            _adj(v, e).reset();
            if (!directed()) _adj(e, v).reset();
        }
    }

    // AIM: Проверить, существует ли ребро между (v,e)
    bool has_edge(size_t v, size_t e) const { return _adj(v, e).exists; }

    // AIM: Вернуть вес существующего ребра между (v,e)
    // WAR: Вернуть DBL_MAX, если ребра не существует
    double edge_weight(size_t v, size_t e) const { return _adj(v, e).weight; }

    // AIM: Вывести граф в STDOUT
    void show() const {
        for (size_t i = 0; i < _num_vertices; i++) {
            for (size_t j = 0; j < _num_vertices; j++) {
                std::cout << std::setw(3) << _adj(i, j).weight << ", ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }

    // AIM: Вернуть строку содержимого графа
    std::string to_string() const {
        std::stringstream output;

        for (size_t i = 0; i < _adj.h(); i++) {
            for (size_t j = 0; j < _adj.w(); j++) {
                output << _adj(i, j).weight << " ";
            }
            output << " ";
        }

        return output.str();
    }

    // STANDARD DESTRUCTOR:
    ~DenseGraph() {}
};

//===========================================================================================================================
// Граф на списке смежности вершин.
class SparseGraph : public Graph {
private:
    // Тип элементов списка смежности
    struct ListElem {
        size_t v2 = 0;                                      // вторая вершина ребра
        double weight = std::numeric_limits<double>::max();  // его вес (DBL_MAX при его отсутствии)

        // AIM: По данным строит элемент списка (v, weight)
        ListElem(const size_t v = 0, const double weight = std::numeric_limits<double>::max())
                : v2(v), weight(weight) {}
        // AIM: Сравнить по полю v2 данный элемент с ListElem
        bool operator== (const ListElem& list_elem) const {
            return list_elem.v2 == v2;
        }
    };

    using AdjList  = std::forward_list<ListElem>;
    using AdjLists = std::vector<AdjList>;
    AdjLists _adj;

public:
    SparseGraph(size_t v = 0, bool is_directed = false) : _adj(v) {
        _is_directed = is_directed;
        _num_vertices = v;
        _num_edges = 0;
    }

    // AIM: Расширить граф для содержания еще more_vertices вершин
    void expand(size_t more_vertices) {
        _adj.resize(_adj.size() + more_vertices);
    }

    // AIM: Вставить ребро edge в Граф
    void insert(const Edge& edge) {
        size_t v(edge.v1), e(edge.v2);

        if (!has_edge(v,e)) {
            if (v > _adj.size() || e > _adj.size())
                expand(std::max(v, e) - _adj.size() + 1);

            _adj[v].emplace_front(e, edge.weight);
            _num_edges++;

            if (v != e && !directed()) {
                _adj[e].emplace_front(v, edge.weight);
            }
        }
    }

    // AIM: Удалить ребро edge из Графа (если таковое существует по вершинам)
    void remove(const Edge& edge) {
        size_t v(edge.v1), e(edge.v2);
        AdjList& v_edges = _adj[v];
        AdjList& e_edges = _adj[e];
        auto adj_elem = find(v_edges.begin(), v_edges.end(), ListElem(e));

        if(adj_elem != v_edges.end()) {
            v_edges.remove(ListElem(e));
            _num_edges--;

            if(!directed()) {
                e_edges.remove(ListElem(v));
            }
        }
    }

    // AIM: Проверить, существует ли ребро между (v,e)
    bool has_edge(size_t v, size_t e) const {
        const AdjList& v_edges = _adj[v];
        auto adj_elem = find(v_edges.begin(), v_edges.end(), ListElem(e));
        return adj_elem != v_edges.end();
    }

    // AIM: Вернуть вес существующего ребра между (v,e)
    // WAR: Вернуть DBL_MAX, если ребра не существует
    double edge_weight(size_t v, size_t e) const {
        if (has_edge(v, e)) {
            const AdjList& v_edges = _adj[v];
            auto adj_elem = find(v_edges.begin(), v_edges.end(), ListElem(e));
            return adj_elem->weight;
        } else {
            return std::numeric_limits<double>::max();
        }
    }

    // AIM: Вывести граф в STDOUT
    void show() const {
        std::cout << "v: " << vertices() << std::endl;
        std::cout << "e: " << edges() << std::endl;
        for(size_t v = 0; v < vertices(); v++ ) {
            std::cout << std::setw(2) << v << ":";
            for(auto list_elem : _adj[v] ) {
                std::cout << std::setw(2) << list_elem.v2 << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }

    // AIM: Вернуть строку содержимого графа
    std::string to_string() const {
        std::stringstream output;

        for(size_t v = 0; v < vertices(); v++ ) {
            output << v << ": ";
            for(auto list_elem : _adj[v] ) {
                output << list_elem.v2 << " ";
            }
            output << " ";
        }

        return output.str();
    }

    // STANDARD DESTRUCTOR:
    ~SparseGraph() {}
};
//===========================================================================================================================

//===========================================================================================================================
// Класс для рассчета максимального потока (алгоритмом предпотоков)
class PreflowPushWrapper {
private:
    size_t _start_vertice;      // исток для искомого потока
    size_t _target_vertice;     // сток для искомого потока
    DenseGraph _capacity_graph; // граф пропускных способностей ребер

    plain_matrix<double> _flow_through_edges;
    std::vector<double>  _excess_in_vertices;
    std::vector<size_t>  _height_of_vertices;

    // AIM: Вернуть остаточную пропускную способность ребра
    double _capacity_left(size_t first_vertice, size_t second_vertice) const;

    // AIM: Слить избыток из первой вершины во вторую
    void _push(size_t from_vertice, size_t into_vertice);

    // AIM: Поднять данную вершину
    void _relabel(size_t vertice_to_lift);

    // AIM: Вернуть следующее номер следующей инцидентной вершины (такие номера стоят в цикле)
    size_t _next_vertice(size_t origin_vertice, size_t start_vertice) const;

    // AIM: Вополнять операции push/relabel до полного истощения избытка
    void _discharge(size_t vertice_to_empty);

public:
    // STANDARD CONSTRUCTOR:
    PreflowPushWrapper(size_t start_vertice, size_t target_vertice, DenseGraph& capacity_graph);

    // AIM: Вернуть, можно ли разрешить величину максимального потока
    bool is_valid_for_max_flow() const { return _capacity_graph.vertices() >= 2; }

    // AIM: Вычислить и вернуть величину максимального потока из start -> target
    double max_flow();
};

//===========================================================================================================================
// AIM: Вычислить и вернуть вектор максимальных потоков для введенных в filename тестов
std::vector<double> calc_max_flows(const std::string& filename);

// AIM: Вернуть считанные данные для конкретно текущего теста
PreflowPushWrapper read_input_one_test(std::ifstream& fin);

// AIM: Вывести вектор максимальных потоков в filename
void output_max_flows(const std::string& filename, const std::vector<double>& max_flow_vec);

//===========================================================================================================================


//===========================================================================================================================
// AIM: Вернуть примерное сравнение на ноль
bool double_is_greater_than_epsilon_zero(double number) {
    const static double EPSILON = 0.0001;
    return number > EPSILON;
}

//===========================================================================================================================
// AIM: Вернуть остаточную пропускную способность ребра
double PreflowPushWrapper::_capacity_left(size_t first_vertice, size_t second_vertice) const {
    double capacity_left = 0;

    if (_capacity_graph.has_edge(first_vertice, second_vertice)) {
        capacity_left += _capacity_graph.edge_weight(first_vertice, second_vertice);
    }

    capacity_left -= _flow_through_edges(first_vertice, second_vertice);

    return capacity_left;
}

// AIM: Слить избыток из первой вершины во вторую
void PreflowPushWrapper::_push(size_t from_vertice, size_t into_vertice) {
    double moveable_excess = std::min(_excess_in_vertices[from_vertice],
                                      _capacity_left(from_vertice, into_vertice));

    _excess_in_vertices[from_vertice] -= moveable_excess;
    _excess_in_vertices[into_vertice] += moveable_excess;

    _flow_through_edges(from_vertice, into_vertice) += moveable_excess;
    _flow_through_edges(into_vertice, from_vertice) -= moveable_excess;
}

// AIM: Поднять данную вершину
void PreflowPushWrapper::_relabel(size_t vertice_to_lift) {
    size_t min_adjacent_height = _capacity_graph.vertices();

    for (size_t other_vertice = 0; other_vertice < _capacity_graph.vertices(); ++other_vertice) {
        if (double_is_greater_than_epsilon_zero(_capacity_left(vertice_to_lift, other_vertice))) {
            if (min_adjacent_height > _height_of_vertices[other_vertice]) {
                min_adjacent_height = _height_of_vertices[other_vertice];
            }
        }
    }

    _height_of_vertices[vertice_to_lift] = min_adjacent_height + 1;
}

//----------------------------------------------------------------------------------------------------------
// AIM: Вернуть следующее номер следующей инцидентной вершины (такие номера стоят в цикле)
size_t PreflowPushWrapper::_next_vertice(size_t origin_vertice, size_t start_vertice) const {
    size_t next_vertice;
    if (start_vertice < _capacity_graph.vertices()) {
        next_vertice = start_vertice + 1;
    } else {
        next_vertice = 0;
    }

    for (next_vertice;
         next_vertice < _capacity_graph.vertices() &&
         !double_is_greater_than_epsilon_zero(_capacity_left(origin_vertice, next_vertice));
         ++next_vertice) {}

    return next_vertice;
}

// AIM: Вополнять операции push/relabel до полного истощения избытка
void PreflowPushWrapper::_discharge(size_t vertice_to_empty) {
    size_t other_vertice = 0;

    while (_excess_in_vertices[vertice_to_empty] > 0) {
        if (other_vertice >= _capacity_graph.vertices()) {
            size_t height_before_relabel = _height_of_vertices[vertice_to_empty];
            _relabel(vertice_to_empty);
            if (_height_of_vertices[vertice_to_empty] == height_before_relabel) {
                break;
            }

            other_vertice = _next_vertice(vertice_to_empty, other_vertice);
        } else {
            if (double_is_greater_than_epsilon_zero(_capacity_left(vertice_to_empty, other_vertice)) &&
                _height_of_vertices[vertice_to_empty] > _height_of_vertices[other_vertice]) {
                _push(vertice_to_empty, other_vertice);
            } else {
                other_vertice = _next_vertice(vertice_to_empty, other_vertice);
            }
        }
    }
}

//----------------------------------------------------------------------------------------------------------
// STANDARD CONSTRUCTOR:
PreflowPushWrapper::PreflowPushWrapper(size_t start_vertice, size_t target_vertice,
                                       DenseGraph& capacity_graph) {
    _start_vertice  = start_vertice;
    _target_vertice = target_vertice;
    _capacity_graph = capacity_graph;

    size_t num_vertices = _capacity_graph.vertices();

    _flow_through_edges = plain_matrix<double>(num_vertices, num_vertices);
    _excess_in_vertices = std::vector<double>(num_vertices, 0);
    _height_of_vertices = std::vector<size_t>(num_vertices, 0);

    for (size_t i = 0; i < num_vertices; ++i) {
        for (size_t j = 0; j < num_vertices; ++j) {
            _flow_through_edges(i, j) = 0;
        }
    }
}

//----------------------------------------------------------------------------------------------------------
// AIM: Вычислить и вернуть величину максимального потока из start -> target
double PreflowPushWrapper::max_flow() {

    std::vector<size_t> discharge_sequence;
    for (size_t curr_vertice = 0; curr_vertice < _capacity_graph.vertices(); ++curr_vertice) {
        if (curr_vertice != _start_vertice && curr_vertice != _target_vertice) {
            discharge_sequence.push_back(curr_vertice);
        }
    }

    _height_of_vertices[_start_vertice] = _capacity_graph.vertices();
    for (size_t curr_vertice = 0; curr_vertice < _capacity_graph.vertices(); ++curr_vertice) {
        if (_capacity_graph.has_edge(_start_vertice, curr_vertice)) {
            double max_curr_excess = _capacity_graph.edge_weight(_start_vertice, curr_vertice);
            _excess_in_vertices[curr_vertice] = max_curr_excess;
            _flow_through_edges(_start_vertice, curr_vertice) = max_curr_excess;
            _flow_through_edges(curr_vertice, _start_vertice) = -max_curr_excess;
        }
    }

    size_t num_consecutive_undischarged_vertices = 0;
    size_t curr_discharge_index = 0;
    while (num_consecutive_undischarged_vertices < 10 * discharge_sequence.size()) {
        size_t curr_vertice = discharge_sequence[curr_discharge_index];

        size_t height_before_discharge = _height_of_vertices[curr_vertice];
        _discharge(curr_vertice);
        if (_height_of_vertices[curr_vertice] > height_before_discharge) {
            num_consecutive_undischarged_vertices = 0;
        } else {
            num_consecutive_undischarged_vertices++;
        }

        curr_discharge_index = (curr_discharge_index + 1) % discharge_sequence.size();
    }

    double max_flow = 0;
    for (size_t curr_vertice = 0; curr_vertice < _capacity_graph.vertices(); ++curr_vertice) {
        max_flow += _flow_through_edges(curr_vertice, _target_vertice);
    }

    return max_flow;
}

//===========================================================================================================================
// AIM: Вычислить и вернуть вектор максимальных потоков для введенных в filename тестов
std::vector<double> calc_max_flows(const std::string& filename) {
    std::ifstream fin(filename);
    std::vector<double> max_flow_vec;

    PreflowPushWrapper curr_preflow_push = read_input_one_test(fin);
    while (curr_preflow_push.is_valid_for_max_flow()) {
        max_flow_vec.push_back(curr_preflow_push.max_flow());
        curr_preflow_push = read_input_one_test(fin);
    }

    fin.close();
    return max_flow_vec;
}

// AIM: Вернуть считанные данные для конкретно текущего теста
PreflowPushWrapper read_input_one_test(std::ifstream& fin) {
    size_t num_vertices = 0;
    fin >> num_vertices;
    DenseGraph capacity_graph(num_vertices, false);

    if (num_vertices == 0) {
        PreflowPushWrapper empty_preflow_push_wrapper(0, 0, capacity_graph);
        return empty_preflow_push_wrapper;
    }

    size_t start_vertice = 0, target_vertice = 0, num_edges = 0;
    fin >> start_vertice >> target_vertice >> num_edges;
    --start_vertice;
    --target_vertice;
    for (size_t i = 0; i < num_edges; ++i) {
        size_t first_vertice, second_vertice;
        double edge_capacity;

        fin >> first_vertice >> second_vertice >> edge_capacity;
        --first_vertice;
        --second_vertice;
        if (capacity_graph.has_edge(first_vertice, second_vertice)) {
            edge_capacity += capacity_graph.edge_weight(first_vertice, second_vertice);
            capacity_graph.remove(Edge(first_vertice, second_vertice));
        }
        capacity_graph.insert(Edge(first_vertice, second_vertice, edge_capacity));
    }

    return PreflowPushWrapper(start_vertice, target_vertice, capacity_graph);
}

// AIM: Вывести вектор максимальных потоков в filename
void output_max_flows(const std::string& filename, const std::vector<double>& max_flow_vec) {
    std::ofstream fout(filename);

    for (const double& max_flow_val : max_flow_vec) {
        fout << (int) max_flow_val << std::endl;
    }

    fout.close();
}

//===========================================================================================================================

// AIM: Ввод и вывод тестов
int main() {
    std::vector<double> max_flow_vec = calc_max_flows("input.txt");
    output_max_flows("output.txt", max_flow_vec);
    return 0;
}