#ifndef MIPT_3SEM_ALG_E_UMF_LIB_H
#define MIPT_3SEM_ALG_E_UMF_LIB_H

#include <graph/graph.h>
#include <fstream>
#include <queue>
#include <memory>

//===========================================================================================================================
// Класс для рассчета максимального потока (алгоритмом предпотоков)
class PreflowPushWrapper {
protected:
    size_t _start_vertice;      // исток для искомого потока
    size_t _target_vertice;     // сток для искомого потока
    std::shared_ptr<Graph> _capacity_graph_ptr; // граф пропускных способностей ребер

    plain_matrix<double> _flow_through_edges;  // текущий поток через ребра
    std::vector<double>  _excess_in_vertices;  // избыток в вершинах графа
    std::vector<size_t>  _height_of_vertices;  // высота вершин графа

    double _max_flow = -1;  // Закешированное значение максимального потока


    // AIM: Вернуть остаточную пропускную способность ребра
    double _capacity_left(size_t first_vertice, size_t second_vertice) const;

    // AIM: Вернуть есть ли остаточная пропускная способность у ребра
    bool _has_capacity_left(size_t first_vertice, size_t second_vertice) const;


    // AIM: Слить избыток из первой вершины во вторую
    void _push(size_t from_vertice, size_t into_vertice);

    // AIM: Поднять данную вершину
    void _relabel(size_t vertice_to_lift);


    // AIM: Вернуть следующее номер следующей инцидентной вершины (такие номера стоят в цикле)
    size_t _next_vertice(size_t origin_vertice, size_t start_vertice) const;

    // AIM: Вополнять операции push/relabel до полного истощения избытка
    void _discharge(size_t vertice_to_empty);

    // AIM: Вычислить величину максимального потока из start -> target
    void _calc_max_flow();


    // AIM: Инициализировать класс (создать обьекты для preflow-push algorithm)
    void _preflow_push_wrapper_init();

public:
    // STANDARD CONSTRUCTOR:
    PreflowPushWrapper(size_t start_vertice, size_t target_vertice,
                       std::shared_ptr<Graph> capacity_graph_ptr);

    // AIM: Вернуть, можно ли разрешить величину максимального потока
    bool is_valid_for_max_flow() const { return _capacity_graph_ptr->vertices() >= 2; }

    // AIM: Вернуть величину максимального потока из start -> target
    double get_max_flow();
};

//===========================================================================================================================
// AIM: Вычислить и вернуть вектор максимальных потоков для введенных в filename тестов
std::vector<double> calc_max_flows(const std::string& filename);

// AIM: Вернуть считанные данные для конкретно текущего теста
PreflowPushWrapper read_input_one_test(std::ifstream& fin);

// AIM: Вывести вектор максимальных потоков в filename
void output_max_flows(const std::string& filename, const std::vector<double>& max_flow_vec);

//===========================================================================================================================

#endif //MIPT_3SEM_ALG_E_UMF_LIB_H
