#include <gtest/gtest.h>
#include "E_UMF_lib.h"

const std::string TEST_DIR = "test/";
const std::string INPUT_A    = TEST_DIR + "input_a.txt";
const std::string INPUT_B    = TEST_DIR + "input_b.txt";
const std::string INPUT_C    = TEST_DIR + "input_c.txt";
const std::string OUTPUT     = TEST_DIR + "output.txt";
const std::string OUTPUT_ALT = TEST_DIR + "output_alt.txt";

struct IOTestStreams {
    std::ifstream fin_a, fin_b, fin_c;
    std::ofstream fout, fout_alt;

    IOTestStreams() : fin_a(INPUT_A),
                      fin_b(INPUT_B),
                      fin_c(INPUT_C),
                       fout(OUTPUT),
                      fout_alt(OUTPUT_ALT) {}

    ~IOTestStreams() {
        fin_a.close();
        fin_b.close();
        fin_c.close();
        fout.close();
        fout_alt.close();
    }
};

TEST(E_UMF_IO_TESTS, read_input_one_test) {
    IOTestStreams streams;
    EXPECT_TRUE(read_input_one_test(streams.fin_a).is_valid_for_max_flow());
    EXPECT_TRUE(read_input_one_test(streams.fin_a).is_valid_for_max_flow());
    EXPECT_TRUE(read_input_one_test(streams.fin_a).is_valid_for_max_flow());
    EXPECT_FALSE(read_input_one_test(streams.fin_a).is_valid_for_max_flow());

    EXPECT_TRUE(read_input_one_test(streams.fin_b).is_valid_for_max_flow());
    EXPECT_TRUE(read_input_one_test(streams.fin_b).is_valid_for_max_flow());
    EXPECT_TRUE(read_input_one_test(streams.fin_b).is_valid_for_max_flow());
    EXPECT_FALSE(read_input_one_test(streams.fin_b).is_valid_for_max_flow());

    EXPECT_TRUE(read_input_one_test(streams.fin_c).is_valid_for_max_flow());
    EXPECT_TRUE(read_input_one_test(streams.fin_c).is_valid_for_max_flow());
    EXPECT_FALSE(read_input_one_test(streams.fin_c).is_valid_for_max_flow());
}

TEST(E_UMF_IO_TESTS, calc_max_flows) {
    std::vector<double> flow_a = calc_max_flows(INPUT_A);
    std::vector<double> flow_b = calc_max_flows(INPUT_B);
    std::vector<double> flow_c = calc_max_flows(INPUT_C);

    EXPECT_EQ(3, flow_a.size());
    EXPECT_EQ(3, flow_b.size());
    EXPECT_EQ(2, flow_c.size());
}

TEST(E_UMF_IO_TESTS, output_max_flows) {
    std::vector<double> flow_b = calc_max_flows(INPUT_B);
    output_max_flows(OUTPUT, flow_b);

    std::vector<double> random {0.0, 9001, 1779, 1886.991, 2016, 1337, -01237912.1203919};
    output_max_flows(OUTPUT_ALT, random);

    std::ifstream fin(OUTPUT_ALT);
    double tmp;
    fin >> tmp >> tmp >> tmp >> tmp;
    EXPECT_EQ(static_cast<int>(1886.991), tmp);
    fin >> tmp >> tmp;
    EXPECT_EQ(1337, tmp);
    fin >> tmp;
    ASSERT_NEAR(-01237912.1203919, tmp, 100);
    fin.close();
}

TEST(E_UMF_TESTS, max_flow) {
    std::vector<double> mf_a = calc_max_flows(INPUT_A);
    EXPECT_EQ(3,    mf_a.size());
    EXPECT_EQ(25,   mf_a[0]);
    EXPECT_EQ(2000, mf_a[1]);
    EXPECT_EQ(24,   mf_a[2]);

    std::vector<double> mf_b = calc_max_flows(INPUT_B);
    EXPECT_EQ(3,  mf_b.size());
    EXPECT_EQ(25, mf_b[0]);
    EXPECT_EQ(0,  mf_b[1]);
    EXPECT_EQ(16, mf_b[2]);

    std::vector<double> mf_c = calc_max_flows(INPUT_C);
    EXPECT_EQ(2,  mf_c.size());
    EXPECT_EQ(0,  mf_c[0]);
    EXPECT_EQ(19, mf_c[1]);
}