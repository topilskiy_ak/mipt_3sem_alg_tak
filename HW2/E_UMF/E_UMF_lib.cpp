#include "E_UMF_lib.h"

//===========================================================================================================================
// AIM: Вернуть остаточную пропускную способность ребра
double PreflowPushWrapper::_capacity_left(size_t first_vertice, size_t second_vertice) const {
    double capacity_left = 0;

    if (_capacity_graph_ptr->has_edge(first_vertice, second_vertice)) {
        capacity_left += _capacity_graph_ptr->edge_weight(first_vertice, second_vertice);
    }

    capacity_left -= _flow_through_edges(first_vertice, second_vertice);

    return capacity_left;
}

// AIM: Вернуть есть ли остаточная пропускная способность у ребра
bool PreflowPushWrapper::_has_capacity_left(size_t first_vertice, size_t second_vertice) const {
    const static double EPSILON = 0.0001;
    return _capacity_left(first_vertice, second_vertice) > EPSILON;
}

// AIM: Слить избыток из первой вершины во вторую
void PreflowPushWrapper::_push(size_t from_vertice, size_t into_vertice) {
    double moveable_excess = std::min(_excess_in_vertices[from_vertice],
                                      _capacity_left(from_vertice, into_vertice));

    _excess_in_vertices[from_vertice] -= moveable_excess;
    _excess_in_vertices[into_vertice] += moveable_excess;

    _flow_through_edges(from_vertice, into_vertice) += moveable_excess;
    _flow_through_edges(into_vertice, from_vertice) -= moveable_excess;
}

// AIM: Поднять данную вершину
void PreflowPushWrapper::_relabel(size_t vertice_to_lift) {
    size_t min_adjacent_height = _capacity_graph_ptr->vertices();

    for (size_t other_vertice = 0; other_vertice < _capacity_graph_ptr->vertices(); ++other_vertice) {
        if (_has_capacity_left(vertice_to_lift, other_vertice)) {
            if (min_adjacent_height > _height_of_vertices[other_vertice]) {
                min_adjacent_height = _height_of_vertices[other_vertice];
            }
        }
    }

    _height_of_vertices[vertice_to_lift] = min_adjacent_height + 1;
}

//----------------------------------------------------------------------------------------------------------
// AIM: Вернуть следующее номер следующей инцидентной вершины (такие номера стоят в цикле)
size_t PreflowPushWrapper::_next_vertice(size_t origin_vertice, size_t start_vertice) const {
    size_t next_vertice;
    if (start_vertice < _capacity_graph_ptr->vertices()) {
        next_vertice = start_vertice + 1;
    } else {
        next_vertice = 0;
    }

    for (next_vertice;
         next_vertice < _capacity_graph_ptr->vertices() &&
         !_has_capacity_left(origin_vertice, next_vertice);
         ++next_vertice) {}

    return next_vertice;
}

// AIM: Вополнять операции push/relabel до полного истощения избытка
void PreflowPushWrapper::_discharge(size_t vertice_to_empty) {
    size_t other_vertice = 0;

    while (_excess_in_vertices[vertice_to_empty] > 0) {
        if (other_vertice >= _capacity_graph_ptr->vertices()) {
            size_t height_before_relabel = _height_of_vertices[vertice_to_empty];
            _relabel(vertice_to_empty);
            if (_height_of_vertices[vertice_to_empty] == height_before_relabel) {
                break;
            }
            other_vertice = _next_vertice(vertice_to_empty, other_vertice);
        } else {
            if (_has_capacity_left(vertice_to_empty, other_vertice) &&
                _height_of_vertices[vertice_to_empty] > _height_of_vertices[other_vertice]) {
                    _push(vertice_to_empty, other_vertice);
            } else {
                other_vertice = _next_vertice(vertice_to_empty, other_vertice);
            }
        }
    }
}

//----------------------------------------------------------------------------------------------------------
// AIM: Вычислить величину максимального потока из start -> target
void PreflowPushWrapper::_calc_max_flow() {
    if (_max_flow != -1) {
        return;
    }

    std::vector<size_t> discharge_sequence;
    for (size_t curr_vertice = 0; curr_vertice < _capacity_graph_ptr->vertices(); ++curr_vertice) {
        if (curr_vertice != _start_vertice && curr_vertice != _target_vertice) {
            discharge_sequence.push_back(curr_vertice);
        }
    }

    _height_of_vertices[_start_vertice] = _capacity_graph_ptr->vertices();
    for (size_t curr_vertice = 0; curr_vertice < _capacity_graph_ptr->vertices(); ++curr_vertice) {
        if (_capacity_graph_ptr->has_edge(_start_vertice, curr_vertice)) {
            double max_curr_excess = _capacity_graph_ptr->edge_weight(_start_vertice, curr_vertice);
            _excess_in_vertices[curr_vertice] = max_curr_excess;
            _flow_through_edges(_start_vertice, curr_vertice) = max_curr_excess;
            _flow_through_edges(curr_vertice, _start_vertice) = -max_curr_excess;
        }
    }

    size_t num_consecutive_undischarged_vertices = 0;
    size_t curr_discharge_index = 0;
    while (num_consecutive_undischarged_vertices < 2 * discharge_sequence.size()) {
        size_t curr_vertice = discharge_sequence[curr_discharge_index];

        size_t height_before_discharge = _height_of_vertices[curr_vertice];
        _discharge(curr_vertice);
        if (_height_of_vertices[curr_vertice] > height_before_discharge) {
            num_consecutive_undischarged_vertices = 0;
        } else {
            num_consecutive_undischarged_vertices++;
        }

        curr_discharge_index = (curr_discharge_index + 1) % discharge_sequence.size();
    }

    _max_flow = 0;
    for (size_t curr_vertice = 0; curr_vertice < _capacity_graph_ptr->vertices(); ++curr_vertice) {
        _max_flow += _flow_through_edges(curr_vertice, _target_vertice);
    }
}

//----------------------------------------------------------------------------------------------------------
// AIM: Инициализировать класс (создать обьекты для preflow-push algorithm)
void PreflowPushWrapper::_preflow_push_wrapper_init() {
    size_t num_vertices = _capacity_graph_ptr->vertices();

    _flow_through_edges = plain_matrix<double>(num_vertices, num_vertices);
    _excess_in_vertices = std::vector<double>(num_vertices, 0);
    _height_of_vertices = std::vector<size_t>(num_vertices, 0);

    for (size_t i = 0; i < num_vertices; ++i) {
        for (size_t j = 0; j < num_vertices; ++j) {
            _flow_through_edges(i, j) = 0;
        }
    }
}

// STANDARD CONSTRUCTOR:
PreflowPushWrapper::PreflowPushWrapper(size_t start_vertice, size_t target_vertice,
                                       std::shared_ptr<Graph> capacity_graph_ptr) {

    _capacity_graph_ptr = capacity_graph_ptr;
    _start_vertice  = start_vertice;
    _target_vertice = target_vertice;

    _preflow_push_wrapper_init();
}

// AIM: Вернуть величину максимального потока из start -> target
double PreflowPushWrapper::get_max_flow() {
    if(is_valid_for_max_flow() && _max_flow == -1) {
        _calc_max_flow();
    }
    return _max_flow;
}

//===========================================================================================================================
// AIM: Вычислить и вернуть вектор максимальных потоков для введенных в filename тестов
std::vector<double> calc_max_flows(const std::string& filename) {
    std::ifstream fin(filename);
    std::vector<double> max_flow_vec;

    PreflowPushWrapper curr_preflow_push = read_input_one_test(fin);
    while (curr_preflow_push.is_valid_for_max_flow()) {
        max_flow_vec.push_back(curr_preflow_push.get_max_flow());
        curr_preflow_push = read_input_one_test(fin);
    }

    fin.close();
    return max_flow_vec;
}

// AIM: Вернуть считанные данные для конкретно текущего теста
PreflowPushWrapper read_input_one_test(std::ifstream& fin) {
    size_t num_vertices = 0;
    fin >> num_vertices;
    DenseGraph capacity_graph(num_vertices, false);

    if (num_vertices == 0) {
        PreflowPushWrapper empty_preflow_push_wrapper(0, 0,
                                                      std::make_shared<DenseGraph>(capacity_graph));
        return empty_preflow_push_wrapper;
    }

    size_t start_vertice = 0, target_vertice = 0, num_edges = 0;
    fin >> start_vertice >> target_vertice >> num_edges;
    --start_vertice;
    --target_vertice;
    for (size_t i = 0; i < num_edges; ++i) {
        size_t first_vertice, second_vertice;
        double edge_capacity;

        fin >> first_vertice >> second_vertice >> edge_capacity;
        --first_vertice;
        --second_vertice;
        if (capacity_graph.has_edge(first_vertice, second_vertice)) {
            double& curr_edge_capacity =
                    capacity_graph.edge_weight_reference(first_vertice, second_vertice);

            curr_edge_capacity += edge_capacity;
        } else {
            capacity_graph.insert(first_vertice, second_vertice, edge_capacity);
        }
    }

    return PreflowPushWrapper(start_vertice, target_vertice,
                              std::make_shared<DenseGraph>(capacity_graph));
}

// AIM: Вывести вектор максимальных потоков в filename
void output_max_flows(const std::string& filename, const std::vector<double>& max_flow_vec) {
    std::ofstream fout(filename);

    for (const double& max_flow_val : max_flow_vec) {
        fout << static_cast<int>(max_flow_val) << std::endl;
    }

    fout.close();
}

//===========================================================================================================================
