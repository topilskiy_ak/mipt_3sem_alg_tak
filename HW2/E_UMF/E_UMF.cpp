#include "E_UMF_lib.h"

// AIM: Ввод и вывод тестов
int main() {
    std::vector<double> max_flow_vec = calc_max_flows("input.txt");
    output_max_flows("output.txt", max_flow_vec);
    return 0;
}