#ifndef MIPT_3SEM_ALG_D_MST_LIB_H
#define MIPT_3SEM_ALG_D_MST_LIB_H

#include <graph/graph.h> // class Edge

//===========================================================================================================================
// AIM: Считать данные из файла kruskal.in
std::vector<Edge> read_d_mst_in(size_t& num_vertices);

//===========================================================================================================================
// A class wrapping edges and vertices into a class, that is ready to perform the Kruskal Algorithm
// WARNING: Vertices must be numbered [1...n] - there shouldn't be any 0-vertices
class KruskalWrapper {
private:
    // Info tied with each vertice in the optimised Kruskal Algorithm
    struct VerticeInfo {
        size_t parent_vertice;
        size_t tree_depth;

        // STANDARD CONSTRUCTOR
        VerticeInfo(const size_t& parent_vertice_init = 0, const size_t& tree_depth_init = 1) :
                parent_vertice(parent_vertice_init), tree_depth(tree_depth_init) {}
    };

    std::vector<Edge> edges;
    std::vector<VerticeInfo> vertices_info;
    double mst_weight = std::numeric_limits<double>::max();

    // AIM: Perform the Union of Roots in the Kruskal Algorithm
    void unify_roots(const size_t& first_root, const size_t& second_root);

    // AIM: Return the root of the set vertice is in
    size_t find_root_of_(const size_t& vertice) const;

public:
    // CONSTUCTOR: initialize edges and vertices_info for Kruskal Algorithm use
    KruskalWrapper(const std::vector<Edge>& edges_init, const size_t& num_vertices_init);

    // Perform the Kruskal Algorithm and return the weight of the MST
    double get_mst_weight();

    // Perform the Kruskal Algorithm and return the SparseGraph Shell of the calculated MST
    SparseGraph get_mst();
};

//===========================================================================================================================

#endif //MIPT_3SEM_ALG_D_MST_LIB_H
