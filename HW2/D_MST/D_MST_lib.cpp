#include "D_MST_lib.h"
#include <fstream>

//===========================================================================================================================
// AIM: Считать данные из файла kruskal.in
std::vector<Edge> read_d_mst_in(size_t& num_vertices) {
    size_t num_edges = 0;
    std::ifstream fin("kruskal.in");

    fin >> num_vertices >> num_edges;
    std::vector<Edge> graph_edges;

    graph_edges.reserve(num_edges);

    size_t v1 = 0, v2 = 0, weight = 0;
    for(; num_edges > 0; --num_edges) {
        fin >> v1 >> v2 >> weight;
        graph_edges.push_back(Edge(v1, v2, weight));
    }

    return graph_edges;
}

//===========================================================================================================================
// CONSTUCTOR: initialize edges and vertices_info for Kruskal Algorithm use
KruskalWrapper::KruskalWrapper(const std::vector<Edge>& edges_init, const size_t& num_vertices_init) {
    edges = edges_init;
    std::sort(edges.begin(), edges.end(),
              [](const Edge& first, const Edge& second) { return first.weight < second.weight; });
    vertices_info = std::vector<VerticeInfo>(num_vertices_init + 1);
}

// AIM: Perform the Union of Roots in the Kruskal Algorithm
void KruskalWrapper::unify_roots(const size_t& first_root, const size_t& second_root) {
    size_t first_root_depth  = vertices_info[first_root].tree_depth;
    size_t second_root_depth = vertices_info[second_root].tree_depth;

    if (first_root_depth > second_root_depth) {
        vertices_info[second_root].parent_vertice = first_root;
    } else if (first_root_depth < second_root_depth) {
        vertices_info[first_root].parent_vertice = second_root;
    } else { // first_root_depth == second_root_depth
        vertices_info[second_root].parent_vertice = first_root;
        vertices_info[first_root].tree_depth++;
    }
}

// AIM: Return the root of the set vertice is in
size_t KruskalWrapper::find_root_of_(const size_t& vertice) const {
    size_t root = vertice;

    while(vertices_info[root].parent_vertice != 0) {
        root = vertices_info[root].parent_vertice;
    }

    return root;
}

//===========================================================================================================================
// Perform the Kruskal Algorithm and return the weight of the MST
double KruskalWrapper::get_mst_weight() {
    if (mst_weight != std::numeric_limits<double>::max()) {
        return mst_weight;
    }

    mst_weight = 0;

    for (size_t edges_iterator = 0; edges_iterator < edges.size(); ++edges_iterator) {
        size_t first_edge_vertice  = edges[edges_iterator].v1;
        size_t second_edge_vertice = edges[edges_iterator].v2;
        double edge_weight = edges[edges_iterator].weight;

        size_t first_root  = find_root_of_(first_edge_vertice);
        size_t second_root = find_root_of_(second_edge_vertice);

        if (first_root != second_root) {
            mst_weight += edge_weight;
            unify_roots(first_root, second_root);
        }
    }

    return mst_weight;
}


//===========================================================================================================================
// Perform the Kruskal Algorithm and return the SparseGraph Shell of the calculated MST
SparseGraph KruskalWrapper::get_mst() {
    // Perform Kruskal, if haven't already
    if (mst_weight == std::numeric_limits<double>::max()) {
        get_mst_weight();
    }

    SparseGraph mst_graph(vertices_info.size() - 1, false);

    for (size_t index = 1; index < vertices_info.size(); ++index) {
        if (vertices_info[index].parent_vertice != 0) {
            mst_graph.insert(Edge(vertices_info[index].parent_vertice - 1, index - 1));
        }
    }

    return mst_graph;
}

//===========================================================================================================================
