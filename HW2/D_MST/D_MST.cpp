#include "D_MST_lib.h"
#include <fstream>

// AIM: Вычисление веса MST
int main() {
    size_t num_vertices = 0;
    std::vector<Edge> graph_edges = read_d_mst_in(num_vertices);

    KruskalWrapper kruskal_instance(graph_edges, num_vertices);

    long long mst_weight = static_cast<long long>(kruskal_instance.get_mst_weight());

    std::ofstream fout("kruskal.out");
    fout << mst_weight << std::endl;
    fout.close();
    return 0;
}