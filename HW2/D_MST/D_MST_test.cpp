#include <gtest/gtest.h>
#include "D_MST_lib.h"

TEST(d_mst_tests, test_read_d_mst_in) {
    size_t num_vertices;
    std::vector<Edge> graph_edges = read_d_mst_in(num_vertices);
    EXPECT_EQ(graph_edges.size(), 4);
    EXPECT_EQ(graph_edges[0].v1, 1);
    EXPECT_EQ(graph_edges[1].v2, 3);
    EXPECT_EQ(graph_edges[2].weight, 5);
}

TEST(d_mst_tests, test_get_shortest_paths) {
    size_t num_vertices;
    std::vector<Edge> graph_edges = read_d_mst_in(num_vertices);
    KruskalWrapper kruskal_instance(graph_edges, num_vertices);
    EXPECT_EQ(kruskal_instance.get_mst_weight(), 7);
}