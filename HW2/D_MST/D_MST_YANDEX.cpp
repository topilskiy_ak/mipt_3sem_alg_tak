#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>

//===========================================================================================================================
// Структура Ребра Графа
struct Edge {
    static constexpr double BASE_WEIGHT = 1.0;  // Вес невзвешанного ребра

    size_t v1;       // Вершина Начала Ребра
    size_t v2;       // Вершина Конца Ребра
    double weight;   // Вес Ребра

    Edge(size_t v = 0, size_t e = 0, double w = BASE_WEIGHT) : v1(v), v2(e), weight(w) {}
};

//===========================================================================================================================
class KruskalWrapper {
private:
    struct VerticeInfo {
        size_t parent_vertice;
        size_t tree_depth;

        VerticeInfo(const size_t& parent_vertice_init = 0, const size_t& tree_depth_init = 1) :
                parent_vertice(parent_vertice_init), tree_depth(tree_depth_init) {}
    };

    std::vector<Edge> edges;
    std::vector<VerticeInfo> vertices_info;

public:
    KruskalWrapper(const std::vector<Edge>& edges_init, const size_t& num_vertices_init);

    void unify_roots(const size_t& first_root, const size_t& second_root);

    size_t find_root_of_(const size_t& vertice) const;

    double get_mst_weight();
};

//===========================================================================================================================
// AIM: Считать данные из файла kruskal.in
std::vector<Edge> read_d_mst_in(size_t& num_vertices) {
    size_t num_edges = 0;
    std::ifstream fin("kruskal.in");

    fin >> num_vertices >> num_edges;
    std::vector<Edge> graph_edges;

    size_t v1 = 0, v2 = 0, weight = 0;
    for(; num_edges > 0; --num_edges) {
        fin >> v1 >> v2 >> weight;
        graph_edges.push_back(Edge(v1, v2, weight));
    }

    return graph_edges;
}

//===========================================================================================================================
KruskalWrapper::KruskalWrapper(const std::vector<Edge>& edges_init, const size_t& num_vertices_init) {
    edges = edges_init;
    std::sort(edges.begin(), edges.end(),
              [](const Edge& first, const Edge& second) { return first.weight < second.weight; });
    vertices_info = std::vector<VerticeInfo>(num_vertices_init + 1);
}

void KruskalWrapper::unify_roots(const size_t& first_root, const size_t& second_root) {
    if (vertices_info[first_root].tree_depth > vertices_info[second_root].tree_depth) {
        vertices_info[second_root].parent_vertice = first_root;
    } else if (vertices_info[first_root].tree_depth < vertices_info[second_root].tree_depth) {
        vertices_info[first_root].parent_vertice = second_root;
    } else { // vertices_info[first_root].tree_depth == vertices_info[second_root].tree_depth
        vertices_info[second_root].parent_vertice = first_root;
        vertices_info[first_root].tree_depth++;
    }
}

size_t KruskalWrapper::find_root_of_(const size_t& vertice) const {
    size_t root = vertice;
    while(vertices_info[root].parent_vertice != 0) {
        root = vertices_info[root].parent_vertice;
    }
    return root;
}

//===========================================================================================================================
double KruskalWrapper::get_mst_weight() {
    double mst_weight = 0;

    for (size_t edges_iterator = 0; edges_iterator < edges.size(); ++edges_iterator) {
        size_t first_edge_vertice  = edges[edges_iterator].v1;
        size_t second_edge_vertice = edges[edges_iterator].v2;
        double edge_weight = edges[edges_iterator].weight;

        size_t first_root  = find_root_of_(first_edge_vertice);
        size_t second_root = find_root_of_(second_edge_vertice);

        if (first_root != second_root) {
            mst_weight += edge_weight;
            unify_roots(first_root, second_root);
        }
    }

    return mst_weight;
}

//===========================================================================================================================

// AIM: Вычисление веса MST
int main() {
    size_t num_vertices = 0;
    std::vector<Edge> graph_edges = read_d_mst_in(num_vertices);

    KruskalWrapper kruskal_instance(graph_edges, num_vertices);

    std::ofstream fout("kruskal.out");
    fout << kruskal_instance.get_mst_weight() << std::endl;
    fout.close();
    return 0;
}