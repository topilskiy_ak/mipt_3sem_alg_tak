#ifndef MIPT_3SEM_ALG_G_HEM_LIB_H
#define MIPT_3SEM_ALG_G_HEM_LIB_H

#include <E_UMF_lib.h>

//===========================================================================================================================
// Cтруктура, содержащая сравниваемые по Хеммингу строки (содержащие 0, 1, ?)
struct HemmingPair {
    std::string origin_string;   // "первая" строка - строка s
    std::string template_string; // "шаблон" - строка p
    size_t hemming_distance = 0; // расстояние Хемминга между строками

    HemmingPair(const std::string&   origin_string_init = "",
                const std::string& template_string_init = "",
                const size_t hemming_distance_init = 0) :
              origin_string(origin_string_init),
            template_string(template_string_init),
            hemming_distance(hemming_distance_init) {}
};

//===========================================================================================================================
// Класс для рассчета минимального разреза (алгоритмом предпотоков)
class MinCutPreflowPushWrapper : private PreflowPushWrapper {
private:
    // Множество вершин, вошедших в часть минимального разреза с start_vertice
    std::set<size_t> _start_vertice_cut_part;

    // AIM: Рассчитать минимальный разрез, записать его соотвествующую часть в start_vertice_cut_part
    void _calc_min_cut();

public:
    // STANDARD CONSTRUCTOR:
    MinCutPreflowPushWrapper(size_t start_vertice, size_t target_vertice,
                             std::shared_ptr<Graph> capacity_graph_ptr) :
            PreflowPushWrapper(start_vertice, target_vertice, capacity_graph_ptr) {}

    // AIM: Вернуть размер минимального разреза
    double get_min_cut_size() { return get_max_flow(); }

    // AIM: Вернуть множество вершин, вошедших в часть минимального разреза с start_vertice
    const std::set<size_t>& get_min_cut();
};

//===========================================================================================================================
// AIM: Вернуть минимальную по расстоянию Хемминга пару строк
//      которая получается из замены <? => 0/1> в строках origin_string_pair
HemmingPair calc_min_hemming_distance(const HemmingPair& origin_string_pair);

//----------------------------------------------------------------------------------------------------------
// AIM: Вернуть считанные из filename cтроки для сравнения по Хеммингу
HemmingPair read_input(const std::string& filename);

// AIM: Вывести в filename разрешенную по Хеммингу пару строк (с соотвествующим расстоянием)
void write_output(const HemmingPair& solved_string_pair, const std::string& filename);

//===========================================================================================================================

#endif //MIPT_3SEM_ALG_G_HEM_LIB_H
