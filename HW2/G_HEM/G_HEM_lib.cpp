#include "G_HEM_lib.h"

//===========================================================================================================================
// AIM: Рассчитать минимальный разрез, записать его соотвествующую часть в start_vertice_cut_part
void MinCutPreflowPushWrapper::_calc_min_cut() {
    _calc_max_flow();

    std::queue<size_t> left_capacity_BFS_queue;

    left_capacity_BFS_queue.push(_start_vertice);
    _start_vertice_cut_part.insert(_start_vertice);
    while (!left_capacity_BFS_queue.empty()) {
        size_t curr_vertice = left_capacity_BFS_queue.front();
        left_capacity_BFS_queue.pop();

        for (size_t adj_vertice = 0; adj_vertice < _capacity_graph_ptr->vertices(); ++adj_vertice) {
            if (_capacity_graph_ptr->has_edge(curr_vertice, adj_vertice) &&
                _has_capacity_left(curr_vertice, adj_vertice)) {

                bool is_new_vertice = _start_vertice_cut_part.insert(adj_vertice).second;
                if (is_new_vertice) {
                    left_capacity_BFS_queue.push(adj_vertice);
                }
            }
        }
    }
}

// AIM: Вернуть множество вершин, вошедших в часть минимального разреза с start_vertice
const std::set<size_t>& MinCutPreflowPushWrapper::get_min_cut() {
    if(is_valid_for_max_flow() && _start_vertice_cut_part.empty()) {
        _calc_min_cut();
    }
    return _start_vertice_cut_part;
}

//===========================================================================================================================
//===========================================================================================================================
// Класс для инкапсулированной работы с HemmingPair
class HemmingPairWrapper {
private:
    const std::string& _origin_string;   // "первая" строка - строка s
    const std::string& _template_string; // "шаблон" - строка p

    // Текущая вычисляемая хемминг-пара
    HemmingPair _min_hemming_pair;
    bool _min_hemming_pair_is_calculated;

    // вектор вершин-'?'
    std::vector<size_t> _vertice_questionmark_pairing;
    // начало вектора вершин-'?' шаблона
    size_t _template_questionmark_offset;

    // специальный граф под "хемминг-расстояния" '?' в строках _origin_string_pair
    DenseGraphLight _hemming_graph;

    // AIM: Занумеровать origin_string_pair '?' в векторе
    void _vertice_questionmark_pairing_init();

    // AIM: Записать в граф "хемминг-расстояния" '?'
    void _hemming_graph_init();

public:
    // STANDARD CONSTRUCTOR: INIT
    HemmingPairWrapper(const HemmingPair& origin_string_pair_init);

    // Вычислить минимальную хемминг-пару
    void calc_min_hemming_pair();

    // Вернуть хемминг-пару ребер с минимальным хемминг-расстоянием
    HemmingPair get_min_hemming_pair();
};

//===========================================================================================================================
// AIM: Занумеровать origin_string_pair '?' в векторе
void HemmingPairWrapper::_vertice_questionmark_pairing_init() {
    _vertice_questionmark_pairing = std::vector<size_t>(2, 0);
    _template_questionmark_offset = 2;

    for (size_t i = 0; i < _origin_string.size(); ++i) {
        if (_origin_string[i] == '?') {
            _vertice_questionmark_pairing.push_back(i);
        }
    }

    _template_questionmark_offset = _vertice_questionmark_pairing.size();

    for (size_t i = 0; i < _template_string.size(); ++i) {
        if (_template_string[i] == '?') {
            _vertice_questionmark_pairing.push_back(i);
        }
    }
}

// AIM: Записать в граф "хемминг-расстояния" '?'
void HemmingPairWrapper::_hemming_graph_init() {
    plain_matrix<double> hemming_graph_matrix(_vertice_questionmark_pairing.size(),
                                              _vertice_questionmark_pairing.size());

    for (size_t i = 0; i < _vertice_questionmark_pairing.size(); ++i) {
        for (size_t j = 0; j < _vertice_questionmark_pairing.size(); ++j) {
            hemming_graph_matrix(i, j) = 0;
        }
    }

    size_t origin_questionmark_index_start = 2;

    for (size_t origin_string_start = 0;
         origin_string_start + _template_string.size() <= _origin_string.size();
         ++origin_string_start) {

        size_t questionmark_origin_index = origin_questionmark_index_start;
        size_t questionmark_template_index = _template_questionmark_offset;

        for (size_t template_string_index = 0;
             template_string_index < _template_string.size();
             ++template_string_index) {

            char origin_char   =   _origin_string[origin_string_start + template_string_index];
            char template_char = _template_string[template_string_index];

            size_t start_vertice = 1;
            size_t end_vertice   = 0;

            if (origin_char == '0') {
                if (template_char == '0') {
                    continue;
                } else if (template_char == '1') {
                    _min_hemming_pair.hemming_distance++;
                } else { // template_char == '?'
                    start_vertice = 0;
                    end_vertice = questionmark_template_index;
                    ++questionmark_template_index;
                }
            }

            if (origin_char == '1') {
                if (template_char == '0') {
                    _min_hemming_pair.hemming_distance++;
                } else if (template_char == '1') {
                    continue;
                } else { // template_char == '?'
                    start_vertice = questionmark_template_index;
                    end_vertice = 1;
                    ++questionmark_template_index;
                }
            }

            if (origin_char == '?') {
                if (template_char == '0') {
                    start_vertice = 0;
                    end_vertice = questionmark_origin_index;
                } else if (template_char == '1') {
                    start_vertice = questionmark_origin_index;
                    end_vertice = 1;
                } else { // template_char == '?'
                    start_vertice = questionmark_origin_index;
                    end_vertice   = questionmark_template_index;
                    ++questionmark_template_index;
                }
                ++questionmark_origin_index;
            }

            if (start_vertice != 1 || end_vertice != 0) {
                if (start_vertice == 0 || end_vertice == 1) {
                    hemming_graph_matrix(start_vertice, end_vertice)++;
                } else { // one or more is and index for _vertice_questionmark_pairing
                    hemming_graph_matrix(start_vertice, end_vertice)++;
                    hemming_graph_matrix(end_vertice, start_vertice)++;
                }
            }
        }

        if (origin_string_start == _vertice_questionmark_pairing[origin_questionmark_index_start]) {
            ++origin_questionmark_index_start;
        }
    }

    for (size_t i = 0; i < _vertice_questionmark_pairing.size(); ++i) {
        for (size_t j = 0; j < _vertice_questionmark_pairing.size(); ++j) {
            if (hemming_graph_matrix(i, j) == 0)
                hemming_graph_matrix(i, j) = DENSE_GRAPH_LIGHT_EDGE_NON_EXISTANT;
        }
    }

    _hemming_graph = DenseGraphLight(std::move(hemming_graph_matrix), true);
}

//----------------------------------------------------------------------------------------------------------------
// STANDARD CONSTRUCTOR: INIT
HemmingPairWrapper::HemmingPairWrapper(const HemmingPair& origin_string_pair_init) :
      _origin_string(origin_string_pair_init.origin_string),
    _template_string(origin_string_pair_init.template_string),
      _min_hemming_pair(origin_string_pair_init.origin_string,
                        origin_string_pair_init.template_string) {

    _min_hemming_pair.hemming_distance = 0;
    _min_hemming_pair_is_calculated = false;
    _vertice_questionmark_pairing_init();
    _hemming_graph_init();
}

//----------------------------------------------------------------------------------------------------------------
// Вычислить минимальную хемминг-пару
void HemmingPairWrapper::calc_min_hemming_pair() {
    if (_min_hemming_pair_is_calculated) {
        return;
    }

    MinCutPreflowPushWrapper min_cut(0, 1, std::make_shared<DenseGraphLight>(_hemming_graph));

    _min_hemming_pair.hemming_distance += min_cut.get_min_cut_size();
    std::set<size_t> zero_questionmark_vertices = min_cut.get_min_cut();

    for (size_t origin_questionmark_index = 2;
         origin_questionmark_index < _template_questionmark_offset;
         ++origin_questionmark_index) {

        size_t _origin_string_index = _vertice_questionmark_pairing[origin_questionmark_index];

        if (zero_questionmark_vertices.find(origin_questionmark_index) !=
            zero_questionmark_vertices.end()) {
            _min_hemming_pair.origin_string[_origin_string_index] = '0';
        } else {
            _min_hemming_pair.origin_string[_origin_string_index] = '1';
        }
    }

    for (size_t template_questionmark_index = _template_questionmark_offset;
         template_questionmark_index < _vertice_questionmark_pairing.size();
         ++template_questionmark_index) {

        size_t template_string_index = _vertice_questionmark_pairing[template_questionmark_index];

        if (zero_questionmark_vertices.find(template_questionmark_index) !=
            zero_questionmark_vertices.end()) {
            _min_hemming_pair.template_string[template_string_index] = '0';
        } else {
            _min_hemming_pair.template_string[template_string_index] = '1';
        }
    }

    _min_hemming_pair_is_calculated = true;
}

// Вернуть хемминг-пару ребер с минимальным хемминг-расстоянием
HemmingPair HemmingPairWrapper::get_min_hemming_pair() {
    if (!_min_hemming_pair_is_calculated) {
        calc_min_hemming_pair();
    }

    return _min_hemming_pair;
}

//===========================================================================================================================
//===========================================================================================================================
// AIM: Вернуть минимальную по расстоянию Хемминга пару строк
//      которая получается из замены <? => 0/1> в строках origin_string_pair
HemmingPair calc_min_hemming_distance(const HemmingPair& origin_string_pair) {
    HemmingPairWrapper origin_string_pair_wrapper(origin_string_pair);
    return origin_string_pair_wrapper.get_min_hemming_pair();
}

// AIM: Вернуть считанные из filename cтроки для сравнения по Хеммингу
HemmingPair read_input(const std::string& filename) {
    std::ifstream fin(filename);

    HemmingPair origin_string_pair;
    fin >> origin_string_pair.origin_string;
    fin >> origin_string_pair.template_string;

    fin.close();

    return origin_string_pair;
}

// AIM: Вывести в filename разрешенную по Хеммингу пару строк (с соотвествующим расстоянием)
void write_output(const HemmingPair& solved_string_pair, const std::string& filename) {
    std::ofstream fout(filename);

    fout << solved_string_pair.hemming_distance << std::endl;
    fout << solved_string_pair.origin_string    << std::endl;
    fout << solved_string_pair.template_string  << std::endl;

    fout.close();
}

//===========================================================================================================================
