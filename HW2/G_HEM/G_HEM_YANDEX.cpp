#include <string>
#include <fstream>
#include <vector>
#include <queue>
#include <memory>

//===========================================================================================================================
// Структура Ребра Графа
struct Edge {
    static constexpr size_t BASE_CAPACITY = 0;
    static constexpr size_t BASE_FLOW = 0;

    size_t v1;       // Вершина Начала Ребра
    size_t v2;       // Вершина Конца Ребра
    size_t capacity;  // Пропускная способность Ребра
    size_t flow;     // Текущий поток через ребро

    Edge(const size_t& v = 0, const size_t& e = 0,
         const size_t& c = BASE_CAPACITY, const size_t& f = BASE_FLOW) :
            v1(v), v2(e), capacity(c), flow(f) {}
};

using VecEdge    = std::vector<Edge>;
using VecEdgePtr = std::shared_ptr<VecEdge>;
//===========================================================================================================================
class GraphCoreIterator {
private:
    std::vector<size_t>::iterator _graph_iter;
    VecEdgePtr _graph_edges_ptr;

public:
    GraphCoreIterator() : _graph_iter(std::vector<size_t>::iterator()), _graph_edges_ptr(nullptr) {}

    GraphCoreIterator(std::vector<size_t>::iterator graph_iter_init, VecEdgePtr graph_edges_ptr_init) :
            _graph_iter(graph_iter_init), _graph_edges_ptr(graph_edges_ptr_init) {}

    GraphCoreIterator& operator++() { _graph_iter++; return *this; }

    size_t graph_index() const { return *_graph_iter; }

    const Edge* operator->() const { return &(*_graph_edges_ptr)[*_graph_iter]; }

    bool operator!=(const GraphCoreIterator& iter_to_compare) const {
        return _graph_iter != iter_to_compare._graph_iter;
    }
};

//===========================================================================================================================
class GraphCore {
private:
    std::vector<std::vector<size_t>> _graph_list;
    VecEdgePtr _graph_edges_ptr;

public:
    using iterator = GraphCoreIterator;

    GraphCore(size_t num_vertices) :
            _graph_list(num_vertices + 1), _graph_edges_ptr(std::make_shared<VecEdge>(VecEdge())) {}

    void insert(size_t first_vertice, size_t second_vertice, size_t capacity) {
        _graph_list[first_vertice].emplace_back(_graph_edges_ptr->size());
        _graph_edges_ptr->emplace_back(first_vertice, second_vertice, capacity, 0);

        _graph_list[second_vertice].emplace_back(_graph_edges_ptr->size());
        _graph_edges_ptr->emplace_back(second_vertice, first_vertice, 0, 0);
    }

    iterator begin(size_t vertice) { return iterator(_graph_list[vertice].begin(), _graph_edges_ptr); }
    iterator   end(size_t vertice) { return iterator(_graph_list[vertice].end(),   _graph_edges_ptr); }

    VecEdgePtr get_graph_edges_ptr() { return _graph_edges_ptr; }

    size_t graph_list_size() const { return _graph_list.size(); }
};

//===========================================================================================================================
static constexpr size_t INF_DISTANCE = 10000;
static constexpr size_t INF_FLOW = 10000;

class FlowCalcWrapper {
private:
    GraphCore& graph;
    size_t _start_vertice, _target_vertice;
    std::vector<size_t> distance;
    std::vector<GraphCore::iterator> current_edge_iter;
    
    bool _BFS() {
        distance.assign(graph.graph_list_size(), INF_DISTANCE);
        distance[_start_vertice] = 0;
        std::queue<size_t> BFS_queue;
        
        BFS_queue.push(_start_vertice);
        while (!BFS_queue.empty()) {
            size_t first_vertice = BFS_queue.front(); 
            BFS_queue.pop();
            
            for (auto edge_iter = graph.begin(first_vertice);
                      edge_iter != graph.end(first_vertice);
                    ++edge_iter) {

                if ((distance[edge_iter->v2] > distance[first_vertice] + 1) && 
                    (edge_iter->flow < edge_iter->capacity)) {

                    distance[edge_iter->v2] = distance[first_vertice] + 1;
                    BFS_queue.push(edge_iter->v2);
                }
            }
        }

        return distance[_target_vertice] < INF_DISTANCE;
    }

    size_t _calc_subflow(size_t subflow_start_vertice, size_t max_flow) {
        if (subflow_start_vertice == _target_vertice || max_flow == 0) { 
            return max_flow; 
        }
        
        size_t subflow = 0;
        for (auto& edge_iter = current_edge_iter[subflow_start_vertice];
                   edge_iter != graph.end(subflow_start_vertice) && subflow == 0;
                 ++edge_iter) {

            if (distance[edge_iter->v2] == distance[subflow_start_vertice] + 1) {
                subflow = _calc_subflow(edge_iter->v2,
                                     std::min(max_flow, edge_iter->capacity - edge_iter->flow));
                if (subflow != 0) {
                    size_t edge_index = edge_iter.graph_index();
                    (*(graph.get_graph_edges_ptr()))[edge_index].flow     += subflow;
                    (*(graph.get_graph_edges_ptr()))[edge_index ^ 1].flow -= subflow;
                }
            }
        }

        return subflow;
    }

public:
    FlowCalcWrapper(GraphCore& graph_init, size_t start_vertice_init, size_t target_vertice_init) :
            graph(graph_init), _start_vertice(start_vertice_init), _target_vertice(target_vertice_init),
            distance(graph_init.graph_list_size()), current_edge_iter(graph_init.graph_list_size()) {}

    size_t max_flow() {
        size_t subflow = 0;
        size_t current_flow = 0;
        
        while (_BFS()) {
            
            for (size_t vertice = 0; vertice < graph.graph_list_size(); ++vertice) {
                current_edge_iter[vertice] = graph.begin(vertice);
            }

            for (subflow = _calc_subflow(_start_vertice, INF_FLOW);
                 subflow > 0;
                 current_flow += subflow, subflow = _calc_subflow(_start_vertice, INF_FLOW));
        }
        
        return current_flow;
    }
};

//===========================================================================================================================
class MinCutWrapper {
private:
    GraphCore& _graph;
    std::vector<bool> _visited_vertice;
    size_t _start_vertice, _target_vertice;

    void _DFS(size_t dfs_start_vertice) {
        _visited_vertice[dfs_start_vertice] = true;
        
        for (auto edge_iter = _graph.begin(dfs_start_vertice);
                  edge_iter != _graph.end(dfs_start_vertice);
                ++edge_iter) {

            if ((!_visited_vertice[edge_iter->v2]) &&
                (edge_iter->flow < edge_iter->capacity)) {
                
                _DFS(edge_iter->v2); }
        }
    }

public:
    MinCutWrapper(GraphCore &graph_init, size_t start_vertice_init, size_t target_vertice_init) :
            _graph(graph_init), _start_vertice(start_vertice_init), _target_vertice(target_vertice_init),
            _visited_vertice(_graph.graph_list_size(), false) {}

    std::vector<std::vector<size_t>> calc_min_cut() {
        std::vector<std::vector<size_t>> min_cut_vectors(2);
        _DFS(_start_vertice);
        
        for (size_t vertice_num = 0; vertice_num < _graph.graph_list_size(); ++vertice_num) {
            min_cut_vectors[(!_visited_vertice[vertice_num]) ? 1 : 0].push_back(vertice_num);
        }
        
        return min_cut_vectors;
    }
};

//===========================================================================================================================
// Cтруктура, содержащая сравниваемые по Хеммингу строки (содержащие 0, 1, ?)
struct HemmingPair {
    std::string origin_string;   // "первая" строка - строка s
    std::string template_string; // "шаблон" - строка p
    size_t hemming_distance = 0;  // расстояние Хемминга между строками

    HemmingPair(const std::string&   origin_string_init = "",
                const std::string& template_string_init = "",
                const size_t hemming_distance_init = 0) :
            origin_string(origin_string_init),
            template_string(template_string_init),
            hemming_distance(hemming_distance_init) {}
};

//===========================================================================================================================
class HemmingPairWrapper {
private:
    std::string& _origin_string;
    std::string& _template_string;

    GraphCore _graph;
    size_t _hemming_distance = 0;
    size_t _start_vertice;
    size_t _target_vertice;

    void _graph_init() {
        std::vector<size_t> count[2];
        count[0].resize(_origin_string.size());
        count[1].resize(_origin_string.size());

        size_t curr_zero_count, curr_one_count;
        for (size_t template_string_index = 0;
                    template_string_index < _template_string.size();
                    ++template_string_index) {

            curr_zero_count = 0;
            curr_one_count = 0;
            
            for (size_t origin_string_index = 0;
                        origin_string_index <= _origin_string.size() - _template_string.size();
                        ++origin_string_index) {

                if (_template_string[template_string_index] != '?') { 
                    count[_template_string[template_string_index] - '0']
                         [origin_string_index + template_string_index]++;
                }
                
                if (_origin_string[origin_string_index + template_string_index] == '0') { curr_zero_count++; }
                if (_origin_string[origin_string_index + template_string_index] == '1') { curr_one_count++; }
                if (_origin_string[origin_string_index + template_string_index] == '?' &&
                    _template_string[template_string_index] == '?') {

                    _graph.insert(origin_string_index + template_string_index,
                                 template_string_index + _origin_string.size(), 1);
                    _graph.insert(template_string_index + _origin_string.size(),
                                 origin_string_index + template_string_index, 1);
                }
            }
            
            if (_template_string[template_string_index] == '?') {
                _graph.insert(_start_vertice,
                              template_string_index + _origin_string.size(), curr_zero_count);
                _graph.insert(template_string_index + _origin_string.size(),
                              _target_vertice, curr_one_count);
            }
        }
        
        for (size_t origin_string_index = 0;
                    origin_string_index < _origin_string.size();
                    ++origin_string_index) {

            if (_origin_string[origin_string_index] != '?') {
                _hemming_distance += count[(_origin_string[origin_string_index] == '0') ? 1 : 0]
                                          [origin_string_index];
            } else {
                _graph.insert(_start_vertice, origin_string_index, count[0][origin_string_index]);
                _graph.insert(origin_string_index, _target_vertice, count[1][origin_string_index]);
            }
        }
    }

    void _calc_result_strings() {
        MinCutWrapper cut(_graph, _start_vertice, _target_vertice);
        std::vector<std::vector<size_t>> min_cut = cut.calc_min_cut();
        
        for (short is_zero = 0; is_zero < 2; ++is_zero) {
            for (auto vertice : min_cut[is_zero]) {
                if (vertice != _start_vertice && vertice != _target_vertice) {
                    if (vertice >= _origin_string.size() &&
                        _template_string[vertice - _origin_string.size()] == '?') {
                        _template_string[vertice - _origin_string.size()] =  '0' + (char)is_zero;
                    }
                    if (vertice <  _origin_string.size() &&
                        _origin_string[vertice] == '?') {
                        _origin_string[vertice] =  '0' + (char)is_zero;
                    }
                }
            }
        }
    }

public:
    HemmingPairWrapper(HemmingPair& origin_string_pair) :
            _origin_string(origin_string_pair.origin_string),
            _template_string(origin_string_pair.template_string),
            _graph(_origin_string.size() + _template_string.size() + 1),
            _start_vertice(_origin_string.size() + _template_string.size()),
            _target_vertice(_start_vertice + 1) {}

    HemmingPair get_min_hemming_pair() {
        _graph_init();

        FlowCalcWrapper flow_calc(_graph, _start_vertice, _target_vertice);
        _hemming_distance += flow_calc.max_flow();

        _calc_result_strings();

        return HemmingPair(_origin_string, _template_string, _hemming_distance);
    }
};

//===========================================================================================================================
// AIM: Вернуть минимальную по расстоянию Хемминга пару строк
//      которая получается из замены <? => 0/1> в строках origin_string_pair
HemmingPair calc_min_hemming_distance(HemmingPair& origin_string_pair) {
    HemmingPairWrapper origin_string_pair_wrapper(origin_string_pair);
    return origin_string_pair_wrapper.get_min_hemming_pair();
}

// AIM: Вернуть считанные из filename cтроки для сравнения по Хеммингу
HemmingPair read_input(const std::string& filename) {
    std::ifstream fin(filename);

    HemmingPair origin_string_pair;
    fin >> origin_string_pair.origin_string;
    fin >> origin_string_pair.template_string;

    fin.close();

    return origin_string_pair;
}

// AIM: Вывести в filename разрешенную по Хеммингу пару строк (с соотвествующим расстоянием)
void write_output(const HemmingPair& solved_string_pair, const std::string& filename) {
    std::ofstream fout(filename);

    fout << solved_string_pair.hemming_distance << std::endl;
    fout << solved_string_pair.origin_string    << std::endl;
    fout << solved_string_pair.template_string  << std::endl;

    fout.close();
}

//===========================================================================================================================
#ifndef GTEST_BLOCK_1337
#define GTEST_BLOCK_1337

// AIM: Ввод и вывод данных
int main() {
    HemmingPair origin_string_pair = read_input("input.txt");
    HemmingPair solved_string_pair = calc_min_hemming_distance(origin_string_pair);
    write_output(solved_string_pair, "output.txt");
    return 0;
}

#endif
//===========================================================================================================================
