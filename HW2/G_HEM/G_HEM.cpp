#include "G_HEM_lib.h"

// AIM: Ввод и вывод данных
int main() {
    HemmingPair origin_string_pair = read_input("input.txt");
    HemmingPair solved_string_pair = calc_min_hemming_distance(origin_string_pair);
    write_output(solved_string_pair, "output.txt");
    return 0;
}