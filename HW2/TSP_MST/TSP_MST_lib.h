#ifndef MIPT_3SEM_ALG_TSP_MST_LIB_H
#define MIPT_3SEM_ALG_TSP_MST_LIB_H

#include <D_MST_lib.h>
#include <math.h>
#include <random_num_gen/random_num_gen.h>

//===========================================================================================================================
// Using a pair of normally-distributed numbers as a random point in a euclidian space
using RandomPoint = NormalDistributionNumberPair;

// AIM: return the standard euclidian distance of two points in space = sqrt((x1-x2)^2 + (y1-y2)^2)
double euclidian_distance(const RandomPoint& first_point, const RandomPoint& second_point);

// AIM: Generate a vector of vector_size RandomPoint elements
std::vector<RandomPoint> generate_random_point_vector_of_size(const size_t& vector_size);

// AIM: Generate the vector of Edges for the set of RandomPoints
std::vector<Edge> calculate_graph_edges(const std::vector<RandomPoint>& random_point_graph);

//===========================================================================================================================
// AIM: Calculate an approximate answer to the Travelling Salesman Problem using the MST
double tsp_mst_approx(const std::vector<RandomPoint>& random_point_graph);

// AIM: Calculate the answer to the Travelling Salesman Problem (using brute-force)
double tsp_answer(const std::vector<RandomPoint>& random_point_graph);

//===========================================================================================================================
// Struct for passing the Statistical Deviation and Mean Square Error (in fractions of the optimal answer)
struct TestResult {
    double statistical_deviation = 0.0;
    double mean_square_error = 0.0;

    TestResult(const double& statistical_deviation_init, const double& mean_square_error_init) :
            statistical_deviation(statistical_deviation_init),
            mean_square_error(mean_square_error_init) {}
};

// AIM: Test the TSP-MST approximation of num_point (normally-spread(0,0) 2D-Euclidian) points
//      Repeat the test num_tests times and return the collected TestResult
TestResult test_tsp_mst_approx(const size_t& num_points, const size_t& num_tests);

// AIM: Repeat test_tsp_mst_approx in range [num_points_start, num_points_end]
std::vector<TestResult> test_tsp_mst_approx_range(const size_t& num_points_start,
                                                  const size_t& num_points_end,
                                                  const size_t& num_tests);
//===========================================================================================================================

#endif //MIPT_3SEM_ALG_TSP_MST_LIB_H
