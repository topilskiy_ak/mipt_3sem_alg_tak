#include "TSP_MST_lib.h"
#include <stack>

//===========================================================================================================================
// AIM: return the standard euclidian distance of two points in space = sqrt((x1-x2)^2 + (y1-y2)^2)
double euclidian_distance(const RandomPoint& first_point, const RandomPoint& second_point) {
    double delta_first  = first_point.first_num  - second_point.first_num;
    double delta_second = first_point.second_num - second_point.second_num;
    return sqrt(delta_first  * delta_first +
                delta_second * delta_second);
}

// AIM: Generate a vector of vector_size RandomPoint elements
std::vector<RandomPoint> generate_random_point_vector_of_size(const size_t& vector_size) {
    std::vector<RandomPoint> random_point_vector;

    while (vector_size > random_point_vector.size()) {
        random_point_vector.push_back(normal_distribution_number_pair_generator());
    }

    return random_point_vector;
}

// AIM: Generate the vector of Edges for the set of RandomPoints
std::vector<Edge> calculate_graph_edges(const std::vector<RandomPoint>& random_point_graph) {
    std::vector<Edge> graph_edges;

    for (size_t first_point = 0; first_point < random_point_graph.size(); ++first_point) {
        for (size_t second_point = 0; second_point < random_point_graph.size(); ++second_point) {
            if (first_point != second_point) {
                Edge new_edge(first_point + 1, second_point + 1);
                new_edge.weight = euclidian_distance(random_point_graph[first_point],
                                                     random_point_graph[second_point]);
                graph_edges.push_back(new_edge);
            }
        }
    }

    return graph_edges;
}

//===========================================================================================================================
// AIM: Calculate the weight of the current path (in random_point_graph) given by point_path
double weight_of_point_path(const std::vector<RandomPoint>& random_point_graph,
                            const std::vector<size_t>& point_path) {
    if (point_path.size() == 0) {
        return 0.0;
    }

    double path_weight = 0.0;

    for (size_t index = 0; index < point_path.size() - 1; ++index) {
        path_weight += euclidian_distance(random_point_graph[point_path[index]],
                                          random_point_graph[point_path[index + 1]]);
    }
    path_weight += euclidian_distance(random_point_graph[point_path[random_point_graph.size() - 1]],
                                      random_point_graph[point_path[0]]);

    return path_weight;
}

//---------------------------------------------------------------------------------------------------------------------------
// Simple struct for DST of MST
struct PointCurradj {
    size_t point = 0;
    size_t curr_adjacent = 0;

    PointCurradj(const size_t& point_init, const size_t& curr_adjacent_init) :
            point(point_init), curr_adjacent(curr_adjacent_init) {}

};

// AIM: Return the MST-generated hamilton path
std::vector<size_t> get_mst_tsp_path(const SparseGraph& mst_graph) {
    std::vector<size_t> mst_point_path;

    std::stack<PointCurradj> dst_stack;
    std::set<size_t> dst_set;

    mst_point_path.push_back(0);
    dst_set.insert(0);
    dst_stack.push(PointCurradj(0, 0));

    while (!dst_stack.empty()) {
        PointCurradj curr_point_curradj = dst_stack.top();
        dst_stack.pop();

        size_t& curr_point = curr_point_curradj.point;
        size_t& curr_curradj = curr_point_curradj.curr_adjacent;

        for (;   curr_curradj < mst_graph.vertices(); ++curr_curradj) {
            if (mst_graph.has_edge(curr_point, curr_curradj) &&
                dst_set.count(curr_curradj) < 1) {
                break;
            }
        }

        if (curr_curradj != mst_graph.vertices()) {
            mst_point_path.push_back(curr_curradj);
            dst_set.insert(curr_curradj);
            dst_stack.push(curr_point_curradj);
            dst_stack.push(PointCurradj(curr_curradj, 0));
        }
    }

    return mst_point_path;
}

// AIM: Calculate an approximate answer to the Travelling Salesman Problem using the MST
double tsp_mst_approx(const std::vector<RandomPoint>& random_point_graph) {
    std::vector<Edge> graph_edges = calculate_graph_edges(random_point_graph);
    KruskalWrapper kruskal_instance(graph_edges, random_point_graph.size());
    SparseGraph mst_graph = kruskal_instance.get_mst();

    std::vector<size_t> mst_point_path = get_mst_tsp_path(mst_graph);

    return weight_of_point_path(random_point_graph, mst_point_path);
}

//---------------------------------------------------------------------------------------------------------------------------
// AIM: Calculate the answer to the Travelling Salesman Problem (using brute-force)
double tsp_answer(const std::vector<RandomPoint>& random_point_graph) {
    double tsp_answer = std::numeric_limits<double>::max();

    std::vector<size_t> point_permutation;
    for (size_t index = 0; index < random_point_graph.size(); ++index) {
        point_permutation.push_back(index);
    }
    std::sort(point_permutation.begin(), point_permutation.end());

    do {
        double new_path_len = weight_of_point_path(random_point_graph, point_permutation);
        if (tsp_answer > new_path_len) {
            tsp_answer = new_path_len;
        }
    } while (std::next_permutation(point_permutation.begin(), point_permutation.end()));

    return tsp_answer;
}

//===========================================================================================================================
// AIM: Test the TSP-MST approximation of num_point (normally-spread(0,0) 2D-Euclidian) points
//      Repeat the test num_tests times and return the collected TestResult
TestResult test_tsp_mst_approx(const size_t& num_points, const size_t& num_tests) {
    double intermediate_deviation_sum = 0.0;
    double intermediate_mean_error_squared = 0.0;

    for (size_t num_tests_run = 0; num_tests_run < num_tests; ++num_tests_run) {
        std::vector<RandomPoint> curr_random_points = generate_random_point_vector_of_size(num_points);

        double curr_tsp_approx = tsp_mst_approx(curr_random_points);
        double curr_tsp_answer = tsp_answer(curr_random_points);
        double absolute_delta = std::abs(curr_tsp_approx - curr_tsp_answer) / curr_tsp_answer;

        intermediate_deviation_sum += absolute_delta;
        intermediate_mean_error_squared += absolute_delta * absolute_delta;
    }

    double statistical_deviation = intermediate_deviation_sum / num_tests;
    double mean_square_error = sqrt(intermediate_mean_error_squared / num_tests);

    return TestResult(statistical_deviation, mean_square_error);
}

// AIM: Repeat test_tsp_mst_approx in range [num_points_start, num_points_end]
std::vector<TestResult> test_tsp_mst_approx_range(const size_t& num_points_start,
                                                  const size_t& num_points_end,
                                                  const size_t& num_tests) {
    
    std::vector<TestResult> test_results;

    for (size_t num_points = num_points_start; num_points <= num_points_end; ++num_points) {
        test_results.push_back(test_tsp_mst_approx(num_points, num_tests));
    }

    return test_results;
}

//===========================================================================================================================
