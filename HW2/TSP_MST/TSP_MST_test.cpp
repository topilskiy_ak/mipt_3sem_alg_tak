#include <gtest/gtest.h>
#include "TSP_MST_lib.h"

TEST(TSP_MST_TESTS, EUCLIDIAN_DISTANCE_TEST) {
    const static RandomPoint rndp1(0, 0);
    const static RandomPoint rndp2(1, 0);
    const static RandomPoint rndp3(0, 1);
    const static RandomPoint rndp4(1, 1);
    const static RandomPoint rndpX(5, 5);

    EXPECT_DOUBLE_EQ(0.0, euclidian_distance(rndp1, rndp1));
    EXPECT_DOUBLE_EQ(0.0, euclidian_distance(rndp2, rndp2));
    EXPECT_DOUBLE_EQ(0.0, euclidian_distance(rndp3, rndp3));
    EXPECT_DOUBLE_EQ(0.0, euclidian_distance(rndp4, rndp4));
    EXPECT_DOUBLE_EQ(0.0, euclidian_distance(rndpX, rndpX));

    EXPECT_DOUBLE_EQ(1.0, euclidian_distance(rndp1, rndp2));
    EXPECT_DOUBLE_EQ(1.0, euclidian_distance(rndp1, rndp3));
    EXPECT_DOUBLE_EQ(sqrt(2), euclidian_distance(rndp2, rndp3));
    EXPECT_DOUBLE_EQ(sqrt(2), euclidian_distance(rndp1, rndp4));

    EXPECT_DOUBLE_EQ(euclidian_distance(rndpX, rndp1), euclidian_distance(rndp1, rndpX));
    EXPECT_DOUBLE_EQ(euclidian_distance(rndpX, rndp2), euclidian_distance(rndp2, rndpX));
    EXPECT_DOUBLE_EQ(euclidian_distance(rndpX, rndp3), euclidian_distance(rndp3, rndpX));
    EXPECT_DOUBLE_EQ(euclidian_distance(rndpX, rndp4), euclidian_distance(rndp4, rndpX));

    EXPECT_DOUBLE_EQ(sqrt(2.0 * 25.0), euclidian_distance(rndp1, rndpX));
    EXPECT_DOUBLE_EQ(sqrt(25.0 + 16.0), euclidian_distance(rndp3, rndpX));
}

TEST(TSP_MST_TESTS, GENERATE_RANDOM_POINT_VECTOR_OF_SIZE_TEST) {
    const static size_t NUMBER_OF_POINTS = 100;
    const static double MAX_DIVERGENCE_EPSILON = 0.5;
    std::vector<RandomPoint> rdnpvec = generate_random_point_vector_of_size(NUMBER_OF_POINTS);

    EXPECT_EQ(rdnpvec.size(), NUMBER_OF_POINTS);

    double checksum = 0.0;
    for (const RandomPoint& rndp : rdnpvec) {
        checksum += rndp.first_num + rndp.second_num;
    }
    EXPECT_NEAR(checksum / NUMBER_OF_POINTS, 0.0, MAX_DIVERGENCE_EPSILON);
}

TEST(TSP_MST_TESTS, CALCULATE_GRAPH_EDGES_TEST) {
    const static size_t NUMBER_OF_POINTS = 100;
    std::vector<RandomPoint> rndpvec = generate_random_point_vector_of_size(NUMBER_OF_POINTS);
    std::vector<Edge> rndpedges = calculate_graph_edges(rndpvec);

    EXPECT_EQ(NUMBER_OF_POINTS * (NUMBER_OF_POINTS - 1), rndpedges.size());
}

TEST(TSP_MST_TESTS, TSP_MST_APPROX_TEST) {
    const static size_t NUMBER_OF_POINTS = 100;
    std::vector<RandomPoint> rndpvec = generate_random_point_vector_of_size(NUMBER_OF_POINTS);

    double rndpvec_approx = tsp_mst_approx(rndpvec);
    EXPECT_GE(rndpvec_approx, 0.0);
}

TEST(TSP_MST_TESTS, TSP_ANSWER_TEST) {
    const static size_t NUMBER_OF_POINTS = 5;
    std::vector<RandomPoint> rndpvec = generate_random_point_vector_of_size(NUMBER_OF_POINTS);

    double rndpvec_answer = tsp_answer(rndpvec);
    EXPECT_GE(rndpvec_answer, 0.0);

    double rndpvec_approx = tsp_mst_approx(rndpvec);
    EXPECT_LE(rndpvec_answer, rndpvec_approx);

    std::vector<Edge> graph_edges = calculate_graph_edges(rndpvec);
    KruskalWrapper kruskal_instance(graph_edges, rndpvec.size());
    double mst_weight = kruskal_instance.get_mst_weight();

    EXPECT_GE(rndpvec_answer, mst_weight);
}

TEST(TSP_MST_TESTS, TEST_TSP_MST_APPROX_TEST) {
    const static size_t NUM_POINTS_1 = 3;
    const static size_t NUM_TESTS__1 = 100;
    TestResult result1 = test_tsp_mst_approx(NUM_POINTS_1, NUM_TESTS__1);
    EXPECT_NEAR(result1.statistical_deviation, 0, 0.0001);
    EXPECT_NEAR(result1.mean_square_error,     0, 0.0001);

    const static size_t NUM_POINTS_2 = 5;
    const static size_t NUM_TESTS__2 = 50;
    TestResult result2 = test_tsp_mst_approx(NUM_POINTS_2, NUM_TESTS__2);
    EXPECT_NEAR(result2.statistical_deviation, 0, 0.25);
    EXPECT_NEAR(result2.mean_square_error,     0, 0.25);

    const static size_t NUM_POINTS_3 = 7;
    const static size_t NUM_TESTS__3 = 20;
    TestResult result3 = test_tsp_mst_approx(NUM_POINTS_3, NUM_TESTS__3);
    EXPECT_NEAR(result3.statistical_deviation, 0, 0.40);
    EXPECT_NEAR(result3.mean_square_error,     0, 0.40);
}

TEST(TSP_MST_TESTS, TEST_TSP_MST_APPROX_RANGE_TEST) {
    const static size_t NUM_POINTS_BEGIN = 2;
    const static size_t NUM_POINTS_END   = 10;
    const static size_t NUM_TESTS = 5;

    std::vector<TestResult> vec_test_result =
            test_tsp_mst_approx_range(NUM_POINTS_BEGIN, NUM_POINTS_END, NUM_TESTS);
}