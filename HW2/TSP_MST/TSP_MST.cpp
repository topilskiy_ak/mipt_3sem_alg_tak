#include "TSP_MST_lib.h"

int main() {
    const static size_t NUM_POINTS_BEGIN = 2;
    const static size_t NUM_POINTS_END   = 10;
    const static size_t NUM_TESTS = 20;

    std::vector<TestResult> vec_test_result =
            test_tsp_mst_approx_range(NUM_POINTS_BEGIN, NUM_POINTS_END, NUM_TESTS);

    for (size_t index = NUM_POINTS_BEGIN; index <= NUM_POINTS_END; ++index) {
        std::cout << "EXPERIMENT ON " << index << " VERTICES: " << std::endl;
        std::cout << "  STATISTICAL DEVIATION: "
                  << vec_test_result[index - NUM_POINTS_BEGIN].statistical_deviation << std::endl;
        std::cout << "  MEAN SQUARE ERROR:     "
                  << vec_test_result[index - NUM_POINTS_BEGIN].mean_square_error << std::endl;
    }
}